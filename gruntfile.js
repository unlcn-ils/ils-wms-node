/**
 * Created by lihao on 17/5/30.
 */

module.exports = function(grunt) {
    'use strict';
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: ['public/js/*.js','public/js/ctrl/*.js'],
                dest: 'public/js/build/<%= pkg.name %>.min.js'
            }
        },
        ngtemplates: {
            wmsApp: {
                // cwd: 'dist',
                src: ['public/templates/**/*.html'],
                dest: 'public/js/build/<%= pkg.name %>.tmpl.js',
                options: {
                    htmlmin: { //https://github.com/kangax/html-minifier#options-quick-reference
                        removeComments:                 true,
                        removeCommentsFromCDATA:        true,
                        removeCDATASectionsFromCDATA:   true,
                        collapseWhitespace:             true,
                        conservativeCollapse:           true,
                        preserveLineBreaks:             true,
                        collapseBooleanAttributes:      false,
                        removeAttributeQuotes:          false,
                        removeRedundantAttributes:      true,
                        preventAttributesEscaping:      true,
                        useShortDoctype:                true,
                        removeEmptyAttributes:          false,
                        removeScriptTypeAttributes:     false,
                        removeStyleLinkTypeAttributes:  true,
                        removeOptionalTags:             false,
                        removeIgnored:                  false,
                        removeEmptyElements:            false,
                        keepClosingSlash:               true,
                        caseSensitive:                  true,
                        minifyJS: {
                            mangle: true
                        },
                        minifyCSS:                      true,
                        minifyURLs:                     false
                    }
                }
            }
        },
        'string-replace': {
            tmpl_html: {
                files: {'public/js/build/<%= pkg.name %>.tmpl.js': 'public/js/build/<%= pkg.name %>.tmpl.js'},
                options: {
                    replacements: [
                        {
                            pattern: /put\('public/g,
                            replacement: "put('"
                        }
                    ]
                }
            },
            min_js: {
                files: {'public/js/build/<%= pkg.name %>.min.js': 'public/js/build/<%= pkg.name %>.min.js'},
                options: {
                    replacements: [
                        {
                            pattern: "var adminCtrl=angular.module(\"admin.controllers\",[]);",
                            replacement: ""
                        },
                        {
                            pattern: "\"use strict\";",
                            replacement: "\"use strict\";var adminCtrl = angular.module(\"admin.controllers\", []);"
                        }
                    ]
                }
            }
        },
        cssmin: { // https://github.com/jakubpawlowicz/clean-css#how-to-use-clean-css-programmatically
            css: {
                src: ['public/css/kas.css','public/css/wms.css'],
                // src: ['public/css/index.css', 'public/css/ils-tms-web-animate.css'],
                dest:'public/css/<%=pkg.name %>.min.css'
            }
        }
    });

    // 加载包含 "uglify" 任务的插件。
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // 默认被执行的任务列表。
    grunt.registerTask('admin', ['uglify', 'ngtemplates', 'string-replace', 'cssmin']);

    // 自定义任务
    /*
     * node 环境部署，自动切换环境变量配置命令
     * 切换到项目根目录，执行下面对应环境命令：
     * 1. 本地命令：grunt pom:local:port[8888]。本地环境指定端口，不指定默认为8888; 例如，grunt pom:local:8080
     * 2. 开发命令：grunt pom:dev
     * 3. 测试命令：grunt pom:uat
     * 4. 正式命令：grunt pom:pro
     */
    grunt.task.registerTask('pom', '指定运行环境', function(task) {
        var localObj = {};
        var contents = '/**Created on ' + grunt.template.today("yyyy-mm-dd") + '*/\n\r'
            + '\'use strict\';\n\r' + 'module.exports = ';
        var config = grunt.file.readJSON('./config/pom.json');
        localObj = config[task];
        contents += JSON.stringify(localObj) + ';';
        grunt.file.write('./routes/resource/base-config.js', contents, {
            encoding: 'utf8'
        });
    });

    // 文件操纵命令
    grunt.task.registerTask('file', '指定运行环境', function(type) {
        // 删除源码文件
        if (type == 'delete') {
            // 删除html
            grunt.file.delete('public/templates', {
                force: true
            });
            // js
            grunt.file.delete('public/js/ctrl', {
                force: true
            });
        }
    });

};