/**
 * Created by lsj on 2017/8/17.
 */
'use strict';
var wmsFilter = angular.module('wms.filter', [])
    .filter('billsTypeFilter', ['constant', function (constant) {   //单据类型过滤器
        return function (input, param) {
            if (input == constant.billsType.systemSchedul) {  //系统调度
                return constant.billsTypeName.systemSchedul;
            } else if (input == constant.billsType.manualEntry) {  //手工录入
                return constant.billsTypeName.manualEntry;
            } else if (input == constant.billsType.add) {  //wms新增
                return constant.billsTypeName.add;
            } else if (input == constant.billsType.exit) {  //备料退库
                return constant.billsTypeName.exit;
            }
            return input;
        }
    }])
    .filter('inspectionResultsFilter', ['constant', function (constant) {   //检验结果
        return function (input, param) {
            if (input == constant.inspectionResults.asnOrderPass) {   //合格
                return constant.inspectionResultsName.asnOrderPass;
            } else if (input == constant.inspectionResults.asnOrderNotpassInRepair) {  //异常
                return constant.inspectionResultsName.asnOrderNotpassInRepair;
            }
            /*else　if(input == constant.inspectionResults.asnOrderNotpassOutRepair){  //不合格-委外返工
                            return constant.inspectionResultsName.asnOrderNotpassOutRepair;
                        }else　if(input == constant.inspectionResults.asnOrderNotpass){  //不合格-退厂
                            return constant.inspectionResultsName.asnOrderNotpass;
                        }*/
            return input;
        }
    }])
    .filter('orderStatusFilter', ['constant', function (constant) {   //订单状态
        return function (input, param) {
            if (input == constant.orderStatus.wmsInboundCreate) {   //未收货
                return constant.orderStatusName.wmsInboundCreate;
            } else if (input == constant.orderStatus.wmsInboundWaitAllocation) {  //已收货
                return constant.orderStatusName.wmsInboundWaitAllocation;
            } else if (input == constant.orderStatus.wmsInboundFinish) {  //已入库
                return constant.orderStatusName.wmsInboundFinish;
            } else if (input == constant.orderStatus.wmsInboundCancel) {  //取消入库
                return constant.orderStatusName.wmsInboundCancel;
            }
            return input;
        }
    }])
    .filter('stockUnitFilter', ['constant', function (constant) {   //库位状态
        return function (input, param) {
            if (input == constant.stockUnitStatus.start) {   //启用
                return constant.stockUnitStatusName.start;
            } else if (input == constant.stockUnitStatus.stop) {  //停用
                return constant.stockUnitStatusName.stop;
            }
            return input;
        }
    }])
    .filter('repairTypeFilter', ['constant', function (constant) {   //维修类型
        return function (input, param) {
            if (input == constant.repairType.inner) {   //委内维修
                return constant.repairTypeName.inner;
            } else if (input == constant.repairType.outer) {  //委外维修
                return constant.repairTypeName.outer;
            }
            return input;
        }
    }])
    .filter('repairStatusFilter', ['constant', function (constant) {    //维修状态
        return function (input, param) {
            if (input == constant.repairStatus.unprocessed) {   //未处理
                return constant.repairStatusName.unprocessed;
            } else if (input == constant.repairStatus.maintenance) {  //维修中
                return constant.repairStatusName.maintenance;
            } else if (input == constant.repairStatus.finish) {  //已完成
                return constant.repairStatusName.finish;
            }
            return input;
        }
    }])
    .filter('repairResultFilter', ['constant', function (constant) {    //维修结果
        return function (input, param) {
            if (input == constant.repairResult.qualified) {   //合格
                return constant.repairResultName.qualified;
            } else if (input == constant.repairResult.disqualification) {  //不合格
                return constant.repairResultName.disqualification;
            } else if (input == constant.repairResult.cancellinStocks) {  //退库
                return constant.repairResultName.cancellinStocks;
            }
            return input;
        }
    }])
    .filter('warehouseFilter', ['constant', function (constant) {    //出库状态
        return function (input, param) {
            if (input == constant.warehouse.unprocessed) {   //未领取
                return constant.warehouseName.unprocessed;
            } else if (input == constant.warehouse.execution) {  //待开始
                return constant.warehouseName.execution;
            } else if (input == constant.warehouse.haveOutbound) {  //进行中
                return constant.warehouseName.haveOutbound;
            } else if (input == constant.warehouse.finish) {   //已完成
                return constant.warehouseName.finish;
            } else if (input == constant.warehouse.cancel) {   //已取消
                return constant.warehouseName.cancel;
            }
            return input;
        }
    }])
    .filter('warehouseTypeFilter', ['constant', function (constant) {   //仓库类型
        return function (input, param) {
            if (input == constant.warehouseType.commodityGarage) {   //商品仓库
                return constant.warehouseTypeName.commodityGarage;
            } else if (input == constant.warehouseType.entrepotStorage) {  //中转库
                return constant.warehouseTypeName.entrepotStorage;
            } else if (input == constant.warehouseType.frontLibrary) {  //前置库
                return constant.warehouseTypeName.frontLibrary;
            }
            return input;
        }
    }])
    .filter('warehouseAttrFilter', ['constant', function (constant) {   //仓库属性
        return function (input, param) {
            if (input == constant.warehouseAttr.standardLib) {   //标准库
                return constant.warehouseAttrName.standardLib;
            } else if (input == constant.warehouseAttr.simpleLib) {  //简单库
                return constant.warehouseAttrName.simpleLib;
            }
            return input;
        }
    }])
    .filter('warehouseAutoFilter', ['constant', function (constant) {   //仓库库位自动分配
        return function (input, param) {
            if (input == constant.warehouseAuto.yes) {   //是
                return constant.warehouseAutoName.yes;
            } else if (input == constant.warehouseAuto.no) {  //否
                return constant.warehouseAutoName.no;
            }
            return input;
        }
    }])
    .filter('checkCarStatusFilter', ['constant', function (constant) {   //验车状态
        return function (input, param) {
            if (input == constant.checkCarStatus.noCheck) {   //待验车
                return constant.checkCarStatusName.noCheck;
            } else if (input == constant.checkCarStatus.check) {  //合格
                return constant.checkCarStatusName.check;
            } else if (input == constant.checkCarStatus.error) {   //异常
                return constant.checkCarStatusName.error;
            } else if (input == constant.checkCarStatus.errorApp) {   //异常app
                return constant.checkCarStatusName.errorApp;
            }
            return input;
        }
    }])
    .filter('checkCarResultFilter', ['constant', function (constant) {   //验车结果
        return function (input, param) {
            if (input == constant.checkCarResult.qualified) {   //合格
                return constant.checkCarResultName.qualified;
            } else if (input == constant.checkCarResult.unqualified) {  //不合格
                return constant.checkCarResultName.unqualified;
            }
            return input;
        }
    }])
    .filter('strategyConditionTypeFilter', ['constant', function (constant) {   //策略条件类型
        return function (input, param) {
            if (input == constant.strategyConditionType.shipper) {   //货主
                return constant.strategyConditionTypeName.shipper;
            } else if (input == constant.strategyConditionType.vehicle) {  //车型
                return constant.strategyConditionTypeName.vehicle;
            }
            return input;
        }
    }])
    .filter('strategyConditionFilter', ['constant', function (constant) {   //策略条件
        return function (input, param) {
            if (input == constant.strategyOperator.xd) {   //等于
                return constant.strategyOperatorName.xd;
            } else if (input == constant.strategyOperator.bdy) {  //不等于
                return constant.strategyOperatorName.bdy;
            } else if (input == constant.strategyOperator.xy) {  //小于
                return constant.strategyOperatorName.xy;
            } else if (input == constant.strategyOperator.dy) {  //大于
                return constant.strategyOperatorName.dy;
            } else if (input == constant.strategyOperator.xydy) {  //小于等于
                return constant.strategyOperatorName.xydy;
            } else if (input == constant.strategyOperator.dydy) {  //大于等于
                return constant.strategyOperatorName.dydy;
            } else if (input == constant.strategyOperator.bh) {  //包含
                return constant.strategyOperatorName.bh;
            } else if (input == constant.strategyOperator.bbh) {  //不包含
                return constant.strategyOperatorName.bbh;
            } else if (input == constant.strategyOperator.s) {  //是
                return constant.strategyOperatorName.s;
            } else if (input == constant.strategyOperator.bs) {  //不是
                return constant.strategyOperatorName.bs;
            }
            return input;
        }
    }])
    .filter('outboundStatusFilter', ['constant', function (constant) {  //备料状态
        return function (input, param) {
            if (input == constant.outboundStatus.unStart) {   //未开始
                return constant.outboundStatusName.unStart;
            } else if (input == constant.outboundStatus.planned) {  //已计划
                return constant.outboundStatusName.planned;
            } else if (input == constant.outboundStatus.confirmed) {  //已确认
                return constant.outboundStatusName.confirmed;
            } else if (input == constant.outboundStatus.finish) {  //已完成
                return constant.outboundStatusName.finish;
            } else if (input == constant.outboundStatus.cancel) {  //已取消
                return constant.outboundStatusName.cancel;
            }
            return input;
        }
    }])
    .filter('outboundTypeFilter', ['constant', function (constant) {  //出库类型
        return function (input, param) {
            if (input == constant.outboundType.dispatcherOutbound) {   //调拨出库
                return constant.outboundTypeName.sendOutbound;
            } else if (input == constant.outboundType.sendOutbound) {  //发运出库
                return constant.outboundTypeName.sendOutbound;
            } else if (input == constant.outboundType.repairOutbound) {  //维修出库
                return constant.outboundTypeName.repairOutbound;
            } else if (input == constant.outboundType.borrowOutbound) {  //借用出库
                return constant.outboundTypeName.borrowOutbound;
            }
            return input;
        }
    }])
    .filter('statusFilter', ['constant', function (constant) {  //可用 状态
        return function (input, param) {
            if (input == constant.status.enable) {   //可用
                return constant.statusName.enable;
            } else if (input == constant.status.disable) {  //禁用
                return constant.statusName.disable;
            }
            return input;
        }
    }])
    .filter('outboundOutStatusFilter', ['constant', function (constant) {  //备料出库状态
        return function (input, param) {
            if (input == constant.outboundOutStatus.notOut) {   //未出库
                return constant.outboundOutStatusName.notOut;
            } else if (input == constant.outboundOutStatus.out) {  //已出库
                return constant.outboundOutStatusName.out;
            } else if (input == constant.outboundOutStatus.exit) {  //已退库
                return constant.outboundOutStatusName.exit;
            }
            return input;
        }
    }])
    .filter('shipmentPlanStatusFilter', ['constant', function (constant) {  //备料出库状态
        return function (input, param) {
            if (!input){
                return constant.shipmentPlanStatusName.untreated;
            }
            if (input == constant.shipmentPlanStatus.untreated) {   //未发运
                return constant.shipmentPlanStatusName.untreated;
            } else if (input == constant.shipmentPlanStatus.treated) {  //已发运
                return constant.shipmentPlanStatusName.treated;
            } else if (input == constant.shipmentPlanStatus.leaved) {  //已发运
                return constant.shipmentPlanStatusName.leaved;
            }
            return input;
        }
    }])
    .filter('shipmentPlanTypeFilter', ['constant', function (constant) {  //发运类型
        return function (input, param) {
            if (input == constant.shipmentPlanType.normal) {   //正常发运
                return constant.shipmentPlanTypeName.normal;
            } else if (input == constant.shipmentPlanType.dispatcher) {  //调度发运
                return constant.shipmentPlanTypeName.dispatcher;
            }
            return input;
        }
    }])
    .filter('timeformat', function () {
        return function (time) {
            if (time) {
                return new Date(time).getTime();
            } else {
                return '';
            }
        }
    })
    .filter('businessTypeFilter', ['constant', function (constant) {  //发运类型
        return function (input, param) {
            if (input == constant.businessType.Z1) {   //PDI合格入库
                return constant.businessTypeName.Z1;
            } else if (input == constant.businessType.Z2) {  //调拨入库
                return constant.businessTypeName.Z2;
            } else if (input == constant.businessType.Z3) {  //维修出库
                return constant.businessTypeName.Z3;
            } else if (input == constant.businessType.Z4) {  //维修后入库
                return constant.businessTypeName.Z4;
            } else if (input == constant.businessType.Z5) {  //其他入库
                return constant.businessTypeName.Z5;
            } else if (input == constant.businessType.Z6) {  //其他出库
                return constant.businessTypeName.Z6;
            } else if (input == constant.businessType.Z7) {  //借用出库
                return constant.businessTypeName.Z7;
            } else if (input == constant.businessType.Z8) {  //借用入库
                return constant.businessTypeName.Z8;
            } else if (input == constant.businessType.Z9) {  //退货入库
                return constant.businessTypeName.Z9;
            }
            return input;
        }
    }])
    .filter('aFilter', function () {
        return function (st) {
            if (st == '0') {
                return '未处理'
            } else if (st == '1') {
                return '已处理'
            } else {
                return ''
            }
        }
    })
    .filter('bFilter', function () {
        return function (st) {
            if (st == '10') {
                return '否'
            } else if (st == '20') {
                return '是'
            } else {
                return ''
            }
        }
    })
    .filter('cFilter', function () {
        return function (st) {
            if (st == 'A1') {
                return '正常发运'
            } else if (st == 'A2') {
                return '调拨发运'
            } else {
                return ''
            }
        }
    })
    .filter('dFilter', function () {
        return function (str) {
            if (str == '10') {
                return '未领取';
            } else if (str == '20') {
                return '待开始';
            } else if (str == '30') {
                return '进行中';
            } else if (str == '40') {
                return '已完成';
            } else if (str == '50') {
                return '已取消';
            }
        }
    })
    .filter('eFilter', function () {
        return function (str) {
            if (str == '10') {
                return '合格';
            } else if (str == '20') {
                return '异常';
            }
        }
    })
    .filter('fFilter', ['constant', function (constant) {   //换车状态
        return function (input) {
            if (input == constant.changeCharStatus.noChang) {   //未换车
                return constant.changeCharStatusName.noChang;
            } else if (input == constant.changeCharStatus.change) {  //已换车
                return constant.changeCharStatusName.change;
            } else if (input == constant.changeCharStatus.notChange) {  //无车可换
                return constant.changeCharStatusName.notChange;
            }
        }
    }])
    .filter('locSizeFilter', ['constant', function (constant) {  //库位存车类型
        return function (input, param) {
            if (input == constant.locSize.minSize) {   //小型车
                return constant.locSizeName.minSize;
            } else if (input == constant.locSize.maxSize) {
                return constant.locSizeName.maxSize;
            }
            return input;
        }
    }])
    .filter('locTypeFilter', ['constant', function (constant) {  //库位类型
        return function (input, param) {
            if (input == constant.locType.normal) {   //正常
                return constant.locTypeName.normal;
            } else if (input == constant.locType.standby) {    //备用
                return constant.locTypeName.standby;
            }
            return input;
        }
    }])
    .filter('entranceCardStatusFilter', ['constant', function (constant) {  //门禁卡状态
        return function (input, param) {
            if (input == constant.entranceCardStatus.start) {   //启用
                return constant.entranceCardStatusName.start;
            } else if (input == constant.entranceCardStatus.stop) {    //禁用
                return constant.entranceCardStatusName.stop;
            }
        }
    }])
    .filter('openGateStatusFilter', ['constant', function (constant) {  //开闸状态
        return function (input, param) {
            if (input == constant.openGateStatus.notOpen) {   //未开闸
                return constant.openGateStatusName.notOpen;
            } else if (input == constant.openGateStatus.open) {   //已开闸
                return constant.openGateStatusName.open;
            }
            return input;
        }
    }])
    .filter('outboundPrintStatusFilter', ['constant', function (constant) {  //打印状态
        return function (input, param) {
            if (input == constant.outboundPrintStatus.notPrint) {   //未打印
                return constant.outboundPrintStatusName.notPrint;
            } else if (input == constant.outboundPrintStatus.print) {    //已打印
                return constant.outboundPrintStatusName.print
            }
            return input;
        }
    }])
    .filter('userTypeFilter', ['constant', function (constant) {  //用户类型
        return function (input, param) {
            if (input == constant.userType.pc) {   //pc
                return constant.userTypeName.pc;
            } else if (input == constant.userType.app) {    //app
                return constant.userTypeName.app
            } else if (input == constant.userType.driver) {    //司机
                return constant.userTypeName.driver
            }
            return input;
        }
    }])
    .filter('inventoryStatusFilter', ['constant', function (constant) {  //库存状态
        return function (input, param) {
            if (input == constant.inventoryStatus.normal) {   //正常在库
                return constant.inventoryStatusName.normal;
            } else if (input == constant.inventoryStatus.shipLock) {   //发运锁定
                return constant.inventoryStatusName.shipLock;
            } else if (input == constant.inventoryStatus.borrowLock) {   //借用锁定
                return constant.inventoryStatusName.borrowLock;
            } else if (input == constant.inventoryStatus.maintenanceLock) {   //维修锁定
                return constant.inventoryStatusName.maintenanceLock;
            } else if (input == constant.inventoryStatus.outbound) {   //已出库
                return constant.inventoryStatusName.outbound;
            } else if (input == 60) {
                return '库存冻结'
            }
        }
    }])
    .filter('departureTypeFilter', ['constant', function (constant) {  //离场类型
        return function (input, param) {
            if (input == constant.departureType.normalDeparture) {   //正常离场
                return constant.departureTypeName.normalDeparture;
            } else if (input == constant.departureType.errorDeparture) {   //异常离场
                return constant.departureTypeName.errorDeparture;
            }
            return input;
        }
    }])
    .filter('departureTypeFt', function () {
        return function (str) {
            if (str == '10') {
                return '自动tms调用成功开启'
            } else if (str == '20') {
                return '页面手动开启闸门'
            } else if (str == '30') {
                return '系统闸门开启失败'
            }
        }
    })
    .filter('isOpenGateFail', function () {
        return function (str) {
            if (str == '30') {
                return true
            } else {
                return false
            }
        }
    })
    .filter('departureOutInTypeFt', function () {
        return function (str) {
            if (str == 'OUT') {
                return '出场'
            } else if (str == 'IN') {
                return '入场'
            }
        }
    })
    .filter('bs_ow_md', function () {
        return function (str) {
            if (str == 'ROAD') {
                return '陆运';
            } else if (str == 'RAIL') {
                return '铁运';
            } else if (str == 'WATER') {
                return '船运'
            }
        }
    })
    .filter('wppStatusType', function () {
        return function (str) {
            if (str == '20') {
                return '未处理'
            } else if (str == '30') {
                return '已处理'
            }
        }
    })
    .filter('drag_out_in_status', function () {
        return function (st) {
            if (st == 'NOT_APPROACH') {
                return '未进场'
            } else if (st == 'EXTENDED_NOT_APPROACH') {
                return '超期未进场';
            } else if (st == 'NORMAL_APPROACH') {
                return '正常进场';
            } else if (st == 'EXTENDED_APPROACH') {
                return '超期进场';
            } else if (st == 'NORMAL_DEPARTURE') {
                return '正常离场';
            } else if (st == 'DISPATCH_DELAY') {
                return '超期离场'
            } else if (st == 'EXTENDE_NOT_DEPARTURE') {
                return '超期未离场'
            }
        }
    })
    .filter('abnormal_excpStatus', function () {
        return function (str) {
            if (str == '10') {
                return '未处理'
            } else if (str == '20') {
                return '处理中'
            } else if (str == '30') {
                return '已关闭'
            }
        }
    })
    .filter('abnormal_excpType', function () {
        return function (str) {
            if (str == '10') {
                return '入库异常'
            } else if (str == '20') {
                return '出库异常'
            } else if (str == '30') {
                return '备料异常'
            }
        }
    })
    .filter('abnormal_dealType', function () {
        return function (str) {
            if (str == '10') {
                return '缺件跟进'
            } else if (str == '20') {
                return '出库维修'
            } else if (str == '30') {
                return '库内维修'
            } else if (str == '40') {
                return '缺件跟进，出库维修'
            } else if (str == '50') {
                return '缺件跟进，库位修改'
            }
        }
    }).filter('abnormal_excpTypeCode', function () {
        return function (str) {
            if (str == 'MISS') {
                return '缺件异常'
            } else if (str == 'EXCP') {
                return '质损异常'
            } else if (str == 'MISS_AND_EXCP') {
                return '缺件，质损异常'
            } else if (str == 'LOSS') {
                return '非质损异常'
            } else if (str == 'LOSS_AND_EXCP') {
                return '非质损，质损异常'
            } else if (str == 'LOSS_AND_MISS') {
                return '非质损，缺件异常'
            } else if (str == 'LOSS_EXCP_EXCP') {
                return '非质损，质损，缺件异常'
            }
        }
    })
    .filter('abnormal_excpDesc', ['$sce', function ($sce) {
        return function (str) {
            if (str) {
                var temp = '';
                var arr = str.split(',');
                for (var i = 0; i < arr.length; i++) {
                    temp += "<p style='text-align:left'>" + arr[i] + "</p>";
                }
                return $sce.trustAsHtml(temp);
            } else {
                var str = '<div style="height: auto;text-align: center">无</div>';
                return $sce.trustAsHtml(str)
            }
        }
    }])
    .filter('isHasIcon', function () {
        return function (str) {
            if (str) {
                if (str.indexOf(',') == -1) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false
            }

        }
    })

    .filter('quitTaskFilter', ['constant', function (constant) {  //是否退库
        return function (input, param) {
            if (input == constant.quitTask.yes) {   //是
                return constant.quitTaskName.yes;
            } else if (input == constant.quitTask.no) {   //否
                return constant.quitTaskName.no;
            }
            return input;
        }
    }])
    .filter('outOfStorageOperationSendStatusFilter', [function () {
        return function (str) {
            if (str == '10') {
                return '发送成功';
            } else if (str == '20') {
                return '发送失败';
            } else if (str == '30') {
                return '发送中';
            } else if (str == '40') {
                return '未发送';
            }
        }

    }])
    .filter('outOfStorageOperationInOutTypeFilter', [function () {
        return function (str) {
            if (str == 'C1') {
                return '出库';
            } else if (str == 'C2') {
                return '入库';
            }
        }
    }])
    .filter('outOfStorageOperationBusinessTypeFilter', [function () {
        return function (str) {
            if (str == 'Z1') {
                return 'PDI合格入库';
            } else if (str == 'Z2') {
                return '调拨入库';
            } else if (str == 'Z3') {
                return '维修出库';
            } else if (str == 'Z4') {
                return '维修后入库';
            } else if (str == 'Z5') {
                return '其他入库';
            } else if (str == 'Z6') {
                return '其他出库';
            } else if (str == 'Z7') {
                return '借用出库';
            } else if (str == 'Z8') {
                return '借用入库';
            } else if (str == 'Z9') {
                return '退货入库';
            }
        }
    }]);
