/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
wmsCtrl.controller('closeAbnormalCtrl',['$scope','$rootScope','$timeout','ngDialog','abnormalManagementService',function($scope,$rootScope,$timeout,ngDialog,abnormalManagementService){

    //    异常关闭
    $scope.abm_desc ='';
    $scope.updateToClose = function(){
        var obj = $rootScope.abm_main_tableList[$rootScope.abm_indexArr[0]];
        var temp = {
            orderId : obj.orderId,
            excpType: obj.excpType,
            dealResultDesc: $scope.abm_desc,
            excpTypeCode: obj.excpTypeCode,
            excpTimestamp:obj.excpTimestamp
        };
        abnormalManagementService.updateToClosed(temp).then(function(res){
            if(res.success){
                $rootScope.abm_getList();   //成功后刷新列表
                ngDialog.closeAll();
                $timeout(function(){
                    $scope.Tools.alert(res.message);
                },20)
            } else {
                ngDialog.closeAll();
                layer.open({
                    title: '提示'
                    ,content: res.message
                });
            }
        })
    }
}])