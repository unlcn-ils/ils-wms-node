/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
//入库管理 ctrl
wmsCtrl.controller('inStorageBillCtrl', ['$scope', 'inStorageBillService', 'dropDownService', function (scope, inStorageBillService, dropDownService) {

    //仓库
    var shck = [
        {
            name: '我只是仓库',
            value: '1'
        },
        {
            name: '我只是仓库1',
            value: '2'
        },
        {
            name: '我只是仓库1',
            value: '3'
        }
    ];

    //品牌
    var pp = [
        {
            name: '我只是品牌',
            value: '1'
        },
        {
            name: '我只是品牌1',
            value: '2'
        },
        {
            name: '我只是品牌2',
            value: '3'
        }
    ]

    //新增入库单 click
    scope.addReceivingNoteClick = function () {
        //获取货主
        dropDownService.getCustomerInfo()
            .then(function (customerResult) {
                if (customerResult.success) {
                    //获取车型
                    dropDownService.getVehicleSpec()
                        .then(function (vehicleSpecResult) {
                            if (vehicleSpecResult.success) {
                                scope.Tools.dialog({
                                    controller: 'inStorageBillCtrl.dialog',
                                    template: 'templates/inStorageManagement/inStorageBill/in-storage-bill-form.html',
                                    closeByDocument: false,
                                    closeByEscape: false,
                                    data: {
                                        billsType: scope.Tools.dropDown.billsTypeDropDown, //单据类型
                                        shipper: customerResult.data, //货主
                                        warehouse: shck,   //仓库
                                        vehicleSpec: vehicleSpecResult.data, //车型
                                        brand: pp, //品牌
                                        dropDownDefault: {    //下拉框默认值
                                            billsType: scope.Tools.dropDown.billsTypeDropDown[0],//设置默认值
                                            shipper: customerResult.data[0], //货主默认值
                                            warehouse: shck[0], //仓库默认值
                                            vehicleSpec: vehicleSpecResult.data[0],   //车型默认值
                                            brand: pp[0]   //品牌默认值
                                        },
                                        receipt: {}
                                    },
                                    width: 770,
                                    title: '新增入库单',
                                    scope: scope
                                })
                            } else {
                                scope.Tools.alert(vehicleSpecResult.message);
                            }
                        })

                } else {
                    scope.Tools.alert(customerResult.message);
                }
            })
    }

    //更新入库单 click
    scope.updateReceivingNoteClick = function () {

        var indexArr = scope.Tools.getTableSelectItemIndex('.inStorageTable');  //获取table所选的下标

        if (indexArr.length == 0) {
            scope.Tools.alert('请选择需要更新的数据');
            return;
        }

        if (indexArr.length > 1) {
            scope.Tools.alert('只能选择一条数据进行更新');
            return;
        }
        //根据id获取入库单
        inStorageBillService.getReceiptById(scope.inStorageList[indexArr[0]].odId)
            .then(function (result) {
                if (result.success) {
                    //设置仓库默认值
                    var defaultCk = {};
                    for (var i = 0; i < shck.length; i++) {
                        if (shck[i].value == result.data.odWhId) {
                            defaultCk = shck[i];
                            break;
                        }
                    }

                    //获取货主
                    dropDownService.getCustomerInfo()
                        .then(function (customerResult) {
                            if (customerResult.success) {
                                //获取车型
                                dropDownService.getVehicleSpec()
                                    .then(function (vehicleSpecResult) {
                                        if (vehicleSpecResult.success) {
                                            //设置货主默认值
                                            var defaultShipper = {};
                                            for (var i = 0; i < customerResult.data.length; i++) {
                                                if (customerResult.data[i].customerId) {  //判断是否存在货主
                                                    if (result.data.odCustomerCode == customerResult.data[i].customerId.code) {
                                                        defaultShipper = customerResult.data[i];
                                                        break;
                                                    }
                                                }
                                            }
                                            //设置车型默认值
                                            var defaultVehicleSpec = {};
                                            for (var i = 0; i < vehicleSpecResult.data.length; i++) {
                                                if (result.data.oddVehicleSpecCode == vehicleSpecResult.data[i].code) {
                                                    defaultVehicleSpec = vehicleSpecResult.data[i];
                                                    break;
                                                }
                                            }

                                            scope.Tools.dialog({
                                                controller: 'inStorageBillCtrl.dialog',
                                                template: 'templates/inStorageManagement/inStorageBill/in-storage-bill-form.html',
                                                closeByDocument: false,
                                                closeByEscape: false,
                                                data: {
                                                    billsType: scope.Tools.dropDown.billsTypeDropDown, //单据类型
                                                    shipper: customerResult.data, //货主
                                                    warehouse: shck,   //仓库
                                                    vehicleSpec: vehicleSpecResult.data, //车型
                                                    brand: pp, //品牌
                                                    dropDownDefault: {    //下拉框默认值
                                                        billsType: scope.Tools.getBillsTypeDropDownByKey(result.data.asnBillType),//设置默认值
                                                        shipper: defaultShipper, //货主默认值
                                                        warehouse: defaultCk, //仓库默认值
                                                        vehicleSpec: defaultVehicleSpec/*,   //车型默认值
                                                        brand : pp[0]   //品牌默认值*/
                                                    },
                                                    receipt: result.data
                                                },
                                                width: 770,
                                                title: '更新入库单',
                                                scope: scope
                                            })
                                        } else {
                                            scope.Tools.alert(vehicleSpecResult.message);
                                        }
                                    })
                            } else {
                                scope.Tools.alert(customerResult.message);
                            }
                        })
                } else {
                    scope.Tools.alert(result.message);
                }
            })
    }

    //删除入库单 click
    scope.deleteReceivingNoteClick = function () {

        var indexArr = scope.Tools.getTableSelectItemIndex('.inStorageTable');  //获取table所选的下标

        var inStorageList = scope.Tools.getDataTOIndex(indexArr, scope.inStorageList);

        if (inStorageList.length == 0) {
            scope.Tools.alert('请选择需要删除的数据');
            return;
        }

        scope.Tools.dialogConfirm('确定要删除选中的数据吗？')
            .then(function (res) {
                if (res) {
                    inStorageBillService.deleteAsnOrder(inStorageList)
                        .then(function (result) {
                            if (result.success) {
                                scope.fristSelect = true;   //设置为第一次查询
                                scope.list(scope.listParam());
                                scope.Tools.alert('删除数据成功');
                            } else {
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            })
    }

    //更多条件默认值
    scope.moreText = scope.constant.moreText.spread;
    //更多条件
    scope.moreClick = function () {
        scope.otherForm = !scope.otherForm;
        if (scope.otherForm) {
            scope.moreText = scope.constant.moreText.packUp;
        } else {
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    /*状态数据*/
    scope.dropDownList = {
        status: scope.Tools.dropDown.orderStatusDropDown.concat()    //订单状态
    }
    scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData);  //向前插入空值

    //设置默认值
    scope.dropDownDefault = {
        status: scope.dropDownList.status[0], //状态
    }

    //获取货主
    dropDownService.getCustomerInfo()
        .then(function (result) {
            if (result.success) {
                scope.dropDownList.shipper = result.data;
                scope.dropDownList.shipper.unshift(scope.constant.defaultDropDownNullData); //插入空值
                scope.dropDownDefault.shipper = scope.dropDownList.shipper[0];  //设置默认值
            } else {
                scope.Tools.alert(result.message);
            }
        })

    //获取车型
    dropDownService.getVehicleSpec()
        .then(function (result) {
            if (result.success) {
                scope.dropDownList.vehicleSpec = result.data;
                scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
                scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
            } else {
                scope.Tools.alert(result.message);
            }
        })
    //列表查询接口参数构造
    scope.listParam = function () {

        if (!scope.search) {
            scope.search = {};
        }

        scope.search.pageSize = scope.pageSize;    //设置最大页
        if (!scope.fristSelect) { //不是第一次查询 则设置当前页
            scope.search.pageNo = scope.pageNo;
        } else {  //第一次查询设置为　第一页
            scope.search.pageNo = scope.constant.pageNo;
        }

        if (scope.otherForm) {    //展开了更多
            scope.search.oddVehicleSpecName = scope.dropDownDefault.vehicleSpec.name;   //车型
            scope.search.odCustomerName = scope.dropDownDefault.shipper.customerId ? scope.dropDownDefault.shipper.customerId.name : '';   //货主
            scope.search.odStatus = scope.dropDownDefault.status.value;    //订单状态
            return scope.search;
        } else {  //没有展开
            return {
                odWaybillNo: scope.search.odWaybillNo, //单据号
                pageSize: scope.search.pageSize,   //最大号
                pageNo: scope.search.pageNo    //当前页
            }
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function (params) {
        //设置分页参数
        if (!params) {
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

        inStorageBillService.inStorageList(params)
            .then(function (result) {
                if (result.success) {
                    scope.inStorageList = result.data.asnOrderList;
                    if (scope.fristSelect) {
                        scope.Tools.page(document.getElementById('pages'), {
                            pages: result.data.wmsInboundOrderBO.totalPage,
                            curr: result.data.wmsInboundOrderBO.pageNo,
                            count: result.data.wmsInboundOrderBO.totalRecord
                        }, function (obj, first) {
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if (!first) { //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    } else {
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }

                } else {
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //回车按下查询
    scope.searchListKeyDown = function (e) {
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }

    //查询 click
    scope.searchClick = function () {
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function () {
        scope.search = {};
        scope.dropDownDefault.shipper = scope.dropDownList.shipper[0];
        scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
        scope.dropDownDefault.status = scope.dropDownList.status[0];
    }

    //刷新 click
    scope.refreshClick = function () {
        scope.searchClick();
    }

}]).controller('inStorageBillCtrl.dialog', ['$scope', 'inStorageBillService', function (scope, inStorageBillService) {   //入库管理ctrl dialog

    //新增或者更新入库单
    scope.saveOrUpdateReceiptClick = function () {
        //货主
        if (scope.ngDialogData.dropDownDefault.shipper) {
            if (scope.ngDialogData.dropDownDefault.shipper.customerId) {  //判断有没有货主
                scope.ngDialogData.receipt.odCustomerCode = scope.ngDialogData.dropDownDefault.shipper.customerId.code;
                scope.ngDialogData.receipt.odCustomerName = scope.ngDialogData.dropDownDefault.shipper.customerId.name;
            }
        }
        //单据类型
        scope.ngDialogData.receipt.odWaybillType = scope.constant.billsType.manualEntry;  //默认写死传递数据 手工录入

        //入库仓库
        if (scope.ngDialogData.dropDownDefault.warehouse) {
            scope.ngDialogData.receipt.odWhId = scope.ngDialogData.dropDownDefault.warehouse.value;
            scope.ngDialogData.receipt.odWhCode = scope.ngDialogData.dropDownDefault.warehouse.name;
        }

        //车型
        if (scope.ngDialogData.dropDownDefault.vehicleSpec) {
            scope.ngDialogData.receipt.oddVehicleSpecCode = scope.ngDialogData.dropDownDefault.vehicleSpec.code;
            scope.ngDialogData.receipt.oddVehicleSpecName = scope.ngDialogData.dropDownDefault.vehicleSpec.name;
        }
        /*

        //品牌
        if(scope.ngDialogData.dropDownDefault.brand){
            scope.ngDialogData.receipt.asndtBrandCode = scope.ngDialogData.dropDownDefault.brand.value;
            scope.ngDialogData.receipt.asndtBrandName = scope.ngDialogData.dropDownDefault.brand.name;
        }*/

        //判断是否有id 如果没有id则为新增、有则为修改
        if (scope.ngDialogData.receipt.odId) {   //修改
            //更新入库单
            inStorageBillService.updateAsnOrder(scope.ngDialogData.receipt)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.alert('更新入库单成功');
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        } else {  //新增
            //新增入库单
            inStorageBillService.addReceipt(scope.ngDialogData.receipt)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.alert('新增入库单成功');
                        scope.$parent.fristSelect = true;   //设置为第一次查询
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }
}])
    .controller('inputWorkCtrl', ['$scope', 'inStorageBillService', 'stockUnitStrategyService', 'dropDownService', 'Restangular', function (scope, inStorageBillService, stockUnitStrategyService, dropDownService, restangular) {  //入库作业

        //收货质检 click - 君马
        scope.receivingQualityTestingClick = function () {

            scope.Tools.dialog({
                controller: 'inputWorkCtrl.dialog',
                template: 'templates/inStorageManagement/inputWork/receiving-quality-testing-form.html',
                closeByDocument: false,
                closeByEscape: false,
                data: {
                    /*quality : angular.copy(inputWorkList[0]),  //质检对象*/
                    inspectionResults: scope.Tools.dropDown.inspectionResultsDropDown, //检验结果
                    dropDownDefault: {
                        qualityResult: scope.Tools.dropDown.inspectionResultsDropDown[0]
                    }
                },
                width: 600,
                title: '收货质检',
                scope: scope
            })
        }

        //西南库的收货质检
        scope.receivingQualityTestingXnClick = function () {
            scope.Tools.dialog({
                controller: 'inputWorkCtrl.dialog',
                template: 'templates/inStorageManagement/inputWork/receiving-quality-testing-xn-form.html',
                closeByDocument: false,
                closeByEscape: false,
                data: {},
                width: 600,
                title: '收货质检',
                scope: scope
            })
        }

        //打印二维码
        scope.printQrCodeClick = function () {

            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var printQrCodeList = scope.Tools.getDataTOIndex(indexArr, scope.inputWorkList);

            if (printQrCodeList.length == 0) {
                scope.Tools.alert('请选择需要打印的数据');
                return;
            }

            //判断数据是否是已分配的数据
            for (var i = 0; i < printQrCodeList.length; i++) {
                if (printQrCodeList[i].odStatus == scope.constant.orderStatus.wmsInboundCreate) {
                    scope.Tools.alert('未收货无法打印');
                    return;
                }
            }

            inStorageBillService.printBarcode(printQrCodeList)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.dialog({
                            controller: 'inputWorkCtrl.dialog',
                            template: 'templates/inStorageManagement/inputWork/print-qr-code.html',
                            closeByDocument: false,
                            closeByEscape: false,
                            data: {
                                printList: result.data
                            },
                            width: 500,
                            title: '自动分配',
                            scope: scope
                        })
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        };

        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function () {
            scope.otherForm = !scope.otherForm;
            if (scope.otherForm) {
                scope.moreText = scope.constant.moreText.packUp;
            } else {
                scope.moreText = scope.constant.moreText.spread;
            }
        }

        /*状态数据*/
        scope.dropDownList = {
            status: scope.Tools.dropDown.orderStatusDropDown.concat(),    //订单状态
            billsType: scope.Tools.dropDown.billsTypeDropDown.concat(),    //单据类型
            inspectionResults: scope.Tools.dropDown.inspectionResultsDropDown.concat() //检验结果
        }
        scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData) //插入订单空数据
        scope.dropDownList.billsType.unshift(scope.constant.defaultDropDownNullData) //插入订单空数据
        scope.dropDownList.inspectionResults.unshift(scope.constant.defaultDropDownNullData) //插入订单空数据

        //设置默认值
        scope.dropDownDefault = {
            billsType: scope.dropDownList.status[0], //单据类型
            status: scope.dropDownList.billsType[0], //状态
            inspectionResults: scope.dropDownList.inspectionResults[0] //检验结果
        }

        //获取货主
        dropDownService.getCustomerInfo()
            .then(function (result) {
                if (result.success) {
                    scope.dropDownList.shipper = result.data;
                    scope.dropDownList.shipper.unshift(scope.constant.defaultDropDownNullData); //插入空值
                    scope.dropDownDefault.shipper = scope.dropDownList.shipper[0];  //设置默认值
                } else {
                    scope.Tools.alert(result.message);
                }
            })

        //获取车型
        dropDownService.getVehicleSpec()
            .then(function (result) {
                if (result.success) {
                    scope.dropDownList.vehicleSpec = result.data;
                    scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
                    scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
                } else {
                    scope.Tools.alert(result.message);
                }
            })

        //列表查询接口参数构造
        scope.listParam = function () {

            if (!scope.search) {
                scope.search = {};
            }

            scope.search.pageSize = scope.pageSize;    //设置最大页
            if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            } else {  //第一次查询设置为　第一页
                scope.search.pageNo = scope.constant.pageNo;
            }

            if (scope.otherForm) {    //展开了更多
                scope.search.oddVehicleSpecName = scope.dropDownDefault.vehicleSpec.wndVehicleName;   //车型
                scope.search.odCustomerName = scope.dropDownDefault.shipper ? scope.dropDownDefault.shipper.wnhCustomerName : '';   //货主
                scope.search.odWaybillType = scope.dropDownDefault.billsType.value;   //单据类型
                scope.search.odStatus = scope.dropDownDefault.status.value;    //订单状态
                scope.search.odCheckResult = scope.dropDownDefault.inspectionResults.value;//检验结果
                return scope.search;
            } else {  //没有展开
                return {
                    odWaybillNo: scope.search.odWaybillNo, //单据号
                    pageSize: scope.search.pageSize,   //最大号
                    pageNo: scope.search.pageNo    //当前页
                }
            }
        };

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo

            inStorageBillService.inStorageList(params)
                .then(function (result) {
                    if (result.success) {
                        scope.inputWorkList = result.data.asnOrderList;
                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.data.wmsInboundOrderBO.totalPage,
                                curr: result.data.wmsInboundOrderBO.pageNo,
                                count: result.data.wmsInboundOrderBO.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if (!first) { //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }

                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //列表
        scope.list();

        //回车按下查询
        scope.searchListKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
                e.preventDefault();
            }
        }

        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        }

        //重置 click
        scope.resetClick = function () {
            scope.search = {};
            scope.dropDownDefault.shipper = scope.dropDownList.shipper[0];
            scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
            scope.dropDownDefault.status = scope.dropDownList.status[0];
            scope.dropDownDefault.billsType = scope.dropDownList.billsType[0];
            scope.dropDownDefault.inspectionResults = scope.dropDownList.inspectionResults[0];
        }

        //刷新 click
        scope.refreshClick = function () {
            scope.searchClick();
        }

        //确认入库
        scope.warehousingConfirmClick = function () {

            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var warehousingConfirmList = scope.Tools.getDataTOIndex(indexArr, scope.inputWorkList);

            //如果选择了数据做判断
            if (warehousingConfirmList.length > 0) {
                if (warehousingConfirmList.length > 1) {
                    scope.Tools.alert('只能选择一条入库的数据');
                    return;
                }

                //判断数据是否是待入库的数据
                for (var i = 0; i < warehousingConfirmList.length; i++) {
                    if (warehousingConfirmList[i].odStatus != scope.constant.orderStatus.wmsInboundWaitAllocation && warehousingConfirmList[i].odStatus != scope.constant.orderStatus.wmsInboundCancel) {
                        scope.Tools.alert('只能选择已收货或已取消的数据');
                        return;
                    }
                }
            }

            scope.Tools.dialog({
                controller: 'inputWorkCtrl.dialog',
                template: 'templates/inStorageManagement/inputWork/input-confirm.html',
                closeByDocument: false,
                closeByEscape: false,
                data: {
                    inputConfirm: angular.copy(warehousingConfirmList[0]),   //确认入库 data
                    vin: warehousingConfirmList[0] ? warehousingConfirmList[0].oddVin : null,  //vin码
                    allocation: false  //是否分配库位
                },
                width: 550,
                title: '入库确认',
                scope: scope
            })
        }

        //取消入库
        scope.cancelWarehousingClick = function () {

            var indexArr = scope.Tools.getTableSelectItemIndex('.inputWorkTable');  //获取table所选的下标

            var warehousingConfirmList = scope.Tools.getDataTOIndex(indexArr, scope.inputWorkList);

            if (warehousingConfirmList.length == 0) {
                scope.Tools.alert('请选择需要取消入库的数据');
                return;
            }

            if (warehousingConfirmList.length > 1) {
                scope.Tools.alert('只能选择一条取消入库的数据');
                return;
            }

            //判断数据是否是待入库的数据
            for (var i = 0; i < warehousingConfirmList.length; i++) {
                if (warehousingConfirmList[i].odStatus != scope.constant.orderStatus.wmsInboundFinish) {
                    scope.Tools.alert('只能选择已入库的数据');
                    return;
                }
            }

            var strArr = new Array();
            for (var i = 0; i < warehousingConfirmList.length; i++) {
                strArr.push(warehousingConfirmList[i]);
            }
            //取消入库
            scope.Tools.dialogConfirm('确定要将 ' + warehousingConfirmList[0].oddVin + ' 取消入库？')
                .then(function (res) {
                    if (res) {
                        inStorageBillService.cancelInbound(warehousingConfirmList[0])
                            .then(function (result) {
                                if (result.success) {
                                    scope.Tools.alert('取消入库成功');
                                    scope.list(scope.listParam());
                                } else {
                                    scope.Tools.alert(result.message);
                                }
                            })
                    }
                })
        }

        //开闸测试
        scope.openDoorClick = function () {
            restangular.allUrl('open', 'http://127.0.0.1:8080/open').customPOST({
                chnnel: '001',
                open: 1,
                led1: '开闸test1',
                led2: '开闸test2',
                sound: '闸门已开请通行'
            }, undefined, undefined, {'Content-Type': 'text/plain;charset=UTF-8'})
                .then(function (result) {
                    console.log(result);
                })
        }
    }])
    .controller('inputWorkCtrl.dialog', ['$scope', 'inStorageBillService', 'gateService', function (scope, inStorageBillService, gateService) {   //入库作业 dialog

        //查找收货入库单 keydown -- 君马
        scope.searchQualityKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                if (scope.ngDialogData.queryParam) {
                    if (scope.ngDialogData.queryParam.length != 17) {
                        scope.Tools.alert('请输入正确的17位vin码');
                        return;
                    }
                    inStorageBillService.getInBoundInfoByVin(scope.ngDialogData.queryParam)
                        .then(function (result) {
                            if (result.success) {
                                scope.ngDialogData.quality = result.data;
                            } else {
                                scope.Tools.alert(result.message);
                            }
                        })
                }
                e.preventDefault();
            }
        }

        //西南库查找收货入库单 keydown
        scope.searchQualityXnKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                if (scope.ngDialogData.queryParam) {
                    if (scope.ngDialogData.queryParam.length != 17) {
                        scope.Tools.alert('请输入正确的17位vin码');
                        return;
                    }
                    inStorageBillService.barcodePrint(scope.ngDialogData.queryParam, scope.userInfo.id)
                        .then(function (result) {
                            if (result.success) {
                                scope.ngDialogData.quality = result.data;
                                scope.Tools.alert('收货入库成功');

                                if (!scope.Tools.checkPrintInstall()) {   //打印插件检测
                                    scope.Tools.downLoadPrintPlugIn();    //提示下载打印插件
                                    return;
                                }

                                scope.Tools.dialogConfirm('该车辆【' + scope.ngDialogData.queryParam + '】收货已完成，确定后打印二维码')
                                    .then(function (res) {
                                        if (res) {
                                            scope.Tools.printQr(result.data.odRemark, result.data.asnOrderDetailDTOList[0].oddVin, result.data.asnOrderDetailDTOList[0].oddWhLocName, result.data.odWaybillNo, 2, scope.constant.userWarehouseType.UNLCN_XN_);
                                        }
                                    })
                                scope.list(scope.listParam());

                                //入库类型查询 并打开闸门   --- 库存区10-- 入库10
                                var params = {
                                    gateType: 10,
                                    inOutType: 10
                                };

                                gateService.getByType(params)
                                    .then(function (result) {
                                        if (result.success) {
                                            gateService.open(result.data.channel, scope.constant.gateController.open, scope.ngDialogData.queryParam + '-收货质检成功，闸门已打开', scope.ngDialogData.queryParam + '-收货质检成功，闸门已打开', scope.ngDialogData.queryParam + '-收货质检成功，闸门已打开')
                                                .success(function (data, status, headers, config) {
                                                    scope.Tools.alert('闸门打开成功');
                                                })
                                                .error(function (data, status, headers, config) {
                                                    scope.Tools.alert('闸门打开失败');
                                                })
                                        } else {
                                            scope.Tools.alert(result.message);
                                        }
                                    })
                            } else {
                                scope.ngDialogData.queryParam = '';
                                scope.Tools.alert(result.message);
                            }
                        })
                }
                e.preventDefault();
            }
        }

        //自动加载库区库位
        scope.loadZoneLoc = function () {
            //查询库区
            inStorageBillService.reservoirList(scope.userInfo.whCode)
                .then(function (result) {
                    if (result.success) {
                        scope.ngDialogData.reservoirDropArr = result.data;
                        scope.ngDialogData.dropDownDefault.reservoirDrop = result.data[0];
                        scope.selectReservoirDropChang(scope.ngDialogData.dropDownDefault.reservoirDrop);
                        //设置为手动分配
                        scope.ngDialogData.alType = scope.constant.distributionType.manual;
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //选择库区
        scope.selectReservoirDropChang = function (item) {
            inStorageBillService.storageLocationList(scope.userInfo.whCode, item.locZoneId)
                .then(function (result) {
                    if (result.success) {
                        scope.ngDialogData.storageLocationDropArr = result.data;
                        scope.ngDialogData.dropDownDefault.storageLocationDrop = scope.ngDialogData.storageLocationDropArr[0];
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //收货质检成功
        scope.receivingConfirmaClick = function () {

            if (!scope.ngDialogData.quality || !scope.ngDialogData.quality.odId) {
                scope.Tools.alert('请输入VIN码查询数据');
                return;
            }

            var obj = {
                oddVin: scope.ngDialogData.quality.oddVin, //vin
                odId: scope.ngDialogData.quality.odId,  //id
                odCheckResult: scope.ngDialogData.dropDownDefault.qualityResult.value,   //检验结果
                odCheckDesc: scope.ngDialogData.quality.odCheckDesc, //检验描述
                userId: scope.userInfo.id
            }

            //收货质检
            inStorageBillService.deliverToConfirm(obj)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.alert('收货质检成功');
                        //在当前页刷新
                        scope.list(scope.listParam());
                        scope.ngDialogData.quality = {};
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //根据vin查询
        scope.searchByVinKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                inStorageBillService.queryByVin(scope.ngDialogData.vin)
                    .then(function (result) {
                        if (result.success) {
                            scope.ngDialogData.inputConfirm = result.data;
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
                e.preventDefault();
            }
        }
        scope.confirmFlag = true
        scope.showHandle = function () {
            scope.Tools.alert('处理中...');
        }
        //入库确认 click
        scope.inputConfirmClick = function () {

            var obj = {
                alOdId: scope.ngDialogData.inputConfirm.odId  //订单id
            }

            //判断是否点了库位分配
            if (scope.ngDialogData.allocation) {

                if (!scope.ngDialogData.dropDownDefault) {    //判断下拉默认值是否为空
                    scope.ngDialogData.dropDownDefault = {};    //创建对象
                }

                if (!scope.ngDialogData.dropDownDefault.reservoirDrop || !scope.ngDialogData.dropDownDefault.storageLocationDrop) {
                    scope.Tools.alert('请选择库区和库位');
                    return;
                }

                obj.alTargetZoneId = scope.ngDialogData.dropDownDefault.reservoirDrop.locZoneId;    //库区id
                obj.alTargetZoneCode = scope.ngDialogData.dropDownDefault.reservoirDrop.locZoneCode;    //库区code
                obj.alTargetZoneName = scope.ngDialogData.dropDownDefault.reservoirDrop.locZoneCode;    //库区名称
                obj.alTargetLocId = scope.ngDialogData.dropDownDefault.storageLocationDrop.locId;   //库位id
                obj.alTargetLocCode = scope.ngDialogData.dropDownDefault.storageLocationDrop.locCode;  //库位code
                obj.alTargetLocName = scope.ngDialogData.dropDownDefault.storageLocationDrop.locName;    //库位名字
            } else {  //没有点击
                obj.alTargetZoneId = scope.ngDialogData.inputConfirm.oddWhZoneId;    //库区id
                obj.alTargetZoneCode = scope.ngDialogData.inputConfirm.oddWhZoneCode;    //库区code
                obj.alTargetZoneName = scope.ngDialogData.inputConfirm.oddWhZoneName;    //库区名称
                obj.alTargetLocId = scope.ngDialogData.inputConfirm.oddWhLocId;   //库位id
                obj.alTargetLocCode = scope.ngDialogData.inputConfirm.oddWhLocCode;  //库位code
                obj.alTargetLocName = scope.ngDialogData.inputConfirm.oddWhLocName;    //库位名字
            }

            scope.Tools.dialogConfirm('是否完成入库确定？')
                .then(function (res) {
                    if (res) {
                        scope.confirmFlag = false
                        inStorageBillService.confirmInbound(obj, scope.userInfo.id)
                            .then(function (result) {
                                scope.confirmFlag = true
                                if (result.success) {
                                    scope.Tools.alert('确认入库成功');
                                    //在当前页刷新
                                    scope.list(scope.listParam());
                                    scope.closeThisDialog();
                                } else {
                                    scope.Tools.alert(result.message);
                                }
                            })
                    }
                })
        }

        //库位分配 click
        scope.stockAllocationClick = function () {
            //查询库区
            inStorageBillService.reservoirList(scope.userInfo.whCode)
                .then(function (result) {
                    if (result.success) {

                        var whZoneDefault; //库区默认值
                        if (scope.ngDialogData.inputConfirm.oddWhZoneCode) {    //判断原来的数据是否有库区
                            //设置库区默认值
                            for (var i = 0; i < result.data.length; i++) {
                                if (result.data[i].locZoneCode == scope.ngDialogData.inputConfirm.oddWhZoneCode) {
                                    whZoneDefault = result.data[i];
                                    break;
                                }
                            }
                        }

                        //查询库位
                        inStorageBillService.storageLocationList(whCode, whZoneDefault ? whZoneDefault.locZoneId : result.data[0].locZoneId)
                            .then(function (localResult) {
                                if (localResult.success) {
                                    var whLocalDefault; //库位默认值
                                    //判断原来的数据是否有库位
                                    if (scope.ngDialogData.inputConfirm.oddWhLocCode) {
                                        //设置库位默认值
                                        for (var i = 0; i < localResult.data.length; i++) {
                                            if (localResult.data[i].locCode == scope.ngDialogData.inputConfirm.oddWhLocCode) {
                                                whLocalDefault = localResult.data[i];
                                                break;
                                            }
                                        }
                                    }

                                    scope.ngDialogData.reservoirDropArr = result.data; //库区
                                    scope.ngDialogData.storageLocationDropArr = localResult.data;  //库位
                                } else {
                                    scope.Tools.alert(result.message);
                                }
                            })
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
            scope.ngDialogData.allocation = true;   //设置分配为true
        }

        //收货数据清空 click
        scope.receivingClearClick = function () {
            if (scope.ngDialogData.quality) {
                scope.ngDialogData.quality = null;
                scope.ngDialogData.queryParam = '';
                scope.Tools.alert('收货确认成功');
            }
        }

        //打印所有的二维码
        scope.printAllQrClick = function () {

            if (!scope.Tools.checkPrintInstall()) {   //打印插件检测
                scope.Tools.downLoadPrintPlugIn();    //提示下载打印插件
                return;
            }

            angular.forEach(scope.ngDialogData.printList, function (v, k) {
                scope.Tools.printQr(v.barcode, v.vin, v.locNo, v.asnOrderBillNo, 2, scope.constant.userWarehouseType.UNLCN_XN_);
            })
        }
    }])
    .controller('inputWorkReceivingQualityCtrl', ['$scope', 'inStorageBillService', 'junmaGateService', function (scope, inStorageBillService, junmaGateService) {   //收货质检
        //君马收货质检查询处理
        scope.searchVinKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                if (scope.queryParam) {
                    var obj = {
                        oddVin: scope.queryParam,  //vin
                        userId: scope.userInfo.id  //用户id
                    }
                    inStorageBillService.deliverToConfirm(obj)
                        .then(function (result) {
                            if (result.success) {
                                scope.quality = result.data;
                                scope.Tools.alert('收货质检成功');
                                //     开启君马匝门 入库类型查询 并打开闸门 ---    1-出库   -- 2-入库

                                var params = {
                                    CardNo: scope.queryParam,
                                    MacNo: 2
                                };

                                junmaGateService.open(params)
                                    .success(function (data, status, headers, config) {
                                        scope.Tools.alert('闸门打开成功');
                                    })
                                    .error(function (data, status, headers, config) {
                                        scope.Tools.alert('闸门打开失败');
                                    })

                            } else {
                                scope.Tools.alert(result.message);
                            }
                            scope.queryParam = '';  //清空
                        })
                }
                e.preventDefault();
            }
        }

        //收货质检 change
        scope.searchVinKeyChange = function (e) {
            if (scope.queryParam) {
                inStorageBillService.deliverToConfirm2(scope.queryParam)
                    .then(function (result) {
                        if (result.success) {
                            scope.quality = result.data;
                            scope.Tools.alert('收货质检成功');
                        } else {
                            scope.Tools.alert(result.message);
                        }
                        scope.queryParam = '';  //清空
                    })
            }
        }
    }])
    .controller('inventoryRecordsQueryCtrl', ['$scope', 'dropDownService', 'inStorageBillService', 'ngDialog', function (scope, dropDownService, inStorageBillService, ngDialog) {   //入库记录查询 controller

        //导出
        scope.exports = function () {
            ngDialog.open({
                template: "templates/inStorageManagement/inventoryRecordsQuery/export.html",
                controller: "inventoryRecordsQueryCtrl.dialog",
                title: '导出入库记录',
                width: 590
            });
        };
        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function () {
            scope.otherForm = !scope.otherForm;
            if (scope.otherForm) {
                scope.moreText = scope.constant.moreText.packUp;
            } else {
                scope.moreText = scope.constant.moreText.spread;
            }
        }

        scope.dropDownList = {};    //下拉
        scope.dropDownDefault = {}; //下拉默认值

        //获取车型
        dropDownService.getVehicleSpec()
            .then(function (result) {
                if (result.success) {
                    scope.dropDownList.vehicleSpec = result.data;
                    scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
                    scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
                } else {
                    scope.Tools.alert(result.message);
                }
            })

        scope.search = {};
        //列表查询接口参数构造
        scope.listParam = function () {

            if (!scope.search) {
                scope.search = {};
            }
            scope.search.pageSize = scope.pageSize;    //设置最大页
            if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            } else {  //第一次查询设置为　第一页
                scope.search.pageNo = scope.constant.pageNo;
            }

            if (scope.otherForm) {    //展开了更多
                scope.search.odd_vehicle_spec_name = scope.dropDownDefault.vehicleSpec.wndVehicleName;
                return scope.search;
            } else {  //没有展开
                return {};
            }
        }

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

            inStorageBillService.inboundRecord(angular.toJson(params))
                .then(function (result) {
                    if (result.success) {
                        scope.inventoryRecordsList = result.data.inboundRecord;
                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.data.wmsInboundRecordBO.totalPage,
                                curr: result.data.wmsInboundRecordBO.pageNo,
                                count: result.data.wmsInboundRecordBO.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                if (!first) { //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }

                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //列表
        scope.list();

        //回车按下查询
        scope.searchListKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
                e.preventDefault();
            }
        }

        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        }

        //重置 click
        scope.resetClick = function () {
            scope.search = {};
            scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
        }

        //刷新 click
        scope.refreshClick = function () {
            scope.searchClick();
        }
    }])
    .controller('inventoryRecordsQueryCtrl.dialog', ['$scope', 'inventoryRecordsQueryservice', '$timeout', 'ngDialog', function (scope, inventoryRecordsQueryservice, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.search = {
            recieveStartTime: '',
            recieveEndTime: '',
            whCode: scope.userInfo.whCode,
            userId: scope.userInfo.id
        };
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k];
            }
            return str;
        };
        scope.getData = function (e) {
            if (scope.search.recieveStartTime && scope.search.recieveStartTime) {
                inventoryRecordsQueryservice.queryRecordCount(scope.formatData(scope.search)).then(function (res) {
                    if (res.success == false) {
                        e.preventDefault();
                        scope.Tools.alert(res.message)
                    } else {
                        scope.excelUrl = contextPath + 'asnOrder/recordImportExcel' + scope.formatData(scope.search);
                        $timeout(function () {
                            document.getElementById('irqExport').click();
                            scope.search.recieveStartTime = '';
                            scope.search.recieveEndTime = '';
                            ngDialog.closeAll();
                        }, 20)
                    }
                });

            } else {
                e.preventDefault();
                scope.Tools.alert('有时间未填！')
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }])
    .controller('inventorySelectCtrl', ['$scope', '$rootScope', '$timeout', 'inventorySelectService', 'dropDownService', 'constant', 'ngDialog',
        function (scope, rootScope, $timeout, inventorySelectService, dropDownService, constant, ngDialog) { //库存查询 controller
            //替换库位code
            scope.openChangeLocCode = function () {
                var checkedArray = [];
                $('.wms_wbm_inventory_select').find('td i').each(function (index, obj) {
                    if ($(obj).parent().hasClass('layui-form-checked')) {   //判断是否选中
                        checkedArray.push($(obj));    //获取index
                    }
                });
                var al = checkedArray.length;
                if (al === 1) {
                    ngDialog.open({
                        template: 'templates/inventoryManagement/inventorySelect/change_loc_code.html',
                        controller: 'inventorySelectChangeLocCodeCtrl.dialog',
                        data: {
                            checkedItem: checkedArray[0].parents('tr').attr('item')
                        },
                        title: '替换库位CODE',
                        width: 350
                    });
                    rootScope.reloadInventoryList={
                        reload:function (params) {
                            scope.list(params);
                        }
                    }
                } else if (al < 1) {
                    scope.Tools.alert("请选择要替换的数据!");
                } else {
                    scope.Tools.alert("只能选择一条数据替换");
                }
            };

            scope.isSearch = {
                whCode: scope.userInfo.whCode,
                userId: scope.userInfo.id
            };
            scope.excelUrl = '';

            scope.formatData = function (obj) {
                var str = '?';
                for (var k in obj) {
                    str += '&' + k + '=' + obj[k];
                }
                return str;
            };
            //导出
            scope.export = function () {
                inventorySelectService.queryInventoryRecordCount(scope.formatData(scope.isSearch)).then(function (res) {
                    if (res.success === false) {
                        scope.Tools.alert(res.message)
                    } else {
                        scope.excelUrl = contextPath + 'wmsInventory/inventoryRecordImport' + scope.formatData(scope.isSearch);
                        $timeout(function () {
                            document.getElementById('islExport').click();
                        }, 20)
                    }
                });
            }

            // 冻结订单
            scope.freezen = function () {
                var indexArr = scope.Tools.getTableSelectItemIndex('.wms_wbm_inventory_select');  //获取table所选的下标
                var l = indexArr.length
                if (l === 1) {
                    if (parseInt(scope.inventorySelectList[indexArr[0]].status) === 10) {
                        inventorySelectService.stockFreeze({
                            invlocIds: scope.inventorySelectList[indexArr[0]].invlocId
                        }).then(function (res) {
                            scope.list();
                            setTimeout(function () {
                                scope.Tools.alert(res.message)
                            }, 200)
                        })
                    } else {
                        scope.Tools.alert('只有正常在库的单才能冻结！')
                    }

                } else if (l === 0) {
                    scope.Tools.alert('请选择！')
                } else if (l > 1) {
                    scope.Tools.alert('目前只支持 单条 冻结!')
                }
            }

            // 解冻
            scope.thaw = function () {
                var indexArr = scope.Tools.getTableSelectItemIndex('.wms_wbm_inventory_select');  //获取table所选的下标
                var l = indexArr.length
                if (l >= 1) {
                    if (parseInt(scope.inventorySelectList[indexArr[0]].status) === 60) {
                        var temp = [];
                        for (var i = 0; i < l; i++) {
                            temp.push(scope.inventorySelectList[indexArr[i]].invlocId)
                        }
                        inventorySelectService.stockNormal({
                            invlocIds: temp.toString()
                        }).then(function (res) {
                            scope.list();
                            setTimeout(function () {
                                scope.Tools.alert(res.message)
                            }, 200)
                        })
                    } else {
                        scope.Tools.alert('只有库存冻结的单才能解冻！')
                    }

                } else if (l === 0) {
                    scope.Tools.alert('请选择！')
                }
            }
            //更多条件
            scope.moreText = scope.constant.moreText.spread;
            scope.moreClick = function () {
                scope.otherForm = !scope.otherForm;
                if (scope.otherForm) {
                    scope.moreText = scope.constant.moreText.packUp;
                } else {
                    scope.moreText = scope.constant.moreText.spread;
                }
            }


            //下拉
            scope.dropDownList = {  //下拉
            }

            scope.dropDownDefault = {   //下拉默认值
            }

            //获取货主
            dropDownService.getCustomerInfo()
                .then(function (result) {
                    if (result.success) {
                        scope.dropDownList.shipper = result.data;
                        scope.dropDownList.shipper.unshift(scope.constant.defaultDropDownNullData); //插入空值
                        scope.dropDownDefault.shipper = scope.dropDownList.shipper[0];  //设置默认值
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })

            //获取仓库
            /*dropDownService.getWarehouse()
                .then(function(result){
                    if(result.success){
                        scope.dropDownList.warehouse = result.data;
                        scope.dropDownList.warehouse.unshift(scope.constant.defaultDropDownNullData);//插入仓库空数据
                        scope.dropDownDefault.warehouse = scope.dropDownList.warehouse[0]; //仓库默认值
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })*/

            //获取车型
            dropDownService.getVehicleSpec()
                .then(function (result) {
                    if (result.success) {
                        scope.dropDownList.vehicleSpec = result.data;
                        scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
                        scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })

            scope.isTemp = {
                "invVin": "",
                "colourCode": "",
                "materialCode": ""
            }
            // 判断是君马库
            if (rootScope.userInfo.whCode.indexOf(constant.userWarehouseType.JM_) !== -1) {
                //获取颜色list
                dropDownService.getColorListInventorySelect(scope.isTemp)
                    .then(function (result) {
                        if (result.success) {
                            scope.dropDownList.ColorListInventorySelect = result.data;
                            scope.dropDownList.ColorListInventorySelect.unshift(scope.constant.defaultDropDownNullData); //插入空值
                            scope.dropDownDefault.ColorListInventorySelect = scope.dropDownList.ColorListInventorySelect[0];  //设置默认值
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
                //获取材料
                dropDownService.getMaterialListInventorySelect(scope.isTemp)
                    .then(function (result) {
                        if (result.success) {
                            scope.dropDownList.MaterialListInventorySelect = result.data;
                            scope.dropDownList.MaterialListInventorySelect.unshift(scope.constant.defaultDropDownNullData); //插入空值
                            scope.dropDownDefault.MaterialListInventorySelect = scope.dropDownList.MaterialListInventorySelect[0];  //设置默认值
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            }

            scope.dropDownList.invStatus = [
                {name: '正常在库', value: '10'},
                {name: '发运锁定', value: '20'},
                {name: '借用锁定', value: '30'},
                {name: '维修锁定', value: '40'},
                {name: '库存冻结', value: '60'}
            ]
            scope.search = {};
            //列表条件参数
            scope.listParam = function () {
                if (!scope.search) {
                    scope.search = {};
                }
                scope.search.pageSize = scope.pageSize;    //设置最大页
                if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                    scope.search.pageNo = scope.pageNo;
                } else {  //第一次查询设置为　第一页
                    scope.search.pageNo = scope.constant.pageNo;
                }
                if (scope.otherForm) {    //展开了更多
                    var aT = scope.dropDownDefault.vehicleSpec;
                    var dT = scope.dropDownDefault.invStatus;
                    scope.search.invCustomerName = scope.dropDownDefault.shipper.customerId ? scope.dropDownDefault.shipper.wndCustomerName : '';   //货主
                    scope.search.invVehicleSpecName = aT ? aT.wndVehicleName : ''; //车型
                    scope.search.invStatus = dT ? dT.value : '';   // 库存状态
                    return scope.search;
                } else {
                    return scope.search;
                }
            }

            //列表
            scope.fristSelect = true;   //判断是否是第一次查询
            scope.list = function (params) {
                //设置分页参数
                if (!params) {
                    params = {};
                }
                params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
                params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

                inventorySelectService.list(params)
                    .then(function (result) {
                        if (result.success) {
                            scope.inventorySelectList = result.data.wmsInventoryList;
                            if (scope.fristSelect) {
                                scope.Tools.page(document.getElementById('pages'), {
                                    pages: result.data.wmsInventoryBO.totalPage,
                                    curr: result.data.wmsInventoryBO.pageNo,
                                    count: result.data.wmsInventoryBO.totalRecord
                                }, function (obj, first) {
                                    scope.fristSelect = first;  //设置状态
                                    //初始化多选框
                                    scope.Tools.initCheckBox();
                                    if (!first) { //判断不是第一次加载
                                        var param = scope.listParam();
                                        param.pageNo = obj.curr;    //当前页
                                        param.pageSize = obj.limit; //最大页
                                        scope.pageSize = obj.limit; //设置最大页
                                        scope.pageNo = obj.curr; //设置当前页
                                        scope.list(param);
                                    }
                                })
                            } else {
                                //初始化多选框
                                scope.Tools.initCheckBox();
                            }
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            }

            //列表
            scope.list();

            //查询 click
            scope.searchClick = function () {
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
            }

            //重置 click
            scope.resetClick = function () {
                scope.search = {};
                scope.dropDownDefault.shipper = scope.dropDownList.shipper[0];
                scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
                scope.dropDownDefault.invStatus = {}
                /*scope.dropDownDefault.warehouse = scope.dropDownList.warehouse[0];*/
            }

            //刷新 click
            scope.refreshClick = function () {
                scope.searchClick();
            }

            //回车按下查询
            scope.searchListKeyDown = function (e) {
                var keycode = window.event ? e.keyCode : e.which;
                if (keycode == 13) {  //回车键
                    scope.fristSelect = true;   //设置为第一次查询
                    scope.list(scope.listParam());
                    e.preventDefault();
                }
            }

            //详情 click
            scope.inventorySelectDetailClick = function (item) {
                inventorySelectService.detail(item.invId)
                    .then(function (result) {
                        if (result.success) {
                            scope.Tools.dialog({
                                controller: 'inventorySelectCtrl.dialog',
                                template: 'templates/inventoryManagement/inventorySelect/inventory-select-detail.html',
                                closeByDocument: false,
                                closeByEscape: false,
                                data: {
                                    inventorySelectDetailList: result.data,
                                    invVehicleName: item.invVehicleSpecName
                                },
                                width: 800,
                                title: '车辆存放详情',
                                scope: scope
                            })
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }])
    .controller('inventorySelectCtrl.dialog', ['$scope', function (scope) { //库存查询 dialog controller

    }])
    //换库位的controller
    .controller('inventorySelectChangeLocCodeCtrl.dialog', ['$scope', 'inventorySelectService', 'ngDialog', '$timeout','$rootScope',
        function (scope, inventorySelectService, ngDialog, timeout,rootScope) {    //修改车辆库位CODE ctrl dialog
            scope.search = {};
            scope.params = {};
            var checkItem = angular.fromJson(scope.ngDialogData.checkedItem);
            scope.search = {
                invlocVin: checkItem.invlocVin,
                oldLocCode: checkItem.invlocLocCode,
                oldZoneName: checkItem.invlocZoneName
            };
            scope.confirm = function () {
                scope.params = {
                    invlocVin: scope.search.invlocVin,
                    invlocId: checkItem.invlocId,
                    invlocCodeNew: scope.search.invLocCode,
                    whCode: scope.userInfo.whCode
                };
                var clickAgain = true;
                if (clickAgain) {
                    clickAgain = false;
                    inventorySelectService.changeLocCode(scope.params).then(function (res) {
                        if (res.success) {
                            clickAgain = true;
                            scope.Tools.alert(res.message);
                            //关闭弹窗
                            timeout(function () {
                                ngDialog.closeAll();
                            }, 200);
                            //刷新列表
                            rootScope.reloadInventoryList.reload({
                                    pageSize: 10,
                                    pageNo: 1
                                });
                            rootScope.reloadInventoryList={};
                        } else {
                            clickAgain = true;
                            scope.Tools.alert(res.message);
                        }
                    })
                }
            };
            scope.close = function () {
                ngDialog.closeAll()
            }
        }])
    .controller('inventorySelectExportCtrl.dialog', ['$scope', 'inventoryRecordsQueryservice', '$timeout', 'ngDialog', function (scope, inventoryRecordsQueryservice, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.search = {
            recieveStartTime: '',
            recieveEndTime: '',
            whCode: scope.userInfo.whCode,
            userId: scope.userInfo.id
        };
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k];
            }
            return str;
        };
        scope.getData = function (e) {
            if (scope.search.recieveStartTime && scope.search.recieveStartTime) {
                inventoryRecordsQueryservice.queryRecordCount(scope.formatData(scope.search)).then(function (res) {
                    if (res.success === false) {
                        e.preventDefault();
                        scope.Tools.alert(res.message)
                    } else {
                        scope.excelUrl = contextPath + 'asnOrder/recordImportExcel' + scope.formatData(scope.search);
                        $timeout(function () {
                            document.getElementById('irqExport').click();
                            scope.search.recieveStartTime = '';
                            scope.search.recieveEndTime = '';
                            ngDialog.closeAll();
                        }, 20)
                    }
                });

            } else {
                e.preventDefault();
                scope.Tools.alert('有时间未填！')
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }])

    //库存盘点入库记录查询 controller--查所有客户数据权限
    .controller('inboundAllVerificationCtrl', ['$scope', 'dropDownService', 'inventoryVerificationService', 'ngDialog', '$timeout',
        function (scope, dropDownService, inventoryVerificationService, ngDialog, $timeout) {
            scope.excelUrl = '';
            scope.formatData = function (obj) {
                var str = '?';
                for (var k in obj) {
                    str += '&' + k + '=' + obj[k];
                }
                return str;
            };
            //导出
            scope.exports = function () {
                if (!scope.search.customerCode) {
                    scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.WMS_ALL;
                }
                scope.search.userId = scope.userInfo.id;
                scope.excelUrl = contextPath + 'inventoryManager/getInboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('islExport').click();
                }, 20)
            };
            //客户名称下拉框
            scope.dropDownList = {
                customerList: [
                    {name: '江铃客户', code: 'JL'},
                    {name: '君马客户', code: 'JM'}
                ]
            };
            scope.dropDownList.customerList.unshift(scope.constant.defaultDropDownNullData); //插入空值

            scope.dropDownDefault = {
                customerList: scope.dropDownList.customerList[0]
            };

            //更多条件默认值
            scope.moreText = scope.constant.moreText.spread;
            //更多条件
            scope.moreClick = function () {
                scope.otherForm = !scope.otherForm;
                if (scope.otherForm) {
                    scope.moreText = scope.constant.moreText.packUp;
                } else {
                    scope.moreText = scope.constant.moreText.spread;
                }
            };
            scope.search = {};
            //列表查询接口参数构造
            scope.listParam = function () {

                if (!scope.search) {
                    scope.search = {};
                }
                scope.search.pageSize = scope.pageSize;    //设置最大页
                if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                    scope.search.pageNo = scope.pageNo;
                } else {  //第一次查询设置为　第一页
                    scope.search.pageNo = scope.constant.pageNo;
                }
                if (scope.otherForm) {    //展开了更多
                    scope.search.customerCode = scope.dropDownDefault.customerList.code;
                    return scope.search;
                } else {  //没有展开
                    return {};
                }
            };

            //列表
            scope.fristSelect = true;   //判断是否是第一次查询
            scope.list = function (params) {
                //设置分页参数
                if (!params) {
                    params = {};
                }

                params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
                params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;
                if (!params.customerCode) {
                    params.customerCode = scope.constant.inventoryVerificationCustomerCode.WMS_ALL;//客户默认值
                }
                inventoryVerificationService.getInboundManageList(angular.toJson(params))
                    .then(function (result) {
                        if (result.success) {
                            scope.inboundRecordsList = result.data;
                            if (scope.fristSelect) {
                                scope.Tools.page(document.getElementById('pages'), {
                                    pages: result.pageVo.totalPage,
                                    curr: result.pageVo.pageNo,
                                    count: result.pageVo.totalRecord
                                }, function (obj, first) {
                                    scope.fristSelect = first;  //设置状态
                                    if (!first) { //判断不是第一次加载
                                        var param = scope.listParam();
                                        param.pageNo = obj.curr;    //当前页
                                        param.pageSize = obj.limit; //最大页
                                        scope.pageSize = obj.limit; //设置最大页
                                        scope.pageNo = obj.curr; //设置当前页
                                        scope.list(param);
                                    }
                                })
                            } else {
                                //初始化多选框
                                scope.Tools.initCheckBox();
                            }

                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            };

            //列表
            scope.list();

            //回车按下查询
            scope.searchListKeyDown = function (e) {
                var keycode = window.event ? e.keyCode : e.which;
                if (keycode === 13) {  //回车键
                    scope.fristSelect = true;   //设置为第一次查询
                    scope.list(scope.listParam());
                    e.preventDefault();
                }
            };

            //查询 click
            scope.searchClick = function () {
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
            };

            //重置 click
            scope.resetClick = function () {
                scope.search = {};
                scope.dropDownDefault.customerList = scope.dropDownList.customerList[0];
            };

            //刷新 click
            scope.refreshClick = function () {
                scope.searchClick();
            }
        }])
    .controller('inboundAllVerificationCtrl.dialog', ['$scope', 'inventoryVerificationService', '$timeout', 'ngDialog', function (scope, inventoryVerificationService, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.search = {
            startTime: '',
            endTime: '',
            customerCode: scope.constant.inventoryVerificationCustomerCode.WMS_ALL,
            userId: scope.userInfo.id
        };
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k];
            }
            return str;
        };
        scope.getData = function (e) {
            if (scope.search.startTime && scope.search.endTime) {
                scope.excelUrl = contextPath + 'inventoryManager/getInboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('irqExport').click();
                    scope.search.startTime = '';
                    scope.search.endTime = '';
                    ngDialog.closeAll();
                }, 20);
                // inventoryVerificationService.getInboundManageListExport(scope.formatData(scope.search)).then(function (res) {
                //     if (res.success) {
                //         $timeout(function () {
                //             document.getElementById('irqExport').click();
                //             scope.search.recieveStartTime = '';
                //             scope.search.recieveEndTime = '';
                //             ngDialog.closeAll();
                //         }, 20)
                //     } else {
                //         scope.Tools.alert(res.message);
                //     }
                // });
            } else {
                e.preventDefault();
                scope.Tools.alert('有时间未填！')
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }])
    //库存盘点入库记录查询 controller--查君马客户数据权限
    .controller('inboundJMVerificationCtrl', ['$scope', 'dropDownService', 'inventoryVerificationService', 'ngDialog', '$timeout',
        function (scope, dropDownService, inventoryVerificationService, ngDialog, $timeout) {
            scope.excelUrl = '';
            scope.formatData = function (obj) {
                var str = '?';
                for (var k in obj) {
                    str += '&' + k + '=' + obj[k];
                }
                return str;
            };
            //导出
            scope.exports = function () {
                if (!scope.search.customerCode) {
                    scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.JM;
                }
                scope.search.userId = scope.userInfo.id;
                scope.excelUrl = contextPath + 'inventoryManager/getInboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('islExport').click();
                }, 20)
            };
            //更多条件默认值
            scope.moreText = scope.constant.moreText.spread;
            //更多条件
            scope.moreClick = function () {
                scope.otherForm = !scope.otherForm;
                if (scope.otherForm) {
                    scope.moreText = scope.constant.moreText.packUp;
                } else {
                    scope.moreText = scope.constant.moreText.spread;
                }
            };
            scope.search = {};
            //列表查询接口参数构造
            scope.listParam = function () {

                if (!scope.search) {
                    scope.search = {};
                }
                scope.search.pageSize = scope.pageSize;    //设置最大页
                if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                    scope.search.pageNo = scope.pageNo;
                } else {  //第一次查询设置为　第一页
                    scope.search.pageNo = scope.constant.pageNo;
                }

                if (scope.otherForm) {    //展开了更多
                    // scope.search.odd_vehicle_spec_name = scope.dropDownDefault.vehicleSpec.name;
                    return scope.search;
                } else {  //没有展开
                    return {};
                }
            };

            //列表
            scope.fristSelect = true;   //判断是否是第一次查询
            scope.list = function (params) {
                //设置分页参数
                if (!params) {
                    params = {};
                }
                params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
                params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

                params.customerCode = scope.constant.inventoryVerificationCustomerCode.JM;

                inventoryVerificationService.getInboundManageList(angular.toJson(params))
                    .then(function (result) {
                        if (result.success) {
                            scope.inboundRecordsList = result.data;
                            if (scope.fristSelect) {
                                scope.Tools.page(document.getElementById('pages'), {
                                    pages: result.pageVo.totalPage,
                                    curr: result.pageVo.pageNo,
                                    count: result.pageVo.totalRecord
                                }, function (obj, first) {
                                    scope.fristSelect = first;  //设置状态
                                    if (!first) { //判断不是第一次加载
                                        var param = scope.listParam();
                                        param.pageNo = obj.curr;    //当前页
                                        param.pageSize = obj.limit; //最大页
                                        scope.pageSize = obj.limit; //设置最大页
                                        scope.pageNo = obj.curr; //设置当前页
                                        scope.list(param);
                                    }
                                })
                            } else {
                                //初始化多选框
                                scope.Tools.initCheckBox();
                            }
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            };

            //列表
            scope.list();

            //回车按下查询
            scope.searchListKeyDown = function (e) {
                var keycode = window.event ? e.keyCode : e.which;
                if (keycode === 13) {  //回车键
                    scope.fristSelect = true;   //设置为第一次查询
                    scope.list(scope.listParam());
                    e.preventDefault();
                }
            };

            //查询 click
            scope.searchClick = function () {
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
            };

            //重置 click
            scope.resetClick = function () {
                scope.search = {};
                // scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
            };

            //刷新 click
            scope.refreshClick = function () {
                scope.searchClick();
            }
        }])
    .controller('inboundJMVerificationCtrl.dialog', ['$scope', 'inventoryRecordsQueryservice', '$timeout', 'ngDialog', function (scope, inventoryRecordsQueryservice, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.search = {
            startTime: '',
            endTime: '',
            customerCode: scope.constant.inventoryVerificationCustomerCode.JM,
            userId: scope.userInfo.id
        };
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k];
            }
            return str;
        };
        scope.getData = function (e) {
            if (scope.search.startTime && scope.search.endTime) {
                scope.excelUrl = contextPath + 'inventoryManager/getInboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('irqExport').click();
                    scope.search.startTime = '';
                    scope.search.endTime = '';
                    ngDialog.closeAll();
                }, 20);
                // inventoryVerificationService.getInboundManageListExport(scope.formatData(scope.search)).then(function (res) {
                //     if (res.success) {
                //         $timeout(function () {
                //             document.getElementById('irqExport').click();
                //             scope.search.recieveStartTime = '';
                //             scope.search.recieveEndTime = '';
                //             ngDialog.closeAll();
                //         }, 20)
                //     } else {
                //         scope.Tools.alert(res.message);
                //     }
                // });
            } else {
                e.preventDefault();
                scope.Tools.alert('有时间未填！')
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }])
    //库存盘点入库记录查询 controller--查君马客户数据权限
    .controller('inboundJLVerificationCtrl', ['$scope', 'dropDownService', 'inventoryVerificationService', 'ngDialog', '$timeout',
        function (scope, dropDownService, inventoryVerificationService, ngDialog, $timeout) {
            scope.excelUrl = '';
            scope.formatData = function (obj) {
                var str = '?';
                for (var k in obj) {
                    str += '&' + k + '=' + obj[k];
                }
                return str;
            };
            //导出
            scope.exports = function () {
                if (!scope.search.customerCode) {
                    scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.JL;
                }
                scope.search.userId = scope.userInfo.id;
                scope.excelUrl = contextPath + 'inventoryManager/getInboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('islExport').click();
                }, 20)
            };
            //更多条件默认值
            scope.moreText = scope.constant.moreText.spread;
            //更多条件
            scope.moreClick = function () {
                scope.otherForm = !scope.otherForm;
                if (scope.otherForm) {
                    scope.moreText = scope.constant.moreText.packUp;
                } else {
                    scope.moreText = scope.constant.moreText.spread;
                }
            };

            scope.search = {};
            //列表查询接口参数构造
            scope.listParam = function () {

                if (!scope.search) {
                    scope.search = {};
                }
                scope.search.pageSize = scope.pageSize;    //设置最大页
                if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                    scope.search.pageNo = scope.pageNo;
                } else {  //第一次查询设置为　第一页
                    scope.search.pageNo = scope.constant.pageNo;
                }

                if (scope.otherForm) {    //展开了更多
                    // scope.search.odd_vehicle_spec_name = scope.dropDownDefault.vehicleSpec.name;
                    return scope.search;
                } else {  //没有展开
                    return {};
                }
            };

            //列表
            scope.fristSelect = true;   //判断是否是第一次查询
            scope.list = function (params) {
                //设置分页参数
                if (!params) {
                    params = {};
                }
                params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
                params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

                params.customerCode = scope.constant.inventoryVerificationCustomerCode.JL;

                inventoryVerificationService.getInboundManageList(angular.toJson(params))
                    .then(function (result) {
                        if (result.success) {
                            scope.inboundRecordsList = result.data;
                            if (scope.fristSelect) {
                                scope.Tools.page(document.getElementById('pages'), {
                                    pages: result.pageVo.totalPage,
                                    curr: result.pageVo.pageNo,
                                    count: result.pageVo.totalRecord
                                }, function (obj, first) {
                                    scope.fristSelect = first;  //设置状态
                                    if (!first) { //判断不是第一次加载
                                        var param = scope.listParam();
                                        param.pageNo = obj.curr;    //当前页
                                        param.pageSize = obj.limit; //最大页
                                        scope.pageSize = obj.limit; //设置最大页
                                        scope.pageNo = obj.curr; //设置当前页
                                        scope.list(param);
                                    }
                                })
                            } else {
                                //初始化多选框
                                scope.Tools.initCheckBox();
                            }
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            };

            //列表
            scope.list();

            //回车按下查询
            scope.searchListKeyDown = function (e) {
                var keycode = window.event ? e.keyCode : e.which;
                if (keycode === 13) {  //回车键
                    scope.fristSelect = true;   //设置为第一次查询
                    scope.list(scope.listParam());
                    e.preventDefault();
                }
            };

            //查询 click
            scope.searchClick = function () {
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
            };

            //重置 click
            scope.resetClick = function () {
                scope.search = {};
                // scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
            };

            //刷新 click
            scope.refreshClick = function () {
                scope.searchClick();
            }
        }])
    .controller('inboundJLVerificationCtrl.dialog', ['$scope', 'inventoryRecordsQueryservice', '$timeout', 'ngDialog', function (scope, inventoryRecordsQueryservice, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.search = {
            startTime: '',
            endTime: '',
            customerCode: scope.constant.inventoryVerificationCustomerCode.JL,
            userId: scope.userInfo.id
        };
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k];
            }
            return str;
        };
        scope.getData = function (e) {
            if (scope.search.startTime && scope.search.endTime) {
                scope.excelUrl = contextPath + 'inventoryManager/getInboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('irqExport').click();
                    scope.search.startTime = '';
                    scope.search.endTime = '';
                    ngDialog.closeAll();
                }, 20);
            } else {
                e.preventDefault();
                scope.Tools.alert('有时间未填！')
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }])
    //库存盘点入库记录查询 controller--查所有客户数据权限
    .controller('inventoryAllVerificationCtrl', ['$scope', 'dropDownService', 'inventoryVerificationService', 'ngDialog', '$timeout',
        function (scope, dropDownService, inventoryVerificationService, ngDialog, $timeout) {
            scope.excelUrl = '';
            scope.formatData = function (obj) {
                var str = '?';
                for (var k in obj) {
                    str += '&' + k + '=' + obj[k];
                }
                return str;
            };
            //导出
            scope.export = function () {
                if (!scope.search.customerCode) {
                    scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.WMS_ALL;
                }
                scope.search.userId = scope.userInfo.id;
                scope.excelUrl = contextPath + 'inventoryManager/getInventoryManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('islExport').click();
                }, 20)
            };
            scope.dropDownList = {
                customerList: [
                    {name: "江铃客户", code: "JL"},
                    {name: "君马客户", code: "JM"}
                ]
            };
            scope.dropDownList.customerList.unshift(scope.constant.defaultDropDownNullData);

            scope.dropDownListDefault = {
                customerList: scope.dropDownList.customerList[0]
            };

            //更多条件默认值
            scope.moreText = scope.constant.moreText.spread;
            //更多条件
            scope.moreClick = function () {
                scope.otherForm = !scope.otherForm;
                if (scope.otherForm) {
                    scope.moreText = scope.constant.moreText.packUp;
                } else {
                    scope.moreText = scope.constant.moreText.spread;
                }
            };
            scope.search = {};
            //列表查询接口参数构造
            scope.listParam = function () {
                if (!scope.search) {
                    scope.search = {};
                }
                scope.search.pageSize = scope.pageSize;    //设置最大页
                if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                    scope.search.pageNo = scope.pageNo;
                } else {  //第一次查询设置为　第一页
                    scope.search.pageNo = scope.constant.pageNo;
                }
                if (scope.otherForm) {    //展开了更多
                    scope.search.customerCode = scope.dropDownListDefault.customerList.code;
                    return scope.search;
                } else {  //没有展开
                    return scope.search;
                }
            };

            //列表
            scope.fristSelect = true;   //判断是否是第一次查询
            scope.list = function (params) {
                //设置分页参数
                if (!params) {
                    params = {};
                }
                params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
                params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;
                if (!params.customerCode) {
                    params.customerCode = scope.constant.inventoryVerificationCustomerCode.WMS_ALL;
                }
                inventoryVerificationService.getInventoryManageList(angular.toJson(params))
                    .then(function (result) {
                        if (result.success) {
                            scope.inventoryRecordsList = result.data;
                            if (scope.fristSelect) {
                                scope.Tools.page(document.getElementById('pages'), {
                                    pages: result.pageVo.totalPage,
                                    curr: result.pageVo.pageNo,
                                    count: result.pageVo.totalRecord
                                }, function (obj, first) {
                                    scope.fristSelect = first;  //设置状态
                                    if (!first) { //判断不是第一次加载
                                        var param = scope.listParam();
                                        param.pageNo = obj.curr;    //当前页
                                        param.pageSize = obj.limit; //最大页
                                        scope.pageSize = obj.limit; //设置最大页
                                        scope.pageNo = obj.curr; //设置当前页
                                        scope.list(param);
                                    }
                                })
                            } else {
                                //初始化多选框
                                scope.Tools.initCheckBox();
                            }
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            };

            //列表
            scope.list();

            //回车按下查询
            scope.searchListKeyDown = function (e) {
                var keycode = window.event ? e.keyCode : e.which;
                if (keycode === 13) {  //回车键
                    scope.fristSelect = true;   //设置为第一次查询
                    scope.list(scope.listParam());
                    e.preventDefault();
                }
            };

            //查询 click
            scope.searchClick = function () {
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
            };

            //重置 click
            scope.resetClick = function () {
                scope.search = {};
                scope.dropDownListDefault.customerList = scope.dropDownList.customerList[0];
            };

            //刷新 click
            scope.refreshClick = function () {
                scope.searchClick();
            }
        }])
    //库存盘点入库记录查询 controller--查所有客户数据权限
    .controller('inventoryJMVerificationCtrl', ['$scope', 'dropDownService', 'inventoryVerificationService', 'ngDialog', '$timeout',
        function (scope, dropDownService, inventoryVerificationService, ngDialog, $timeout) {
            scope.excelUrl = '';
            scope.formatData = function (obj) {
                var str = '?';
                for (var k in obj) {
                    str += '&' + k + '=' + obj[k];
                }
                return str;
            };
            //导出
            scope.export = function () {
                if (!scope.search.customerCode) {
                    scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.JM;
                }
                scope.search.userId = scope.userInfo.id;
                scope.excelUrl = contextPath + 'inventoryManager/getInventoryManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('islExport').click();
                }, 20)
            };
            //更多条件默认值
            scope.moreText = scope.constant.moreText.spread;
            //更多条件
            scope.moreClick = function () {
                scope.otherForm = !scope.otherForm;
                if (scope.otherForm) {
                    scope.moreText = scope.constant.moreText.packUp;
                } else {
                    scope.moreText = scope.constant.moreText.spread;
                }
            };

            scope.dropDownList = {
                statusList: [
                    {name: '正常在库', value: '10'},
                    {name: '发运锁定', value: '20'},
                    {name: '借用锁定', value: '30'},
                    {name: '维修锁定', value: '40'},
                    {name: '库存冻结', value: '60'}
                ]
            };    //下拉
            scope.dropDownList.statusList.unshift(scope.constant.defaultDropDownNullData);
            //下拉默认值
            scope.dropDownDefault = {
                statusList: scope.dropDownList.statusList[0]
            };

            //获取车型
            // dropDownService.getVehicleSpec()
            //     .then(function (result) {
            //         if (result.success) {
            //             scope.dropDownList.vehicleSpec = result.data;
            //             scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
            //             scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
            //         } else {
            //             scope.Tools.alert(result.message);
            //         }
            //     });

            scope.search = {};
            //列表查询接口参数构造
            scope.listParam = function () {

                if (!scope.search) {
                    scope.search = {};
                }
                scope.search.pageSize = scope.pageSize;    //设置最大页
                if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                    scope.search.pageNo = scope.pageNo;
                } else {  //第一次查询设置为　第一页
                    scope.search.pageNo = scope.constant.pageNo;
                }

                if (scope.otherForm) {    //展开了更多
                    scope.search.status = scope.dropDownDefault.statusList.value;
                    return scope.search;
                } else {  //没有展开
                    return scope.search;
                }
            };

            //列表
            scope.fristSelect = true;   //判断是否是第一次查询
            scope.list = function (params) {
                //设置分页参数
                if (!params) {
                    params = {};
                }
                params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
                params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

                params.customerCode = scope.constant.inventoryVerificationCustomerCode.JM;

                inventoryVerificationService.getInventoryManageList(angular.toJson(params))
                    .then(function (result) {
                        if (result.success) {
                            scope.inventoryRecordsList = result.data;
                            if (scope.fristSelect) {
                                scope.Tools.page(document.getElementById('pages'), {
                                    pages: result.pageVo.totalPage,
                                    curr: result.pageVo.pageNo,
                                    count: result.pageVo.totalRecord
                                }, function (obj, first) {
                                    scope.fristSelect = first;  //设置状态
                                    if (!first) { //判断不是第一次加载
                                        var param = scope.listParam();
                                        param.pageNo = obj.curr;    //当前页
                                        param.pageSize = obj.limit; //最大页
                                        scope.pageSize = obj.limit; //设置最大页
                                        scope.pageNo = obj.curr; //设置当前页
                                        scope.list(param);
                                    }
                                })
                            } else {
                                //初始化多选框
                                scope.Tools.initCheckBox();
                            }
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            };

            //列表
            scope.list();

            //回车按下查询
            scope.searchListKeyDown = function (e) {
                var keycode = window.event ? e.keyCode : e.which;
                if (keycode === 13) {  //回车键
                    scope.fristSelect = true;   //设置为第一次查询
                    scope.list(scope.listParam());
                    e.preventDefault();
                }
            };

            //查询 click
            scope.searchClick = function () {
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
            };

            //重置 click
            scope.resetClick = function () {
                scope.search = {};
                scope.dropDownDefault.statusList = scope.dropDownList.statusList[0];
            };

            //刷新 click
            scope.refreshClick = function () {
                scope.searchClick();
            }
        }])
    //库存盘点入库记录查询 controller--查所有客户数据权限
    .controller('inventoryJLVerificationCtrl', ['$scope', 'dropDownService', 'inventoryVerificationService', 'ngDialog', '$timeout',
        function (scope, dropDownService, inventoryVerificationService, ngDialog, $timeout) {
            scope.excelUrl = '';
            scope.formatData = function (obj) {
                var str = '?';
                for (var k in obj) {
                    str += '&' + k + '=' + obj[k];
                }
                return str;
            };
            //导出
            scope.export = function () {
                if (!scope.search.customerCode) {
                    scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.JL;
                }
                scope.search.userId = scope.userInfo.id;
                scope.excelUrl = contextPath + 'inventoryManager/getInventoryManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('islExport').click();
                }, 20)
            };
            //更多条件默认值
            scope.moreText = scope.constant.moreText.spread;
            //更多条件
            scope.moreClick = function () {
                scope.otherForm = !scope.otherForm;
                if (scope.otherForm) {
                    scope.moreText = scope.constant.moreText.packUp;
                } else {
                    scope.moreText = scope.constant.moreText.spread;
                }
            };

            //获取车型
            // dropDownService.getVehicleSpec()
            //     .then(function (result) {
            //         if (result.success) {
            //             scope.dropDownList.vehicleSpec = result.data;
            //             scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
            //             scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
            //         } else {
            //             scope.Tools.alert(result.message);
            //         }
            //     });

            scope.search = {};
            //列表查询接口参数构造
            scope.listParam = function () {

                if (!scope.search) {
                    scope.search = {};
                }
                scope.search.pageSize = scope.pageSize;    //设置最大页
                if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                    scope.search.pageNo = scope.pageNo;
                } else {  //第一次查询设置为　第一页
                    scope.search.pageNo = scope.constant.pageNo;
                }

                if (scope.otherForm) {    //展开了更多
                    // scope.search.odd_vehicle_spec_name = scope.dropDownDefault.vehicleSpec.name;
                    return scope.search;
                } else {  //没有展开
                    return scope.search;
                }
            };

            //列表
            scope.fristSelect = true;   //判断是否是第一次查询
            scope.list = function (params) {
                //设置分页参数
                if (!params) {
                    params = {};
                }
                params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
                params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

                params.customerCode = scope.constant.inventoryVerificationCustomerCode.JL;

                inventoryVerificationService.getInventoryManageList(angular.toJson(params))
                    .then(function (result) {
                        if (result.success) {
                            scope.inventoryRecordsList = result.data;
                            if (scope.fristSelect) {
                                scope.Tools.page(document.getElementById('pages'), {
                                    pages: result.pageVo.totalPage,
                                    curr: result.pageVo.pageNo,
                                    count: result.pageVo.totalRecord
                                }, function (obj, first) {
                                    scope.fristSelect = first;  //设置状态
                                    if (!first) { //判断不是第一次加载
                                        var param = scope.listParam();
                                        param.pageNo = obj.curr;    //当前页
                                        param.pageSize = obj.limit; //最大页
                                        scope.pageSize = obj.limit; //设置最大页
                                        scope.pageNo = obj.curr; //设置当前页
                                        scope.list(param);
                                    }
                                })
                            } else {
                                //初始化多选框
                                scope.Tools.initCheckBox();
                            }
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            };

            //列表
            scope.list();

            //回车按下查询
            scope.searchListKeyDown = function (e) {
                var keycode = window.event ? e.keyCode : e.which;
                if (keycode === 13) {  //回车键
                    scope.fristSelect = true;   //设置为第一次查询
                    scope.list(scope.listParam());
                    e.preventDefault();
                }
            };

            //查询 click
            scope.searchClick = function () {
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
            };

            //重置 click
            scope.resetClick = function () {
                scope.search = {};
                // scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
            };

            //刷新 click
            scope.refreshClick = function () {
                scope.searchClick();
            }
        }]);