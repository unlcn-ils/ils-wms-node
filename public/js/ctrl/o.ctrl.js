/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';

wmsCtrl.controller('outboundOrderCtrl', ['$scope', 'warehouseService', 'warehouseMaintenanceService', 'dropDownService', function (scope, warehouseService, warehouseMaintenanceService, dropDownService) {   //备料单 ctrl

    //更多条件默认值
    scope.moreText = scope.constant.moreText.spread;
    //更多条件
    scope.moreClick = function () {
        scope.otherForm = !scope.otherForm;
        if (scope.otherForm) {
            scope.moreText = scope.constant.moreText.packUp;
        } else {
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    /*状态数据*/
    scope.dropDownList = {
        warehouse: scope.Tools.dropDown.warehouseDropDown.concat(),    //出库状态
        outboundStatus: scope.Tools.dropDown.outboundStatusDropDown.concat()   //备料状态
    }
    scope.dropDownList.warehouse.unshift(scope.constant.defaultDropDownNullData);  //向前插入空值
    scope.dropDownList.outboundStatus.unshift(scope.constant.defaultDropDownNullData);  //向前插入空值

    //设置默认值
    scope.dropDownDefault = {
        warehouse: scope.dropDownList.warehouse[0], //出库状态默认值
        outboundStatus: scope.dropDownList.outboundStatus[0]   //备料状态默认值
    }

    //获取装车道
    warehouseMaintenanceService.getEstimateLaneByWhCode(scope.userInfo.whCode)
        .then(function (result) {
            if (result.success) {
                scope.dropDownList.estimateLane = result.data;
                if (scope.dropDownList.estimateLane) {
                    scope.dropDownList.estimateLane.unshift(scope.constant.defaultDropDownNullData);  //向前插入空值
                    scope.dropDownDefault.estimateLane = scope.dropDownList.estimateLane[0];
                }
            } else {
                scope.Tools.alert(result.message);
            }
        })

    //列表查询接口参数构造
    scope.listParam = function () {

        if (!scope.search) {
            scope.search = {};
        }
        scope.search.pageSize = scope.pageSize;    //设置最大页
        if (!scope.fristSelect) { //不是第一次查询 则设置当前页
            scope.search.pageNo = scope.pageNo;
        } else {  //第一次查询设置为　第一页
            scope.search.pageNo = scope.constant.pageNo;
        }

        if (scope.otherForm) {    //展开了更多
            scope.search.ppStatus = scope.dropDownDefault.outboundStatus.value; //备料状态
            scope.search.ppOutstockStatus = scope.dropDownDefault.warehouse.value;  //出库状态
            scope.search.ppEstimateLane = scope.dropDownDefault.estimateLane ? scope.dropDownDefault.estimateLane.elName : '';  //装车道
            return scope.search;
        } else {  //没有展开
            return scope.search;
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function (params) {
        //设置分页参数
        if (!params) {
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

        warehouseService.outboundPlanList(params)
            .then(function (result) {
                if (result.success) {
                    scope.outboundOrderList = result.data.wmsPreparationPlanBO;
                    scope.outboundOrderDetailList = null;   //情况详情数据
                    if (scope.fristSelect) {
                        scope.Tools.page(document.getElementById('pages'), {
                            pages: result.data.wmsPreparationPlanQuery.totalPage,
                            curr: result.data.wmsPreparationPlanQuery.pageNo,
                            count: result.data.wmsPreparationPlanQuery.totalRecord
                        }, function (obj, first) {
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if (!first) { //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    } else {
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }
                } else {
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //回车按下查询
    scope.searchListKeyDown = function (e) {
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }

    //点击每行列表的数据 click
    scope.selectItemClick = function (item, e) {
        scope.Tools.initTableClick(e);  //选中行处理
        warehouseService.getVehicleDetail(item.ppId)
            .then(function (result) {
                if (result.success) {
                    scope.outboundOrderDetailList = result.data
                } else {
                    scope.Tools.alert(result.message);
                }
            })
    }

    //查询 click
    scope.searchClick = function () {
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function () {
        scope.search = {};
        scope.dropDownDefault.outboundStatus = scope.dropDownList.outboundStatus[0];
        scope.dropDownDefault.warehouse = scope.dropDownList.warehouse[0];
        scope.dropDownDefault.estimateLane = scope.dropDownList.estimateLane[0];    //装车道
    }

    //刷新 click
    scope.refreshClick = function () {
        scope.searchClick();
    }

    //创建备料计划 click
    scope.createOutboundPlanClick = function () { //生成备料计划
        var indexArr = scope.Tools.getTableSelectItemIndex('.outboundTable');  //获取table所选的下标

        if (indexArr.length == 0) {
            scope.Tools.alert('请选择备料计划数据');
            return;
        }
        var outboundOrderList = scope.Tools.getDataTOIndex(indexArr, scope.outboundOrderList);

        for (var i = 0; i < outboundOrderList.length; i++) {
            if (outboundOrderList[i].ppStatus != scope.constant.outboundStatus.unStart) {
                scope.Tools.alert('只能选择未开始的数据');
                return;
            }
        }

        //获取装车道
        warehouseMaintenanceService.getEstimateLaneByWhCode(scope.userInfo.whCode)
            .then(function (estimateLaneResult) {
                if (estimateLaneResult.success) {
                    scope.Tools.dialog({
                        controller: 'outboundOrderCtrl.dialog',
                        template: 'templates/warehouseManagement/outboundOrder/generate-outbound-form.html',
                        closeByDocument: false,
                        closeByEscape: false,
                        data: {
                            estimateLane: estimateLaneResult.data,
                            dropDownDefault: {
                                estimateLane: estimateLaneResult.data[0]
                            },
                            outboundOrderList: outboundOrderList
                        },
                        width: 400,
                        title: '生成备料计划',
                        scope: scope
                    })
                } else {
                    scope.Tools.alert(estimateLaneResult.message);
                }
            })
    }

    //发运计划变更 click
    scope.shipmentChangClick = function () {

        var indexArr = scope.Tools.getTableSelectItemIndex('.outboundTable');  //获取table所选的下标

        if (indexArr.length == 0) {
            scope.Tools.alert('请选择备料计划数据');
            return;
        }
        var outboundOrderList = scope.Tools.getDataTOIndex(indexArr, scope.outboundOrderList);

        warehouseService.updateShipToAddPlan(outboundOrderList[0].ppId)
            .then(function (result) {
                if (result.success) {
                    scope.Tools.alert('发运计划变更成功');
                    scope.list(scope.listParam());  //刷新列表数据
                    scope.outboundOrderDetailList = null;
                } else {
                    scope.Tools.alert(result.message);
                }
            })
    }

    //增车 click
    scope.addCarClick = function () {
        var indexArr = scope.Tools.getTableSelectItemIndex('.outboundTable');  //获取table所选的下标

        if (indexArr.length == 0) {
            scope.Tools.alert('请选择备料计划数据');
            return;
        }
        var outboundOrderList = scope.Tools.getDataTOIndex(indexArr, scope.outboundOrderList);
        warehouseService.addCar(outboundOrderList[0].ppId)
            .then(function (result) {
                if (result.success) {
                    scope.Tools.alert('增车成功');
                    scope.selectItemClick(outboundOrderList[0])
                } else {
                    scope.Tools.alert(result.message);
                }
            })
    }

    //删除车 click
    scope.deleteCarClick = function (item) {
        warehouseService.deleteCar(item.vdId)
            .then(function (result) {
                if (result.success) {
                    scope.Tools.alert('删除车辆成功');
                    var indexArr = scope.Tools.getTableSelectItemIndex('.outboundTable');  //获取table所选的下标

                    var outboundOrderList = scope.Tools.getDataTOIndex(indexArr, scope.outboundOrderList);
                    scope.selectItemClick(outboundOrderList[0]);
                } else {
                    scope.Tools.alert(result.message);
                }
            })
    }

    //导出 click
    scope.exportExcelClick = function () {
        var str = '?';
        var param = scope.search;
        for (var k in param) {
            if (param[k]) {
                str += '&' + k + '=' + param[k];
            }
        }
        var a = document.createElement('a');
        a.setAttribute('href', contextPath + 'wmsPreparationPlan/getImportExcelData' + str);
        a.click();
    }

}])
    .controller('outboundOrderCtrl.dialog', ['$scope', 'warehouseMaintenanceService', 'warehouseService', function (scope, warehouseMaintenanceService, warehouseService) {   //备料计划管理 dialog
        scope.flag = true;  //禁止多次点击
        //保存备料计划
        scope.savePlanClick = function () {
            if (scope.flag) {
                scope.flag = false;
                warehouseService.createPlan(scope.ngDialogData.dropDownDefault.estimateLane.elId, scope.ngDialogData.outboundOrderList)
                    .then(function (result) {
                        if (result.success) {
                            scope.Tools.alert('生成备料计划成功');
                            scope.list(scope.listParam());  //刷新列表数据
                            scope.closeThisDialog();
                        } else {
                            scope.Tools.alert(result.message);
                        }
                        scope.flag = true;
                    })
            }

        }
    }])
    .controller('outboundOrderKeyCtrl', ['$scope', 'warehouseService', function (scope, warehouseService) {    //备料钥匙领取 ctrl
        //司机vin key down
        scope.searchDriverVinKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                warehouseService.contrastInfo(scope.driverVin)
                    .then(function (result) {
                        if (result.success) {
                            if (result.data) {
                                scope.driver = result.data;
                                angular.element('.keyVin').focus();
                            } else {
                                scope.Tools.alert('没有查询到vin对应的信息');
                            }
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        };

        //钥匙vin key down
        scope.searchKeyVinKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                //判断司机vin
                if (!scope.driverVin) {
                    scope.Tools.alert('请先扫描司机vin码');
                    return;
                }

                //判断两次的vin是否一样
                if (scope.driverVin != scope.keyVin) {
                    scope.Tools.alert('两次vin不一样请重新输入');
                    return;
                }

                warehouseService.contrastInfo(scope.keyVin)
                    .then(function (result) {
                        if (result.success) {
                            if (scope.key = result.data) {
                                scope.key = result.data;
                                warehouseService.updateStatus(scope.keyVin)
                                    .then(function (result) {
                                        if (result.success) {
                                            scope.Tools.alert('校验通过');
                                            //清空输入框数据
                                            scope.driverVin = null;
                                            scope.keyVin = null;
                                        } else {
                                            scope.Tools.alert(result.message);
                                        }
                                    })
                            } else {
                                scope.Tools.alert('没有查询到vin对应的信息');
                            }
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }
    }])
    .controller('outboundOrderKeyCtrl.dialog', ['$scope', function (scope) {    //备料钥匙领取 dialog
    }])
    .controller('outboundPrintReceiptCtrl', ['$scope', 'warehouseService', function (scope, warehouseService) {    //出库交接单打印
        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function () {
            scope.otherForm = !scope.otherForm;
            if (scope.otherForm) {
                scope.moreText = scope.constant.moreText.packUp;
            } else {
                scope.moreText = scope.constant.moreText.spread;
            }
        }

        /*状态数据*/
        scope.dropDownList = {
            printStatus: scope.Tools.dropDown.outboundPrintStatusDropDown.concat()    //打印状态
        }

        scope.dropDownList.printStatus.unshift(scope.constant.defaultDropDownNullData); //插入打印状态数据

        //设置默认值
        scope.dropDownDefault = {
            printStatus: scope.dropDownList.printStatus[0] //打印状态
        }

        //列表查询接口参数构造
        scope.listParam = function () {

            if (!scope.search) {
                scope.search = {};
            }
            scope.search.pageSize = scope.pageSize;    //设置最大页
            if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            } else {  //第一次查询设置为　第一页
                scope.search.pageNo = scope.constant.pageNo;
            }

            if (scope.otherForm) {    //展开了更多
                scope.search.printStatus = scope.dropDownDefault.printStatus.value; //打印状态
                return scope.search;
            } else {  //没有展开
                return {
                    otVin: scope.search.otVin, //车架号
                    pageSize: scope.search.pageSize,   //最大号
                    pageNo: scope.search.pageNo    //当前页
                }
            }
        }

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

            warehouseService.getPrintOutboundTaskList(params)
                .then(function (result) {
                    if (result.success) {
                        scope.outboundPrintReceiptList = result.data.dataList;
                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.data.page.totalPage,
                                curr: result.data.page.pageNo,
                                count: result.data.page.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if (!first) { //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }

                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //列表
        scope.list();

        //回车按下查询
        scope.searchListKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
                e.preventDefault();
            }
        }

        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        }

        //重置 click
        scope.resetClick = function () {
            scope.search = {};
            scope.dropDownDefault.printStatus = scope.dropDownList.printStatus; //打印状态
        }

        //刷新 click
        scope.refreshClick = function () {
            scope.searchClick();
        }

        //打印出库交接单
        scope.printOutboundReceiptClick = function () {
            var indexArr = scope.Tools.getTableSelectItemIndex('.outboundPrintReceiptTable');  //获取table所选的下标

            if (indexArr.length == 0) {
                scope.Tools.alert('请选择一条数据');
                return;
            }

            if (indexArr.length > 1) {
                scope.Tools.alert('只能选择一条数据');
                return;
            }

            warehouseService.getPrintInfo(scope.outboundPrintReceiptList[indexArr[0]].otId)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.dialog({
                            controller: 'outboundPrintReceiptCtrl.dialog',
                            template: 'templates/warehouseManagement/outboundPrintReceipt/print-outbound-receipt.html',
                            closeByDocument: false,
                            closeByEscape: false,
                            data: {
                                receipt: result.data,
                                date: new Date(),
                                otId: scope.outboundPrintReceiptList[indexArr[0]].otId
                            },
                            width: 1100,
                            title: '打印出库交接单',
                            scope: scope
                        })
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }])
    .controller('outboundPrintReceiptCtrl.dialog', ['$scope', 'warehouseService', function (scope, warehouseService) {    //出库交接单打印  dialog
        //打印交接单 click
        scope.printReceiptClick = function () {
            if (!scope.Tools.checkPrintInstall()) {   //打印插件检测
                scope.Tools.downLoadPrintPlugIn();    //提示下载打印插件
                return;
            }
            scope.Tools.printHTMLWidth($(".dialog-body")[0].outerHTML, 880, 1);
            warehouseService.updatePrintInfo(scope.ngDialogData.otId, scope.userInfo.id)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.alert('打印信息更新成功');
                        scope.list(scope.listParam());
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }])
    .controller('onWayCtrl', ['$scope', '$rootScope', '$timeout', '$state', 'onWayService', function ($scope, $rootScope, $timeout, $state, onWayService) {
        $scope.tableList = [];
        $scope.search = {
            status: '10'
        };
        $scope.selectReset = function () {
            $scope.pageNo = 1;
            $scope.pageSize = 20;
            $scope.getList()
        };

        $scope.fristSelect = true;

        //    获取列表
        $scope.getList = function () {
            var params = {
                pageNo: $scope.pageNo || 1,
                pageSize: $scope.pageSize || 20
            };
            params = angular.extend(params, $scope.search);
            onWayService.onWayLine(params)
                .then(function (result) {
                    if (result.success) {

                        $scope.tableList = result.data;
                        $rootScope.totalPageALL = result.pageVo.totalPage;
                        if (true) {
                            $scope.Tools.page(document.getElementById('pages'), {
                                pages: result.pageVo.totalPage,
                                curr: result.pageVo.pageNo,
                                count: result.pageVo.totalRecord,
                                limit: result.pageVo.pageSize
                                , limits: [10, 20, 30, 40, 50]
                            }, function (obj, first) {
                                $scope.fristSelect = first;  //设置状态

                                if (!first) { //判断不是第一次加载
                                    var param = {};
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    $scope.pageSize = obj.limit; //设置最大页
                                    $scope.pageNo = obj.curr; //设置当前页
                                    $scope.getList(param);
                                }
                            })
                        }
                    } else {
                        $scope.Tools.alert(result.message);
                    }
                })
        };
        // $scope.getList = function(){
        //     onWayService.onWayLine().then(function(data){
        //         if(data.success){
        //             $scope.tableList = data.data;
        //         }
        //     });
        // };
        /**
         *  每隔5秒跳一个页面
         * @type {number}
         */
        $scope.count = 1;
        $scope.getList();
        $rootScope.onwayIntertime = setInterval(function () {
            $scope.pageNo < $rootScope.totalPageALL ? $scope.pageNo++ : $scope.pageNo = 1;
            $timeout(function () {
                $scope.getList();
            }, 1);

        }, 10000);

        window.clearTimeout($rootScope.bs_timer1);
        window.clearTimeout($rootScope.bs_timer2);
        window.clearTimeout($rootScope.bs_timer3);
        window.clearTimeout($rootScope.bs_timer4);
        window.clearInterval($rootScope.bs_timer5);
        window.clearTimeout($rootScope.onwayIntertime);

        $rootScope.bs_timer1 = setTimeout(function () {
            $state.go('transferDepot')
        }, $rootScope.bs_intervalTime * 60000)
    }])
    .controller('operationOutOfStorageCtrl', ['$rootScope', '$scope', 'wmsJmOurOfStorageOperationService', function ($rootScope, $scope, wmsJmOurOfStorageOperationService) {
        /*下拉框状态数据*/
        $scope.dropDownList = {
            inOutType: $scope.Tools.dropDown.operationOutOfStorageDown.concat(),    //出入库状态
            sendDCSStatus: $scope.Tools.dropDown.send_status.concat(),  //发送dcs状态
            sendSAPStatus: $scope.Tools.dropDown.send_status.concat()    //发送sap状态
        };
        $scope.dropDownList.inOutType.unshift($scope.constant.defaultDropDownNullData);  //向前插入空值
        $scope.dropDownList.sendDCSStatus.unshift($scope.constant.defaultDropDownNullData);  //向前插入空值
        $scope.dropDownList.sendSAPStatus.unshift($scope.constant.defaultDropDownNullData);  //向前插入空值
        //设置默认值
        $scope.dropDownListDefault = {
            inOutValue: $scope.dropDownList.inOutType[0], //出入库状态
            sendDCSValue: $scope.dropDownList.sendDCSStatus[0], //发送dcs状态
            sendSapValue: $scope.dropDownList.sendSAPStatus[0] //发送sap状态
        };

        //更多条件默认值
        $scope.moreText = $scope.constant.moreText.spread;

        //更多条件
        $scope.moreClick = function () {
            $scope.otherForm = !$scope.otherForm;
            if ($scope.otherForm) {
                $scope.moreText = $scope.constant.moreText.packUp;
            } else {
                $scope.moreText = $scope.constant.moreText.spread;
            }
        };
        $scope.search = {};
        //列表查询接口参数构造
        $scope.listParam = function () {
            if (!$scope.search) {
                $scope.search = {};
            }
            $scope.search.pageSize = $scope.pageSize;    //设置页面的显示数目
            if (!$scope.fristSelect) { //不是第一次查询 则设置当前页
                $scope.search.pageNo = $scope.pageNo;
            } else {  //第一次查询设置为　第一页
                $scope.search.pageNo = $scope.constant.pageNo;
            }
            if ($scope.otherForm) {    //展开了更多
                $scope.search.zaction = $scope.dropDownListDefault.inOutValue.value;
                $scope.search.sendDcsStatus = $scope.dropDownListDefault.sendDCSValue.value;
                $scope.search.sendSapStatus = $scope.dropDownListDefault.sendSapValue.value;
                return $scope.search;
            } else {  //没有展开
                return {};
            }
        };
        //列表
        $scope.fristSelect = true;   //判断是否是第一次查询
        $scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : $scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : $scope.constant.page.pageNo;

            wmsJmOurOfStorageOperationService.getSendList(angular.toJson(params))
                .then(function (result) {
                    if (result.success) {
                        $scope.outOfStorageSendList = result.data;
                        if ($scope.fristSelect) {
                            $scope.Tools.page(document.getElementById('pages'), {
                                pages: result.pageVo.totalPage,
                                curr: result.pageVo.pageNo,
                                count: result.pageVo.totalRecord
                            }, function (obj, first) {
                                $scope.fristSelect = first;  //设置状态
                                if (!first) { //判断不是第一次加载
                                    var param = $scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    $scope.pageSize = obj.limit; //设置最大页
                                    $scope.pageNo = obj.curr; //设置当前页
                                    $scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            $scope.Tools.initCheckBox();
                        }

                    } else {
                        $scope.Tools.alert(result.message);
                    }
                })
        };
        //列表
        $scope.list();
        //查询 click
        $scope.searchClick = function () {
            $scope.fristSelect = true;   //设置为第一次查询
            $scope.list($scope.listParam());
        };

        //重置 click
        $scope.resetClick = function () {
            $scope.search = {};
            $scope.dropDownListDefault.inOutValue = $scope.dropDownList.inOutType [0];
            $scope.dropDownListDefault.sendDCSValue = $scope.dropDownList.sendDCSStatus[0];
            $scope.dropDownListDefault.sendSapValue = $scope.dropDownList.sendSAPStatus[0];
        };

        //刷新 click
        $scope.refreshClick = function () {
            $scope.searchClick();
        };

        function getIdArr() {
            var idArr = [];
            $('.listTable').find('td i').each(function (index, obj) {
                if ($(obj).parent().hasClass('layui-form-checked')) {
                    var id = $(obj).parents('tr').attr('id');
                    idArr.push(id);
                }
            });
            return idArr;
        }

        //重发dcs
        $scope.sendDcsAgainClick = function () {
            var idArr = getIdArr();
            if (idArr.length <= 0) {
                $scope.Tools.alert("请选择需要重发DCS的数据");
            }
            wmsJmOurOfStorageOperationService.updateSendDcsAgain({
                dataIds: idArr.toString()
            }).then(function (res) {
                if (res.success) {
                    $scope.list($scope.listParam());
                }
                $scope.Tools.alert(res.message);
            })
        };
        $scope.sendSapAgainClick = function () {
            var idArr = getIdArr();
            if (idArr.length <= 0) {
                $scope.Tools.alert("请选择需要重发SAP的数据");
            }
            wmsJmOurOfStorageOperationService.updateSendSapAgain({
                dataIds: idArr.toString()
            }).then(function (res) {
                if (res.success) {
                    $scope.list($scope.listParam());
                }
                $scope.Tools.alert(res.message);
            })
        };

        $scope.updateDcsSuccessClick = function () {
            var idArr = getIdArr();
            if (idArr.length <= 0) {
                $scope.Tools.alert("请选择需要更改DCS发送状态的数据");
            }
            var confirmStatus = $scope.Tools.dialogConfirm("确认要修改选择的数据状态为成功吗?");
            confirmStatus.then(function (isConfirm) {
                if (isConfirm) {
                    wmsJmOurOfStorageOperationService.updateDcsToSuccess({
                        dataIds: idArr.toString()
                    }).then(function (res) {
                        if (res.success) {
                            $scope.list($scope.listParam());
                        }
                        $scope.Tools.alert(res.message);
                    })
                }
            });
        };

        $scope.updateSapSuccessClick = function () {
            var idArr = getIdArr();
            if (idArr.length <= 0) {
                $scope.Tools.alert("请选择需要更改SAP发送状态的数据");
            }
            var confirmStatus = $scope.Tools.dialogConfirm("确认要修改选择的数据状态为成功吗?");
            confirmStatus.then(function (isConfirm) {
                if (isConfirm) {
                    wmsJmOurOfStorageOperationService.updateSapToSuccess({
                        dataIds: idArr.toString()
                    }).then(function (res) {
                        if (res.success) {
                            $scope.list($scope.listParam());
                        }
                        $scope.Tools.alert(res.message);
                    })
                }
            });
        };
    }])
    .controller('operationHandoverOrderCtrl', ['$rootScope', '$scope', 'wmsJmHandoverOperationService', function ($rootScope, $scope, wmsJmHandoverOperationService) {
        $scope.dropDownList = {
            dcsDropDown: $scope.Tools.dropDown.send_status.concat(),//发送dcs状态下拉框
            sapDropDown: $scope.Tools.dropDown.send_status.concat(),//发送sap状态下拉框
            crmDropDown: $scope.Tools.dropDown.send_status.concat()//发送crm状态下拉框
        };
        //下拉框插入空值
        $scope.dropDownList.dcsDropDown.unshift($scope.constant.defaultDropDownNullData);
        $scope.dropDownList.sapDropDown.unshift($scope.constant.defaultDropDownNullData);
        $scope.dropDownList.crmDropDown.unshift($scope.constant.defaultDropDownNullData);
        //设置默认值
        $scope.dropDownListDefault = {
            dcsStatusValue: $scope.dropDownList.dcsDropDown[0],
            sapStatusValue: $scope.dropDownList.sapDropDown[0],
            crmStatusValue: $scope.dropDownList.crmDropDown[0]
        };
        //更多条件默认值
        $scope.moreText = $scope.constant.moreText.spread;

        //更多条件
        $scope.moreClick = function () {
            $scope.otherForm = !$scope.otherForm;
            if ($scope.otherForm) {
                $scope.moreText = $scope.constant.moreText.packUp;
            } else {
                $scope.moreText = $scope.constant.moreText.spread;
            }
        };

        $scope.search = {};
        //列表查询接口参数构造
        $scope.listParam = function () {
            if (!$scope.search) {
                $scope.search = {};
            }
            $scope.search.pageSize = $scope.pageSize;    //设置页面的显示数目
            if (!$scope.fristSelect) { //不是第一次查询 则设置当前页
                $scope.search.pageNo = $scope.pageNo;
            } else {  //第一次查询设置为　第一页
                $scope.search.pageNo = $scope.constant.pageNo;
            }
            if ($scope.otherForm) {    //展开了更多
                $scope.search.sendDcsStatus = $scope.dropDownListDefault.dcsStatusValue.value;
                $scope.search.sendSapStatus = $scope.dropDownListDefault.sapStatusValue.value;
                $scope.search.sendCrmStatus = $scope.dropDownListDefault.crmStatusValue.value;
                return $scope.search;
            } else {  //没有展开
                return {};
            }
        };

        //列表
        $scope.fristSelect = true;   //判断是否是第一次查询
        $scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : $scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : $scope.constant.page.pageNo;

            wmsJmHandoverOperationService.getSendList(angular.toJson(params))
                .then(function (result) {
                    if (result.success) {
                        $scope.HandoverOrderList = result.data;
                        if ($scope.fristSelect) {
                            $scope.Tools.page(document.getElementById('pages'), {
                                pages: result.pageVo.totalPage,
                                curr: result.pageVo.pageNo,
                                count: result.pageVo.totalRecord
                            }, function (obj, first) {
                                $scope.fristSelect = first;  //设置状态
                                if (!first) { //判断不是第一次加载
                                    var param = $scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    $scope.pageSize = obj.limit; //设置最大页
                                    $scope.pageNo = obj.curr; //设置当前页
                                    $scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            $scope.Tools.initCheckBox();
                        }

                    } else {
                        $scope.Tools.alert(result.message);
                    }
                })
        };
        //列表
        $scope.list();
        //查询 click
        $scope.searchClick = function () {
            $scope.fristSelect = true;   //设置为第一次查询
            $scope.list($scope.listParam());
        };

        //重置 click
        $scope.resetClick = function () {
            $scope.search = {};
            $scope.dropDownListDefault.dcsStatusValue = $scope.dropDownList.dcsDropDown [0];
            $scope.dropDownListDefault.sapStatusValue = $scope.dropDownList.sapDropDown[0];
            $scope.dropDownListDefault.crmStatusValue = $scope.dropDownList.crmDropDown[0];
        };

        //刷新 click
        $scope.refreshClick = function () {
            $scope.searchClick();
        };

        function getIdArr() {
            var idArr = [];
            $('.listTable').find('td i').each(function (index, obj) {
                if ($(obj).parent().hasClass('layui-form-checked')) {
                    var id = $(obj).parents('tr').attr('id');
                    idArr.push(id);
                }
            });
            return idArr;
        }

        $scope.sendDcsAgainClick = function () {
            var idArr = getIdArr;
            if (idArr().length <= 0) {
                $scope.Tools.alert("请选择要重传的数据!");
            }
            wmsJmHandoverOperationService.updateSendDcsAgain({
                dataIds: idArr().toString()
            }).then(function (res) {
                if (res.success) {
                    $scope.list($scope.listParam());
                }
                $scope.Tools.alert(res.message);
            })

        };
        $scope.sendSapAgainClick = function () {
            var idArr = getIdArr;
            if (idArr().length <= 0) {
                $scope.Tools.alert("请选择要重传的数据!");
            }
            wmsJmHandoverOperationService.updateSendSapAgain({
                dataIds: idArr().toString()
            }).then(function (res) {
                if (res.success) {
                    $scope.list($scope.listParam());
                }
                $scope.Tools.alert(res.message);
            })
        };
        $scope.sendCrmAgainClick = function () {
            var idArr = getIdArr;
            if (idArr().length <= 0) {
                $scope.Tools.alert("请选择要重传的数据!");
            }
            wmsJmHandoverOperationService.updateSendCrmAgain({
                dataIds: idArr().toString()
            }).then(function (res) {
                if (res.success) {
                    $scope.list($scope.listParam());
                }
                $scope.Tools.alert(res.message);
            })
        };
        $scope.updateDcsSuccessClick = function () {
            var idArr = getIdArr;
            if (idArr().length <= 0) {
                $scope.Tools.alert("请选择要重传的数据!");
            }
            var ckConfirm = $scope.Tools.dialogConfirm("确认:确认要对选择的数据进行该操作吗?");
            ckConfirm.then(function (isConfirm) {
                if (isConfirm) {
                    wmsJmHandoverOperationService.updateDcsToSuccess({
                        dataIds: idArr().toString()
                    }).then(function (res) {
                        if (res.success) {
                            $scope.list($scope.listParam());
                        }
                        $scope.Tools.alert(res.message);
                    })
                }
            });
        };
        $scope.updateSapSuccessClick = function () {
            var idArr = getIdArr;
            if (idArr().length <= 0) {
                $scope.Tools.alert("请选择要重传的数据!");
            }
            var ckConfirm = $scope.Tools.dialogConfirm("确认:确认要对选择的数据进行该操作吗?");
            ckConfirm.then(function (isConfirm) {
                if (isConfirm) {
                    wmsJmHandoverOperationService.updateSapToSuccess({
                        dataIds: idArr().toString()
                    }).then(function (res) {
                        if (res.success) {
                            $scope.list($scope.listParam());
                        }
                        $scope.Tools.alert(res.message);
                    })
                }
            });
        };
        $scope.updateCrmSuccessClick = function () {
            var idArr = getIdArr;
            if (idArr().length <= 0) {
                $scope.Tools.alert("请选择要重传的数据!");
            }
            var ckConfirm = $scope.Tools.dialogConfirm("确认:确认要对选择的数据进行该操作吗?");
            ckConfirm.then(function (isConfirm) {
                if (isConfirm) {
                    wmsJmHandoverOperationService.updateCrmToSuccess({
                        dataIds: idArr().toString()
                    }).then(function (res) {
                        if (res.success) {
                            $scope.list($scope.listParam());
                        }
                        $scope.Tools.alert(res.message);
                    })
                }
            });
        };
    }])

    //出库记录盘点 ctrl -- 所有客户数据
    .controller('outboundAllVerificationCtrl', ['$scope', 'inventoryVerificationService', 'dropDownService', 'ngDialog','$timeout',
        function (scope, inventoryVerificationService, dropDownService, ngDialog,$timeout) {
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k];
            }
            return str;
        };
        //导出
        scope.export = function () {
            if(!scope.search.customerCode){
                scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.WMS_ALL;
            }
            scope.search.userId = scope.userInfo.id;
            scope.excelUrl = contextPath + 'inventoryManager/getOutboundManageExport' + scope.formatData(scope.search);
            $timeout(function () {
                document.getElementById('islExport').click();
            }, 20)
        };

        //下拉对象
        scope.dropDownList = {
            customerList: [
                {name: '江铃客户', code: 'JL'},
                {name: '君马客户', code: 'JM'}
            ]
        };
        scope.dropDownList.customerList.unshift(scope.constant.defaultDropDownNullData);  //插入空值
        //下拉默认值
        scope.dropDownDefault = {
            customerList: scope.dropDownList.customerList[0]
        };

        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function () {
            scope.otherForm = !scope.otherForm;
            if (scope.otherForm) {
                scope.moreText = scope.constant.moreText.packUp;
            } else {
                scope.moreText = scope.constant.moreText.spread;
            }
        };
        scope.search = {};
        //列表查询接口参数构造
        scope.listParam = function () {
            if (!scope.search) {
                scope.search = {};
            }
            scope.search.pageSize = scope.pageSize;    //设置最大页
            if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            } else {  //第一次查询设置为　第一页
                scope.search.pageNo = scope.constant.pageNo;
            }
            if (scope.otherForm) {    //展开了更多
                scope.search.customerCode = scope.dropDownDefault.customerList.code;
                return scope.search;
            } else {  //没有展开
                return {};
            }
        };

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;
            if (!params.customerCode) {
                //查询所有客户
                params.customerCode = scope.constant.inventoryVerificationCustomerCode.WMS_ALL;
            }
            inventoryVerificationService.getOutboundManageList(params)
                .then(function (result) {
                    if (result.success) {
                        scope.outboundRecordList = result.data;
                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.pageVo.totalPage,
                                curr: result.pageVo.pageNo,
                                count: result.pageVo.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if (!first) { //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }

                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        };

        //列表
        scope.list();

        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        };

        //重置 click
        scope.resetClick = function () {
            scope.search = {};
            scope.dropDownDefault.customerList = scope.dropDownList.customerList[0];
        };

        //刷新 click
        scope.refreshClick = function () {
            scope.searchClick();
        }
    }])
    .controller('outboundAllVerificationCtrl.dialog', ['$scope', 'inventoryVerificationService', '$timeout', 'ngDialog', function (scope, inventoryVerificationService, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.search = {
            startTime: '',
            endTime: '',
            customerCode: scope.constant.inventoryVerificationCustomerCode.WMS_ALL,
            userId: scope.userInfo.id
        };
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k]
            }
            return str;
        };
        scope.getData = function (e) {
            if ((scope.search.startTime && scope.search.endTime )) {
                scope.excelUrl = contextPath + 'inventoryManager/getOutboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('wrlExport').click();
                    scope.search.startTime = '';
                    scope.search.endTime = '';
                    ngDialog.closeAll()
                }, 20)
            } else {
                e.preventDefault();
                scope.Tools.alert('请选择时间！')
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }])
    //出库记录盘点 ctrl --君马客户数据展示
    .controller('outboundJMVerificationCtrl', ['$scope', 'inventoryVerificationService', 'dropDownService', 'ngDialog','$timeout',
        function (scope, inventoryVerificationService, dropDownService, ngDialog,$timeout) {
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k];
            }
            return str;
        };
        //导出
        scope.export = function () {
            if(!scope.search.customerCode){
                scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.JM;
            }
            scope.search.userId = scope.userInfo.id;
            scope.excelUrl = contextPath + 'inventoryManager/getOutboundManageExport' + scope.formatData(scope.search);
            $timeout(function () {
                document.getElementById('islExport').click();
            }, 20)
        };

        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function () {
            scope.otherForm = !scope.otherForm;
            if (scope.otherForm) {
                scope.moreText = scope.constant.moreText.packUp;
            } else {
                scope.moreText = scope.constant.moreText.spread;
            }
        };
        scope.search = {};
        //列表查询接口参数构造
        scope.listParam = function () {

            if (!scope.search) {
                scope.search = {};
            }
            scope.search.pageSize = scope.pageSize;    //设置最大页
            if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            } else {  //第一次查询设置为　第一页
                scope.search.pageNo = scope.constant.pageNo;
            }
            if (scope.otherForm) {    //展开了更多
                return scope.search;
            } else {  //没有展开
                return {};
            }
        };

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;
            //查询所有客户
            params.customerCode = scope.constant.inventoryVerificationCustomerCode.JM;
            inventoryVerificationService.getOutboundManageList(params)
                .then(function (result) {
                    if (result.success) {
                        scope.outboundRecordList = result.data;
                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.pageVo.totalPage,
                                curr: result.pageVo.pageNo,
                                count: result.pageVo.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if (!first) { //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }

                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        };

        //列表
        scope.list();

        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        };

        //重置 click
        scope.resetClick = function () {
            scope.search = {};
            // scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
        };

        //刷新 click
        scope.refreshClick = function () {
            scope.searchClick();
        }
    }])
    .controller('outboundJMVerificationCtrl.dialog', ['$scope', 'warehouseRecordExportService', '$timeout', 'ngDialog', function (scope, warehouseRecordExportService, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.search = {
            startTime: '',
            endTime: '',
            customerCode: scope.constant.inventoryVerificationCustomerCode.JM,
            userId: scope.userInfo.id
        };
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k]
            }
            return str;
        };
        scope.getData = function (e) {
            if ((scope.search.startTime && scope.search.endTime )) {
                scope.excelUrl = contextPath + 'inventoryManager/getOutboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('wrlExport').click();
                    scope.search.startTime = '';
                    scope.search.endTime = '';
                    ngDialog.closeAll()
                }, 20)
            } else {
                e.preventDefault();
                scope.Tools.alert('请选择时间！')
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }])
    //出库记录盘点 ctrl -- 江铃客户数据展示
    .controller('outboundJLVerificationCtrl', ['$scope', 'inventoryVerificationService', 'dropDownService', 'ngDialog','$timeout',
        function (scope, inventoryVerificationService, dropDownService, ngDialog,$timeout) {
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k];
            }
            return str;
        };
        //导出
        scope.export = function () {
            if(!scope.search.customerCode){
                scope.search.customerCode = scope.constant.inventoryVerificationCustomerCode.JL;
            }
            scope.search.userId = scope.userInfo.id;
            scope.excelUrl = contextPath + 'inventoryManager/getOutboundManageExport' + scope.formatData(scope.search);
            $timeout(function () {
                document.getElementById('islExport').click();
            }, 20)
        };
        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function () {
            scope.otherForm = !scope.otherForm;
            if (scope.otherForm) {
                scope.moreText = scope.constant.moreText.packUp;
            } else {
                scope.moreText = scope.constant.moreText.spread;
            }
        };
        scope.search = {};
        //列表查询接口参数构造
        scope.listParam = function () {

            if (!scope.search) {
                scope.search = {};
            }
            scope.search.pageSize = scope.pageSize;    //设置最大页
            if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            } else {  //第一次查询设置为　第一页
                scope.search.pageNo = scope.constant.pageNo;
            }

            if (scope.otherForm) {    //展开了更多
                // scope.search.otVehicleSpecName = scope.dropDownDefault.vehicleSpec.name;   //车型
                // scope.search.otOutboundFlag = scope.dropDownDefault.shipmentStatus.value;   //发运状态
                return scope.search;
            } else {  //没有展开
                return {};
            }
        };

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;
            //查询所有客户
            params.customerCode = scope.constant.inventoryVerificationCustomerCode.JL;
            inventoryVerificationService.getOutboundManageList(params)
                .then(function (result) {
                    if (result.success) {
                        scope.outboundRecordList = result.data;
                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.pageVo.totalPage,
                                curr: result.pageVo.pageNo,
                                count: result.pageVo.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if (!first) { //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }

                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        };

        //列表
        scope.list();

        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        };

        //重置 click
        scope.resetClick = function () {
            scope.search = {};
            // scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
        };

        //刷新 click
        scope.refreshClick = function () {
            scope.searchClick();
        }
    }])
    .controller('outboundJLVerificationCtrl.dialog', ['$scope', 'warehouseRecordExportService', '$timeout', 'ngDialog', function (scope, warehouseRecordExportService, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.search = {
            startTime: '',
            endTime: '',
            customerCode: scope.constant.inventoryVerificationCustomerCode.JL,
            userId: scope.userInfo.id
        };
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k]
            }
            return str;
        };
        scope.getData = function (e) {
            if ((scope.search.startTime && scope.search.endTime )) {
                scope.excelUrl = contextPath + 'inventoryManager/getOutboundManageExport' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('wrlExport').click();
                    scope.search.startTime = '';
                    scope.search.endTime = '';
                    ngDialog.closeAll()
                }, 20)
            } else {
                e.preventDefault();
                scope.Tools.alert('请选择时间！')
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }]);
