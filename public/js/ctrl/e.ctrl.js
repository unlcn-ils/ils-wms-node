/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
wmsCtrl.controller('entranceCardCtrl',['$scope','entranceCardService',function(scope,entranceCardService){    //门禁卡 ctrl

    //更多条件默认值
    scope.moreText = scope.constant.moreText.spread;
    //更多条件
    scope.moreClick = function(){
        scope.otherForm = !scope.otherForm;
        if(scope.otherForm){
            scope.moreText = scope.constant.moreText.packUp;
        }else{
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    /*状态数据*/
    scope.dropDownList = {
        status : scope.Tools.dropDown.entranceCardStatusDropDown.concat()    //验车状态
    }
    scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData);//插入验车状态空数据

    //设置默认值
    scope.dropDownDefault = {
        status :  scope.dropDownList.status[0] //状态
    }

    //列表查询接口参数构造
    scope.listParam = function(){

        if(!scope.search){
            scope.search = {};
        }

        scope.search.pageSize = scope.pageSize;    //设置最大页
        if(!scope.fristSelect){ //不是第一次查询 则设置当前页
            scope.search.pageNo = scope.pageNo;
        }else{  //第一次查询设置为　第一页
            scope.search.pageNo = scope.constant.pageNo;
        }

        if(scope.otherForm){    //展开了更多
            scope.search.status = scope.dropDownDefault.status.value;   //状态
            scope.search.driverUserName = scope.dropDownDefault.driverUser.username;   //司机用户名
            return scope.search;
        }else{  //没有展开
            return {
                atVin : scope.search.atVin, //车架号
                pageSize : scope.search.pageSize,   //最大页数
                pageNo : scope.search.pageNo    //当前页
            }
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function(params){
        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

        entranceCardService.list(angular.toJson(params))
            .then(function(result){
                if(result.success){
                    scope.entranceCardList = result.data.wmsBorrowCarList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.totalPage,
                            curr : result.data.pageNo,
                            count : result.data.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //回车按下查询
    scope.searchListKeyDown = function(e){
        var keycode = window.event ? e.keyCode:e.which;
        if(keycode == 13){  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function(){
        scope.search = {};
        scope.dropDownDefault.status = scope.dropDownList.status[0];
        scope.dropDownDefault.driverUser = scope.dropDownList.driverUser[0];
    }

    //刷新 click
    scope.refreshClick = function(){
        scope.searchClick();
    }

    //添加 门禁卡  click
    scope.addEntranceCardClick = function(){
        //获取司机用户
        entranceCardService.queryDriverUser()
            .then(function(result){
                if(result.success){
                    scope.Tools.dialog({
                        controller : 'entranceCardCtrl.dialog',
                        template : 'templates/systemManagement/entranceCardManagement/entrance-card-form.html',
                        closeByDocument : false,
                        closeByEscape : false,
                        data : {
                            status : scope.Tools.dropDown.entranceCardStatusDropDown,   //状态
                            driverUser : result.data,   //司机用户
                            dropDownDefault : {
                                status : scope.Tools.dropDown.entranceCardStatusDropDown[0],    //状态默认值
                                driverUser : result.data[0] //司机用户默认值
                            }
                        },
                        width : 550,
                        title : '新增门禁卡',
                        scope : scope
                    })
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //更新门禁卡 click
    scope.updateEntranceCardClick = function(){

        var indexArr = scope.Tools.getTableSelectItemIndex('.entranceCardTable');  //获取table所选的下标

        var entranceCardList = scope.Tools.getDataTOIndex(indexArr,scope.entranceCardList);

        if(entranceCardList.length == 0){
            scope.Tools.alert('请选择一条数据更新');
            return;
        }

        if(entranceCardList.length > 1){
            scope.Tools.alert('只能选择一条数据更新');
            return;
        }

        var entraceCard = {
            cardNum : entranceCardList[0].cardNum,  //门禁卡号
            name : entranceCardList[0].name,    //司机名称
            id : entranceCardList[0].id,    //id
            status : entranceCardList[0].status,    //状态
            driverUserName : entranceCardList[0].driverUserName //司机账号
        }

        //设置状态默认值
        var defaultStatus = null;
        for(var i = 0; i < scope.Tools.dropDown.entranceCardStatusDropDown.length;i++){
            if(entraceCard.status == scope.Tools.dropDown.entranceCardStatusDropDown[i].value){
                defaultStatus = scope.Tools.dropDown.entranceCardStatusDropDown[i];
            }
        }

        //获取司机用户
        entranceCardService.queryDriverUser()
            .then(function(result){
                if(result.success){

                    //设置司机用户默认值
                    var defaultDriver = null;
                    for(var i = 0; i < result.data.length;i++){
                        if(entraceCard.driverUserName == result.data[i].username){
                            defaultDriver = result.data[i];
                        }
                    }
                    scope.Tools.dialog({
                        controller : 'entranceCardCtrl.dialog',
                        template : 'templates/systemManagement/entranceCardManagement/entrance-card-form.html',
                        closeByDocument : false,
                        closeByEscape : false,
                        data : {
                            status : scope.Tools.dropDown.entranceCardStatusDropDown,   //状态
                            driverUser : result.data,   //司机用户
                            dropDownDefault : {
                                status : defaultStatus,    //状态默认值
                                driverUser : defaultDriver //司机用户默认值
                            },
                            entranceCard : entraceCard
                        },
                        width : 550,
                        title : '更新门禁卡',
                        scope : scope
                    })
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //获取司机用户
    entranceCardService.queryDriverUser()
        .then(function(result){
            if(result.success){
                scope.dropDownList.driverUser = result.data;    //司机用户
                scope.dropDownList.driverUser.unshift(scope.constant.defaultDropDownNullData);//插入司机用户空数据
                scope.dropDownDefault.driverUser = scope.dropDownList.driverUser[0] //司机用户默认值
            }else{
                scope.Tools.alert(result.message);
            }
        })
}])
.controller('entranceCardCtrl.dialog',['$scope','entranceCardService',function(scope,entranceCardService){    //门禁卡 dialog

    //保存或更新
    scope.saveOrUpdateClick = function(){
        if(!scope.ngDialogData.entranceCard){
            scope.ngDialogData.entranceCard = {};
        }
        scope.ngDialogData.entranceCard.driverUserName =  scope.ngDialogData.dropDownDefault.driverUser.username;   //司机账号
        scope.ngDialogData.entranceCard.driverId = scope.ngDialogData.dropDownDefault.driverUser.id;    //司机id
        scope.ngDialogData.entranceCard.status =  scope.ngDialogData.dropDownDefault.status.value;   //状态

        if(!scope.ngDialogData.entranceCard.driverUserName && !scope.ngDialogData.entranceCard.driverId){
            scope.Tools.alert('请选择司机');
            return;
        }

        if(scope.ngDialogData.entranceCard && scope.ngDialogData.entranceCard.id){  //更新

            entranceCardService.updateCard(scope.ngDialogData.entranceCard)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('更新成功');
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }else{  //新增
            entranceCardService.addCard(scope.ngDialogData.entranceCard)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('新增成功');
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }

    //司机选择改变
    scope.driverUserChange = function(item){
        if(!scope.ngDialogData.entranceCard){
            scope.ngDialogData.entranceCard = {};
        }
        if(item){
            scope.ngDialogData.entranceCard.name = item.name;   //司机名称
        }
    }

    if(!scope.ngDialogData.entranceCard || !scope.ngDialogData.entranceCard.id){  //如果是更新则不自动选择司机名称
        scope.driverUserChange(scope.ngDialogData.dropDownDefault.driverUser);  //默认赋值name
    }
}])