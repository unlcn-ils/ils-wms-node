/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';

wmsCtrl.controller('distributionCtrl',['$scope','$rootScope','$timeout','$state','Tools','distributionService',function($scope,$rootScope,$timeout,$state,$Tools,distributionService){

    $scope.pt1 =  {
        title: {
            x: 'center',
            text: '配送',
            textStyle: {
                color: '#fff',
                fontSize: '30'
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                crossStyle: {
                    color: '#999',
                    fontSize: '30'
                }
            }
        },
        toolbox: {
            feature: {
                dataView: {show: true, readOnly: false},
                magicType: {show: true, type: ['line', 'bar']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        color: ['#5086d6', '#f67c2b', '#a5a5a5','#ffc000'],
        legend: {
            bottom: 10,
            left: 'center',
            data: ['发运数', '在途数', '运抵数', '配送及时率'],
            textStyle: {
                color: '#fff'
            }
        },
        xAxis: [
            {
                type: 'category',
                data: ['-', '-', '-', '-', '-'],
                axisPointer: {
                    type: 'shadow'
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#000',
                        opacity:0.3
                    }
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '数量',
                min: 0,
                max: 300,
                interval: 30,
                axisLabel: {
                    formatter: '{value} 次',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#464646'
                    }
                }
            },
            {
                type: 'value',
                name: '比率',
                min: 0,
                max: 100,
                interval: 10,
                axisLabel: {
                    formatter: '{value} %',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#464646'
                    }
                }
            }
        ],
        series: [
            {
                name: '发运数',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '在途数',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '运抵数',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '配送及时率',
                type: 'line',
                yAxisIndex: 1,
                data: [0, 0, 0, 0, 0],
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            formatter: '{c}%',
                            color:'#fff'
                        },
                        lineStyle: {
                            color: '#ffc000'
                        }
                    }
                }
            }
        ]
    };

    $rootScope.db_load = function(a){
            var boxObj = document.getElementById('bs_db_box');
            var canvasBox = document.getElementById('bs_db_canvasBox');

            var boxW = boxObj.offsetWidth;
            var boxH = boxObj.offsetHeight;

            canvasBox.style.width  = boxW +'px';
            canvasBox.style.height = boxH +'px';

            var box = echarts.init(canvasBox);
            box.setOption(a);
    };

    //获取主报表
    distributionService.distributionChartCount().then(function(res){
        if(res.success && res.data!=null){
            $Tools.bigScreenGetYMax($scope.pt1,res.data);
            var r_d = res.data;
            var pt_D = $scope.pt1.series;
            var temp = [];
            for(var i=0; i< r_d.length;i++){
                if(r_d[i].data == null){
                    var tp =[];
                    for(var j = 0; j < r_d[i].rate.length; j++){
                        if(r_d[i].rate[j]){
                            r_d[i].rate[j] = parseFloat(r_d[i].rate[j]*100).toFixed(2);
                        } else {
                            r_d[i].rate[j] = 0;
                        }
                    }
                    pt_D[i].data = r_d[i].rate;
                } else {
                    pt_D[i].data = r_d[i].data
                }
                pt_D[i].name = r_d[i].name;                                                   // 加载柱状图
                temp.push(r_d[i].name);
            }
            $scope.pt1.legend.data = temp;                                                     //  加载条件注释
            $scope.pt1.xAxis[0].data = r_d[0].date;                                           //  加载x轴数据

            var update = '更新时间: '+ $rootScope.getNowFormatDate();
            $scope.pt1.title.subtext = update;

            $rootScope.db_load($scope.pt1);
        }
    });

    //获取表格
    distributionService.distributionLineCount().then(function(res){
            if(res.success){
                $scope.tableList = res.data;
            }
    });

    window.clearTimeout($rootScope.bs_timer1);
    window.clearTimeout($rootScope.bs_timer2);
    window.clearTimeout($rootScope.bs_timer3);
    window.clearTimeout($rootScope.bs_timer4);
    window.clearTimeout($rootScope.bs_timer5);
    window.clearTimeout($rootScope.onwayIntertime);

    $rootScope.bs_timer4 = setTimeout(function(){
        $state.go('drayOutIn')
    },$rootScope.bs_intervalTime*60000)

}])
    .controller('drayOutInCtrl',['$scope','$rootScope','$timeout','$state','Tools','drayOutInService',function($scope,$rootScope,$timeout,$state,$Tools,drayOutInService){

        $scope.pt1 = {
            title: {
                x: 'center',
                text: '板车进出场',
                textStyle: {
                    color: '#fff',
                    fontSize: '30'
                }
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999',
                        fontSize: '30'
                    }
                }
            },
            toolbox: {
                feature: {
                    dataView: {show: true, readOnly: false},
                    magicType: {show: true, type: ['line', 'bar']},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            color: ['#5086d6','#fec500', '#f67c2b', '#a5a5a5'],
            legend: {
                bottom: 10,
                left: 'center',
                data: ['进场板数', '出场板数', '进场及时率', '出场及时率'],
                textStyle: {
                    color: '#fff'
                }
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['-', '-', '-', '-', '-'],
                    axisPointer: {
                        type: 'shadow'
                    },
                    axisLabel: {
                        textStyle: {
                            color: '#fff'
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#000',
                            opacity:0.3
                        }
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: '数量',
                    min: 0,
                    max: 20,
                    interval: 2,
                    axisLabel: {
                        formatter: '{value} 台',
                        textStyle: {
                            color: '#fff',
                            opacity:0
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#464646'
                        }
                    }
                },
                {
                    type: 'value',
                    name: '比率',
                    min: 0,
                    max: 100,
                    interval: 10,
                    axisLabel: {
                        formatter: '{value} %',
                        textStyle: {
                            color: '#fff'
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#464646'
                        }
                    }
                }
            ],
            series: [
                {
                    name: '进场板数',
                    type: 'bar',
                    data: [0, 0, 0, 0, 0],
                    barGap: 0,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true
                            }
                        }
                    }
                },
                {
                    name: '出场板数',
                    type: 'bar',
                    data: [0, 0, 0, 0, 0],
                    barGap: 0,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true
                            }
                        }
                    }
                }
                // {
                //     name: '进场及时率',
                //     type: 'line',
                //     yAxisIndex: 1,
                //     data: [0, 0, 0, 0, 0],
                //     barGap: 0,
                //     itemStyle: {
                //         normal: {
                //             label: {
                //                 show: true,
                //                 formatter: '{c}%'
                //             },
                //             lineStyle: {
                //                 color: '#f67c2b'
                //             }
                //         }
                //     }
                // },
                // {
                //     name: '出场及时率',
                //     type: 'line',
                //     yAxisIndex: 1,
                //     data: [0, 0, 0, 0, 0],
                //     barGap:0,
                //     itemStyle: {
                //         normal: {
                //             label: {
                //                 show: true,
                //                 formatter: '{c}%',
                //                 color:'#fff'
                //             },
                //             lineStyle: {
                //                 color: '#a5a5a5'
                //             }
                //         }
                //     }
                // }
            ]
        };

        $rootScope.td2_load = function(a){

            var boxObj = document.getElementById('bs_td2_box');
            var canvasBox = document.getElementById('bs_td2_canvasBox');

            var boxW = boxObj.offsetWidth;
            var boxH = boxObj.offsetHeight;

            canvasBox.style.width  = boxW+'px';
            canvasBox.style.height = boxH+'px';

            var box = echarts.init(canvasBox);

            box.setOption(a);
        };

        // $rootScope.td2_load($scope.pt1);

        //获取主报表
        drayOutInService.inOutDrayChartCount().then(function(res){
            if(res.success){
                $Tools.bigScreenGetYMax($scope.pt1,res.data);
                var r_d = res.data;
                var pt_D = $scope.pt1.series;
                var temp = [];
                for(var i=0; i< r_d.length-2;i++){
                    if(r_d[i].data == null){
                        var tp =[];
                        for(var j = 0; j < r_d[i].rate.length; j++){
                            if(r_d[i].rate[j]){  //检验是否为空
                                r_d[i].rate[j] = parseFloat(r_d[i].rate[j]*100).toFixed(2);
                            } else {
                                r_d[i].rate[j] = 0;
                            }
                        }
                        pt_D[i].data = r_d[i].rate;
                    } else {
                        pt_D[i].data = r_d[i].data
                    }
                    pt_D[i].name = r_d[i].name;                                                   // 加载柱状图
                    temp.push(r_d[i].name);
                }
                $scope.pt1.legend.data = temp;                                                     //  加载条件注释
                $scope.pt1.xAxis[0].data = r_d[0].date;                                           //  加载x轴数据

                var update = '更新时间:'+ $rootScope.getNowFormatDate();
                $scope.pt1.title.subtext = update;

                $rootScope.td2_load($scope.pt1);

            }
        });

        //获取表格
        drayOutInService.inOutDrayLineCount().then(function(res){
            if(res.success){
                $scope.tableList = res.data;
            }
        });

        window.clearTimeout($rootScope.bs_timer1);
        window.clearTimeout($rootScope.bs_timer2);
        window.clearTimeout($rootScope.bs_timer3);
        window.clearTimeout($rootScope.bs_timer4);
        window.clearInterval($rootScope.bs_timer5);
        window.clearTimeout($rootScope.onwayIntertime);

       $rootScope.bs_timer3 =  setTimeout(function(){
            $state.go('distribution')
        },$rootScope.bs_intervalTime*60000)

    }])
    .controller('dealAbnormalCtrl',['$scope','$rootScope','abnormalManagementService','ngDialog',function($scope,$rootScope,abnormalManagementService,ngDialog){
        //    异常处理
        $scope.layer_dealType = '';
        $scope.updateToDeal = function(){
            var obj = $rootScope.abm_imageList[$rootScope.abm_indexArr[0]];
            var temp = {
                orderId : obj.orderId,
                excpType: obj.excpType,
                excpDesc: obj.excpDesc,
                dealType: $scope.layer_dealType,
                vin:      obj.vin,
                excpTypeCode: obj.excpTypeCode,
                excpTimestamp:obj.excpTimestamp
            };
            abnormalManagementService.updateToDeal(temp).then(function(res){
                if(res.success){
                    $rootScope.abm_getList();   //成功后刷新列表
                    ngDialog.closeAll();
                    setTimeout(function(){
                        $scope.Tools.alert(res.message);
                    },20)
                } else {
                    ngDialog.closeAll();
                    layer.open({
                        title: '提示'
                        ,content: res.message
                    });
                }
            })
        }
    }])