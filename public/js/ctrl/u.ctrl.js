/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
wmsCtrl.controller('userCtrl',['$scope','userService','$state',function(scope,userService,state){    //用户  ctrl
    //更多条件默认值
    scope.moreText = scope.constant.moreText.spread;
    //更多条件
    scope.moreClick = function(){
        scope.otherForm = !scope.otherForm;
        if(scope.otherForm){
            scope.moreText = scope.constant.moreText.packUp;
        }else{
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    /*状态数据*/
    scope.dropDownList = {
        status : scope.Tools.dropDown.statusDropDown.concat()    //状态
    }
    scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData);  //向前插入空值

    //设置默认值
    scope.dropDownDefault = {
        status :  scope.dropDownList.status[0] //状态默认值
    }

    //列表查询接口参数构造
    scope.listParam = function(){

        if(!scope.search){
            scope.search = {};
        }
        scope.search.pageSize = scope.pageSize;    //设置最大页
        if(!scope.fristSelect){ //不是第一次查询 则设置当前页
            scope.search.pageNo = scope.pageNo;
        }else{  //第一次查询设置为　第一页
            scope.search.pageNo = scope.constant.pageNo;
        }

        if(scope.otherForm){    //展开了更多
            scope.search.enable = scope.dropDownDefault.status.value;   //状态
            return scope.search;
        }else{  //没有展开
            return {};
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function(params){
        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

        userService.listUser(params)
            .then(function(result){
                if(result.success){
                    scope.userList = result.data.dataList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.page.totalPage,
                            curr : result.data.page.pageNo,
                            count : result.data.page.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function(){
        scope.search = {};
        scope.dropDownDefault.status = scope.dropDownList.status[0];
    }

    //刷新 click
    scope.refreshClick = function(){
        scope.searchClick();
    }

    //更新用户 click
    scope.updateUserClick = function(){
        var indexArr = scope.Tools.getTableSelectItemIndex('.userTable');  //获取table所选的下标

        var userList = scope.Tools.getDataTOIndex(indexArr,scope.userList);

        if(userList.length == 0){
            scope.Tools.alert('请选择需要更新的数据');
            return;
        }

        if(userList.length > 1){
            scope.Tools.alert('只能选择一条用户数据更新');
            return;
        }

        state.go('userForm',{userId : userList[0].id});
    }

    //删除用户 click
    scope.deleteUser = function(){
        var indexArr = scope.Tools.getTableSelectItemIndex('.userTable');  //获取table所选的下标
        var userList = scope.Tools.getDataTOIndex(indexArr,scope.userList);

        if(userList.length == 0){
            scope.Tools.alert('请选择需要删除的数据');
            return;
        }

        scope.Tools.dialogConfirm('确定删除选中的数据吗？')
            .then(function(res){
                if(res){
                    var ids = new Array();
                    angular.forEach(userList,function(v,k){ //封装id
                        ids.push(v.id);
                    })
                    userService.deleteSysUser({userIdList : ids})
                        .then(function(result){
                            if(result.success){
                                scope.Tools.alert('删除用户成功');
                                scope.list(scope.listParam());
                            }else{
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            })
    }

}])
.controller('userFormCtrl',['$scope','dropDownService','userService','$state','$stateParams',function(scope,dropDownService,userService,state,stateParams){

    //分配角色
    scope.assignRoleClick = function(){
        scope.Tools.dialog({
            controller : 'userFormCtrl.dialog',
            template : 'templates/systemManagement/userManagement/user-assign-role.html',
            closeByDocument : false,
            closeByEscape : false,
            data : {
            },
            width : 750,
            title : '角色分配',
            scope : scope
        })
    }

    /*状态数据*/
    scope.dropDownList = {
        sex : scope.Tools.dropDown.sexDropDown,  //性别
        status : scope.Tools.dropDown.statusDropDown,    //状态
        type : scope.Tools.dropDown.userTypeDropDown //用户类型
    }

    //设置默认值
    scope.dropDownDefault = {
        sex : scope.dropDownList.sex[0], //性别默认值
        status : scope.Tools.dropDown.statusDropDown[0],    //状态
        type : scope.Tools.dropDown.userTypeDropDown[0] //用户类型
    }

    //新增或更新
    scope.saveOrUpdateClick = function(){

        if(!scope.roleList || scope.roleList.length == 0){
            scope.Tools.alert('请分配用户角色');
            return;
        }

        if(!scope.user){
            scope.user = {};
        }

        scope.user.warehouseId = scope.dropDownDefault.warehouse.whId;  //仓库id
        scope.user.enable = scope.dropDownDefault.status.value; //状态
        scope.user.gender = scope.dropDownDefault.sex.value;    //性别
        scope.user.roleIdList = new Array();    //角色id
        scope.user.type = scope.dropDownDefault.type.value; //角色类型
        angular.forEach(scope.roleList,function(v,k){   //角色id 封装
            scope.user.roleIdList.push(v.id);
        })

        if(scope.user.id){    //更新
            userService.updateSysUser(scope.user)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('更新角色成功');
                        state.go('user');   //跳转到user
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }else{  //新增
            userService.addSysUser(scope.user)
                .then(function(result){
                    if(result.success){
                        scope.Tools.alert('新增角色成功');
                        state.go('user');   //跳转到user
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }

    //删除当前的 角色
    scope.deleteThisRoleClick = function(index){
        scope.roleList.splice(index,1);
    }

    //判断userId 是否有值
    if(stateParams.userId){
        //获取用户信息
        userService.findSysUserById(stateParams.userId)
            .then(function(result){
                if(result.success){
                    scope.user = result.data;
                    scope.roleList = result.data.roleList;
                    delete scope.user.roleList; //删除角色 list

                    //状态赋值
                    angular.forEach(scope.dropDownList.status,function(v,k){
                        if(v.value == scope.user.enable){
                            scope.dropDownDefault.status = v;
                            return;
                        }
                    })

                    //性别赋值
                    angular.forEach(scope.dropDownList.sex,function(v,k){
                        if(v.value == scope.user.gender){
                            scope.dropDownDefault.sex = v;
                            return;
                        }
                    })

                    //获取仓库
                    dropDownService.getWarehouse()
                        .then(function(result){
                            if(result.success){
                                scope.dropDownList.warehouse = result.data;
                                angular.forEach(scope.dropDownList.warehouse,function(v,k){ //赋值默认仓库
                                    if(v.whId == scope.user.warehouseId){
                                        scope.dropDownDefault.warehouse = v;
                                    }
                                })
                            }else{
                                scope.Tools.alert(result.message);
                            }
                        })

                    //用户类型
                    angular.forEach(scope.dropDownList.type,function(v,k){
                        if(v.value == scope.user.type){
                            scope.dropDownDefault.type = v;
                            return;
                        }
                    })

                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }else{
        //获取仓库
        dropDownService.getWarehouse()
            .then(function(result){
                if(result.success){
                    scope.dropDownList.warehouse = result.data;
                    scope.dropDownDefault.warehouse = result.data[0];
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }
}])
.controller('userFormCtrl.dialog',['$scope','roleService',function(scope,roleService){

    //参数构造
    scope.listParam = function () {

        if(!scope.ngDialogData.search){
            scope.ngDialogData.search = {};
        }

        return scope.ngDialogData.search;
    }

    scope.fristSelect = true;   //判断是否是第一次查询
    //角色列表
    scope.list = function(params){

        //设置分页参数
        if(!params){
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

        roleService.listRole(params)
            .then(function(result){
                if(result.success){
                    scope.roleList = result.data.dataList;
                    if(scope.fristSelect){
                        scope.Tools.page(document.getElementById('rolePages'),{
                            pages : result.data.page.totalPage,
                            curr : result.data.page.pageNo,
                            count : result.data.page.totalRecord
                        },function(obj, first){
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if(!first){ //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    }else{
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }
                }else{
                    scope.Tools.alert(result.message);
                }
            })
    }

    //获取角色
    scope.list();

    //查询 click
    scope.searchClick = function(){
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function(){
        scope.ngDialogData.search = {};
    }

    //添加角色到用户
    scope.addRoleForUser = function(){
        var indexArr = scope.Tools.getTableSelectItemIndex('.roleTable');  //获取table所选的下标

        var roleList = scope.Tools.getDataTOIndex(indexArr,scope.roleList);

        if(roleList.length == 0){
            scope.Tools.alert('请选择需要添加的数据');
            return;
        }

        //判断数据是否重复
        if(scope.$parent.roleList && scope.$parent.roleList.length > 0){
            var oldArray = scope.$parent.roleList.concat();
            for(var i = 0; i < roleList.length;i++){
                var exist = false;
                for(var j = 0; j < oldArray.length;j++){
                    if(oldArray[j].id == roleList[i].id){
                        exist = true;
                    }
                }
                if(!exist){
                    scope.$parent.roleList.push(roleList[i]);
                }
            }
        }else{
            scope.$parent.roleList = roleList;
        }

        scope.closeThisDialog();
    }
}])
    .controller('urlCtrl',['$scope','urlService',function(scope,urlService){
        scope.tableList = ''
        scope.getList = function(){
            urlService.getUrlList().then(function(res){
                if(res.success){
                    scope.tableList = res.data
                }
            })
        };
        scope.getList();
        scope.updateUrl = function(){
                urlService.editeUrl({
                    channel:scope.tableList[0].channel,
                    id:scope.tableList[0].id,
                    whCode: scope.userInfo.whCode
                }).then(function(res){
                    if(res.success){
                        scope.Tools.alert('更新成功');
                    } else {
                        scope.Tools.alert(res.message);
                    }
                })
        }
    }]);