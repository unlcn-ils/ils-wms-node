/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';

wmsCtrl.controller('nullCtrl',['$scope','$rootScope','ngDialog','$timeout',function($scope,$rootScope,ngDialog,$timeout){

    $scope.bs_time_layer = '';

    //关闭弹框
    $scope.bcClose = function(){
        $timeout(function(){
            $rootScope.bs_intervalTime = $scope.bs_time_layer;
        },2);
        ngDialog.closeAll();
    };
}]);