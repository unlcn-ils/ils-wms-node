/**
 * Created by lihao on 2017/10/20.  borrowCarRegistration
 */
'use strict';
wmsCtrl.controller('borrowCarRegistrationCtrl',['$scope','Tools','$rootScope','$http','$timeout','ngDialog','borrowCarRegistrationService','FileUploader',function($scope,Tools,$rootScope,$http,$timeout,ngDialog,borrowCarRegistrationService,FileUploader){

    //新增
    if($rootScope.bcrAddFlag){
        $scope.addFlag = false;
        var bcidsArr = [];
        $('.wms_bcr_table').find('td i').each(function(i,obj) {
            if ($(obj).parent().hasClass('layui-form-checked')) {
                var id = $(obj).parents('td').attr('bcid');
                bcidsArr.push(id);
            }
        });
        borrowCarRegistrationService.queryModifyDetail(bcidsArr[0]).then(function(data){
            if(data.success){
                $scope.addObj = data.data[0];
                $('.bcr-add-image').css('background-image','url('+$scope.addObj.uploadPath+')');
                $rootScope.rcrAddFlag = false;

                $rootScope.bcrAddFlag = false;
                //获取七牛token
                borrowCarRegistrationService.getUploadToken().then(function(result){
                    if (result.success) {
                        $scope.brc_token = result.data;
                        var imgKey = null;

                        var uploader = $rootScope.uploaderBRC = new FileUploader({
                            url: 'http://upload.qiniu.com/',
                            formData: [{
                                token: result.data
                            }],
                            queueLimit: 1,
                            autoUpload: true
                        });
                        $rootScope.brcFlag = true;
                        uploader.filters.push({
                            name: 'filterName',
                            fn: function (item /*{File|FileLikeObject}*/, options) {
                                $scope.imageUploadFlag = true;
                                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                            }
                        });
                        uploader.onSuccessItem = function (fileItem, response, status, headers) {
                            console.log("上传成功");
                            $scope.addObj.uploadKey = response.key;
                            $rootScope.uploadKey = response.key;
                            console.log($scope.addObj);

                            //获取图片url
                            borrowCarRegistrationService.getDownUrl(response.key).then(function (result) {
                                if (result.success) {

                                    $('.bcr-add-image').css("background","url("+result.data+")");
                                    $('.bcr-add-image').css("background-size","100%");
                                    $('.bcr-add-image').css("background-repeat","no-repeat");
                                }
                            }).finally(function () {
                                layer.close(index);
                            });
                        };
                        uploader.onProgressAll = function(progress) {
                            $("#progress"+$scope.index).css('width',(progress-10)*1.32+'px');
                            console.info('onProgressAll', progress);
                        };

                        uploader.onErrorItem = function (fileItem, response, status, headers) {
                            alert("文件" + fileItem.file.name + "上传失败！", "提示");
                            uploader.removeFromQueue(fileItem);
                            console.info('上传失败的文件', fileItem, response, status, headers);
                        };

                    }
                });

            }
        });
    } else {
        $scope.addFlag = true;
    }

        $scope.addNew = function(){

            ngDialog.open({
                template: "templates/vehicleBorrowingManagement/layer/bcr-addNew.html",
                controller: "borrowCarRegistrationCtrl",
                title: $rootScope.bcrAddFlag?'编辑借用信息':'借用登记',
                width:710
            });

            //加载完页面后再渲染下页面
            $timeout(function(){
                layui.use('form',function(){
                    var form = layui.form();
                    form.render();
                    //全选
                })
            },50);
        };
    //    添加新增单
    $scope.addObj = {
        bcBorrowName:'',
        bcBorrowDepartment:'',
        bcBorrowTel:'',
        bcBorrowDate:'',
        bcEstimatedReturnDate:'',
        bcBorrowReason:'',
        uploadKey:'',
        wmsBorrowCarDetailList:[]

    };
    $scope.wmsTemp = {
        bdId:null,
        bdVin: '',
        bdCarSeries:'',
        bdCarSpecname:'',
        bdCarColorname:'',
        num:''
    };

    $scope.isNull = function(obj){
        for(var key in obj ){
            if(obj[key]==''){
                return true
            }
        }
        return  false
    };

    //添加车辆
    $scope.addObj.wmsBorrowCarDetailList = [];

    $scope.count =1;
    $scope.addCar = function(){
        $scope.addbcrInfo = true;

        if( $scope.wmsTemp.bdVin || ($scope.wmsTemp.bdCarSpecname && $scope.wmsTemp.bdCarColorname )){
            $scope.count++;
            $scope.addObj.wmsBorrowCarDetailList.push($scope.wmsTemp);
            //加载完页面后再渲染下页面
            $timeout(function(){
                layui.use('form',function(){
                    var form = layui.form();
                    form.render();
                    //全选
                })
            },50);

            $scope.wmsTemp = {
                bdId:null,
                bdVin : '',
                bdCarSeries:'',
                bdCarSpecname:'',
                bdCarColorname:'',
                num:''
            };
        } else if( $scope.count !=1){
            $scope.Tools.alert('车架号 或 （车型颜色） 至少填写一组');
        }
    };

    //删除车辆信息
    $scope.delCar = function(){
        var indexArr = [];
        $('.wmsCarInfo').find('td i').each(function(i,obj) {
            if ($(obj).parent().hasClass('layui-form-checked')) {
                var index = $(obj).parents('td').attr('index');
                indexArr.push(index);
            }
        });
        for(var i=0; i<indexArr.length;i++){
            $scope.addObj.wmsBorrowCarDetailList.splice(indexArr[i],1)
        }
    };

    //关闭弹框
    $scope.bcClose = function(){

        $scope.addFlag = true;  //标记保存位置
        ngDialog.closeAll();
    };

    //保存编辑
    $scope.saveEdite = function(){
        if($scope.Tools.isRange($scope.addObj.bcBorrowDate,$scope.addObj.bcEstimatedReturnDate)) {
            borrowCarRegistrationService.modify($scope.addObj).then(function (data) {
                if (data.success) {
                    $scope.Tools.alert('编辑保存成功');

                    $scope.addFlag = true;  //标记保存位置

                    ngDialog.closeAll();
                    $scope.getList();
                    //刷新后保存当前被选择项
                    $timeout(function () {
                        $('.wms_bcr_table').find('td i').eq($rootScope.currEQ).parent().addClass('layui-form-checked');
                        $('.wms_bcr_table').find('td i').eq($rootScope.currEQ).parents('tr').addClass('trSelect');
                    }, 200);

                    //    获取详细信息
                    borrowCarRegistrationService.findBorrowDetail($rootScope.currEditeID).then(function (result) {
                        if (result.success) {
                            $rootScope.bcDetail = result.data;
                            //刷新后保存当前被选择项
                            $timeout(function () {
                                $('.wms_bcr_table').find('td i').eq($rootScope.currEQ).parent().addClass('layui-form-checked');
                                $('.wms_bcr_table').find('td i').eq($rootScope.currEQ).parents('tr').addClass('trSelect');
                                $rootScope.currEQ = '';
                            }, 200);
                        }
                    })
                } else {
                    $scope.Tools.alert(data.message);
                }
            })
        } else{
            $scope.Tools.alert('预计还车时间必须大于等于借车时间！')
        }
    };
    $scope.addbcrInfo = false;


    //删除
    $scope.delect = function(){
        var bcidsArr = [];
        $('.wms_bcr_table').find('td i').each(function(i,obj) {
            if ($(obj).parent().hasClass('layui-form-checked')) {
                var id = $(obj).parents('td').attr('bcid');
                bcidsArr.push(id);
            }
        });
        if(bcidsArr.length==0){
            $scope.Tools.alert('未选择借用单');
        } else{
            borrowCarRegistrationService.del(bcidsArr).then(function(data){
                if(data.success){
                    $scope.Tools.alert('删除成功！');
                    $rootScope.bcDetail = '';
                    $scope.getList();
                } else {
                    $scope.Tools.alert(data.message);
                }
            })
        }
    };

    $scope.bcPrint = function(){

        if(!$scope.Tools.checkPrintInstall()){   //打印插件检测
            $scope.Tools.downLoadPrintPlugIn();    //提示下载打印插件
            return;
        }

        var bcidsArr = [];
        $('.wms_bcr_table').find('td i').each(function(i,obj) {
            if ($(obj).parent().hasClass('layui-form-checked')) {
                var id = $(obj).parents('td').attr('bcid');
                bcidsArr.push(id);
            }
        });

        if(bcidsArr.length == 1){
            $rootScope.currPrintID = bcidsArr[0];
            ngDialog.open({
                template: "templates/vehicleBorrowingManagement/layer/printHTML.html",
                controller: "bcrPrintCtrl",
                title: '车辆借用单预览',
                width:800
            });
        } else {
            $scope.Tools.alert('只能单条打印！');
        }

    };

    $rootScope.brcFlag = false;

    //新增借用单
    $scope.addbcrBill = function(){

        $rootScope.bcrAddFlag = false;
        $scope.addNew();

        //获取七牛token
        borrowCarRegistrationService.getUploadToken().then(function(result){
            if (result.success) {
                $scope.brc_token = result.data;
                var imgKey = null;

                var uploader = $rootScope.uploaderBRC = new FileUploader({
                    url: 'http://upload.qiniu.com/',
                    formData: [{
                        token: result.data
                    }],
                    queueLimit: 1,
                    autoUpload: true
                });
                $rootScope.brcFlag = true;
                uploader.filters.push({
                    name: 'filterName',
                    fn: function (item /*{File|FileLikeObject}*/, options) {
                        $scope.imageUploadFlag = true;
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                    }
                });
                uploader.onSuccessItem = function (fileItem, response, status, headers) {
                    console.log("上传成功");
                    $scope.addObj.uploadKey = response.key;
                    $rootScope.uploadKey = response.key;
                    console.log($scope.addObj);

                    //获取图片url
                    borrowCarRegistrationService.getDownUrl(response.key).then(function (result) {
                        if (result.success) {

                            $('.bcr-add-image').css("background","url("+result.data+")");
                            $('.bcr-add-image').css("background-size","100%");
                            $('.bcr-add-image').css("background-repeat","no-repeat");
                        }
                    }).finally(function () {
                        layer.close(index);
                    });
                };
                uploader.onProgressAll = function(progress) {
                    $("#progress"+$scope.index).css('width',(progress-10)*1.32+'px');
                    console.info('onProgressAll', progress);
                };

                uploader.onErrorItem = function (fileItem, response, status, headers) {
                    alert("文件" + fileItem.file.name + "上传失败！", "提示");
                    uploader.removeFromQueue(fileItem);
                    console.info('上传失败的文件', fileItem, response, status, headers);
                };

            }
        });
    };

    $rootScope.imgSrc = '';

    //    编辑
    $scope.edite = function(){
        var bcidsArr = [];
        $('.wms_bcr_table').find('td i').each(function(i,obj) {
            if ($(obj).parent().hasClass('layui-form-checked')) {
                var id = $(obj).parents('td').attr('bcid');
                bcidsArr.push(id);
                $rootScope.currEQ = i;
            }
        });
        if(bcidsArr.length==1){

            $rootScope.bcrAddFlag = true;
            $rootScope.currEditeID = bcidsArr[0];
            $scope.addNew();

            //刷新后保存当前被选择项
            $timeout(function(){
                $('.wms_bcr_table').find('td i').eq($rootScope.currEQ).parent().addClass('layui-form-checked');
                $('.wms_bcr_table').find('td i').eq($rootScope.currEQ).parents('tr').addClass('trSelect');
            },200);

        } else if(bcidsArr.length==0){
            $scope.Tools.alert('未选择借用信息!');
        } else if (bcidsArr.length>1){
            $scope.Tools.alert('编辑只能选择一条!');
        }
    };

    layui.use('form',function(){
        var form = layui.form();
        form.render();
        //全选
        form.on('checkbox(allChoose)', function (data) {
            $(data.elem).parent().parent().parent().parent().parent().find('tr').toggleClass('trSelect');
            var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]');
            child.each(function(index, item){
                item.checked = data.elem.checked;
                if(data.elem.checked){
                    $(item).parents('tr').addClass('trSelect');
                } else {
                    $(item).parents('tr').removeClass('trSelect');
                }
            });
            form.render('checkbox');
        });

        //  前面选择框选中
        form.on('checkbox(test)',function(data){
            if(data.elem.checked){
                $(data.elem).parents('tr').addClass('trSelect');
            } else {
                $(data.elem).parents('tr').removeClass('trSelect');
            }
        });
    });

    //    显示更多条件
    $scope.moreClick = function(){
        $('.otherForm').slideToggle(500);
    };
    $rootScope.bcrMainList = '';


    $scope.fristSelect = true;

    //    获取列表
    $scope.getList = function(){
        var params = {
            pageNo:$scope.pageNo || 1,
            pageSize:$scope.pageSize || 10
        };
        params = angular.extend(params,$scope.search);
        borrowCarRegistrationService.borrowLine(params)
            .then(function(result){
                if(result.success){

                    $rootScope.bcrMainList = result.data.wmsBorrowCarList;

                    if($scope.fristSelect){
                        $scope.Tools.page(document.getElementById('pages'),{
                            pages : result.data.totalPage,
                            curr : result.data.pageNo,
                            count : result.data.totalRecord,
                            limit:result.data.pageSize
                            ,limits: [10,20,30,40,50]
                        },function(obj, first){
                            $scope.fristSelect = first;  //设置状态

                            if(!first){ //判断不是第一次加载
                                var param = {};
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                $scope.pageSize = obj.limit; //设置最大页
                                $scope.pageNo = obj.curr; //设置当前页
                                $scope.getList(param);
                            }
                        })
                    }
                }else{
                    $scope.Tools.alert(result.message);
                }
            })
    };
    $rootScope.bcrGetList = $scope.getList;


    //新增保存
    $scope.addSave = function(){

        $scope.addObj.uploadKey = $rootScope.uploadKey;
        if (!$scope.addObj.bcBorrowName || !$scope.addObj.bcBorrowDepartment || !$scope.addObj.bcBorrowTel || !$scope.addObj.bcBorrowDate || !$scope.addObj.bcEstimatedReturnDate || !$scope.addObj.bcBorrowReason || $scope.addObj.wmsBorrowCarDetailList.length==0 ){
            console.log($scope.addObj);
            $scope.Tools.alert('请完整填写 借车人信息 和 车辆信息 ！');

        } else {   //  bcBorrowDate:'',  bcEstimatedReturnDate:'',
            debugger;
            if($scope.Tools.isRange($scope.addObj.bcBorrowDate,$scope.addObj.bcEstimatedReturnDate)){
                if($scope.addObj.uploadKey){
                    borrowCarRegistrationService.add($scope.addObj).then(function(data){
                        if(data.success){
                            $scope.Tools.alert('新增保存成功');
                            ngDialog.closeAll();
                            $rootScope.bcrGetList();

                            $rootScope.bcDetail = '';

                        } else {
                            $scope.Tools.alert(data.message);
                        }
                    })
                }  else {
                    $scope.Tools.alert('请上传凭证！');
                }

            }  else {
                $scope.Tools.alert('预计还车时间必须大于等于借车时间！');
            }

        }

    };

        $scope.getList();

    $scope.search ={};
    $scope.search.bcBorrowName = '';
    $scope.search.bcBorrowDepartment = '';
    $scope.search.borrowStartTime = '';
    $scope.search.borrowEndTime = '';

//    查询
    $scope.resetClick = function(){
        $scope.search.bcBorrowName = '';
        $scope.search.bcBorrowDepartment = '';
        $scope.search.borrowStartTime = '';
        $scope.search.borrowEndTime = '';
    };
//刷新
    $scope.refreshA = function(){
        $scope.pageSize = 10;
        $scope.resetClick();
        $scope.getList();
    };

    $scope.fristSelectT = true;
//    获取信息信息
    $scope.getDetail = function(e){
           var id =  angular.element(e.target).parent('tr').attr('bcid');
            borrowCarRegistrationService.findBorrowDetail(id).then(function(result){
                if(result.success){
                    $rootScope.bcDetail = result.data;
                } else {
                    $scope.Tools.alert(result.message);
                }
            })
    };


//    图片上传
    $scope.uploadImgs = function () {

    };
    // $rootScope.imgDetailURL = 'https://ss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=2995040420,4087761391&fm=96';
    // $rootScope.imgDetailURL ='';

    // 查看上传凭证
    $rootScope.getImageDatail = function(url){
        $rootScope.imgDetailURL = url;

        ngDialog.open({
            template: "templates/vehicleBorrowingManagement/layer/imgDetail.html",
            controller: "borrowCarRegistrationCtrl",
            title: '凭证详情',
            height:300
        });
        $timeout(function(){
            $('.ngdialog-content').css('display','table')
        },50)
    };

    $scope.db = function(e){
        var src = $(e.target).attr('src');
        $rootScope.showImage(src);
    };
}])
    .controller('borrowAndReturnRecordSearchCtrl',['$scope','$rootScope','$http','$timeout','ngDialog','borrowAndReturnRecordSearchService',function($scope,$rootScope,$http,$timeout,ngDialog,borrowAndReturnRecordSearchService){

        $scope.search ={};
        $scope.search.vin = '';
        $scope.search.isReturn = '';
        $scope.search.borrowName = '';
        $scope.search.returnName = '';
        $scope.search.returnStartTime = '';
        $scope.search.returnEndTime = '';

        $scope.fristSelect = true;
        //    获取列表
        $scope.getList = function(){
            var params = {
                pageNo:$scope.pageNo || 1,
                pageSize:$scope.pageSize || 10
            };
            params = angular.extend(params,$scope.search);
            borrowAndReturnRecordSearchService.borrowHistoryLine(params)
                .then(function(result){
                    if(result.success){
                        $scope.mainList = result.data.wmsBorrowCarList;
                        if($scope.fristSelect){
                            $scope.Tools.page(document.getElementById('pages'),{
                                pages : result.data.totalPage,
                                curr : result.data.pageNo,
                                count : result.data.totalRecord
                            },function(obj, first){
                                $scope.fristSelect = first;  //设置状态

                                if(!first){ //判断不是第一次加载
                                    var param = {};
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    $scope.pageSize = obj.limit; //设置最大页
                                    $scope.pageNo = obj.curr; //设置当前页
                                    $scope.getList(param);
                                }
                            })
                        }
                    }else{
                        $scope.Tools.alert(result.message);
                    }
                })
        };
        $scope.getList();

        //查询
        $scope.rcSearch  = function(){

        };

        //    重置
        $scope.resetClick = function(){
            $scope.search.vin = '';
            $scope.search.isReturn = '';
            $scope.search.borrowName = '';
            $scope.search.returnName = '';
            $scope.search.returnStartTime = '';
            $scope.search.returnEndTime = '';
        };
    //刷新
        $scope.refreshA = function(){
            $scope.pageSize = 10;
            $scope.resetClick();
            $scope.getList()
        };
    }])
    .controller('bcrPrintCtrl',['$scope','Tools','$rootScope','$http','$timeout','ngDialog','borrowCarRegistrationService',function($scope,Tools,$rootScope,$http,$timeout,ngDialog,borrowCarRegistrationService){
            $scope.close = function(){
                ngDialog.closeAll();
            };

            $scope.brcPrint = function(){
               Tools.printHTML($('.brcPrintMain')[0].outerHTML,790,1);
            };

        $scope.tableObj = '';
        function getTime()
        {
            var now = new Date();

            var year = now.getFullYear();       //年
            var month = now.getMonth() + 1;     //月
            var day = now.getDate();            //日

            var hh = now.getHours();            //时
            var mm = now.getMinutes();          //分

            var clock = year + "-";

            if(month < 10)
                clock += "0";

            clock += month + "-";

            if(day < 10)
                clock += "0";

            clock += day + " ";

            if(hh < 10)
                clock += "0";

            clock += hh + ":";
            if (mm < 10) clock += '0';
            clock += mm;
            return clock;
        }

        $scope.currTime = getTime();

        //        获取打印列表
        borrowCarRegistrationService.printInfo($rootScope.currPrintID).then(function(data){
            if(data.success){
                $scope.tableObj = data.data;
            }
        })

    }])
    .controller('bcr_image_detailCtrl',['$scope','$rootScope','$timeout',function($scope,$rootScope,$timeout){

    }])
    .controller('boardCarInventoryRegistrationCtrl',['$scope','$rootScope','$timeout','boardCarInventoryRegistrationService',function(scope,rootScope,$timeout,boardCarInventoryRegistrationService){
        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function(){
            scope.otherForm = !scope.otherForm;
            if(scope.otherForm){
                scope.moreText = scope.constant.moreText.packUp;
            }else{
                scope.moreText = scope.constant.moreText.spread;
            }
        };
        scope.openGateList = [
            {name:'自动tms调用成功开启',value:'10'},
            {name:'页面手动开启闸门',value:'20'},
            {name:'系统闸门开启失败',value:'30'}
        ];
        scope.exportSrc = "#";

        scope.formatGet = function(obj){
            var temp ='?';
            var tempObj = {
                userId:scope.userInfo.id,
                whCode:scope.userInfo.whCode
            };
            var selfObj = angular.extend(obj,tempObj);
            for(var k in selfObj){
                temp+=(k+'='+selfObj[k])+'&';
            }
            return temp;
        };

        scope.export = function(){
            // scope.Tools.dialog({
            //     controller : 'boardCarInventoryRegistrationCtrl.dialog',
            //     template : 'templates/inStorageManagement/boardCar-inventory-registration/layer/export.html',
            //     width : 190,
            //     title : '导出'
            // })
            scope.exportSrc = contextPath+'gate/inGateRecordImportExcel'+scope.formatGet(scope.search);
        };

        //enter  进场
        scope.openGate = function(str){
            scope.Tools.dialog({
                controller : 'boardCarInventoryRegistrationCtrl.dialog',
                template : 'templates/inStorageManagement/boardCar-inventory-registration/layer/openGate.html',
                width : 190,
                data: {
                    drId: str,
                    searchObj: scope.search
                },
                title : '进场原因'
            })
        };


        //创建搜索对象
        scope.search = {
            vehicle:'',
            departureType:'',
            startDeparturTime:'',
            endDepartureTime:''
        };

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function(params){
            //设置分页参数
            if(!params){
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

            scope.search.departureType?scope.search.departureType=scope.search.departureType.value:'';

            params = angular.extend(params,scope.search);

            boardCarInventoryRegistrationService.inboundGateList(params)
                .then(function(result){
                    if(result.success){
                        scope.inboundGateList = result.data;

                        if(scope.fristSelect){
                            scope.Tools.page(document.getElementById('pages'),{
                                pages : result.pageVo.totalPage,
                                curr : result.pageVo.pageNo,
                                count : result.pageVo.totalRecord
                            },function(obj, first){
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if(!first){ //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        }else{
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }
                    }else{
                        scope.Tools.alert(result.message);
                    }
                })
        };
        //初始化列表
        rootScope.birGetList = scope.list;
        scope.list();



        //查询 click
        scope.searchClick = function(){
            scope.fristSelect = true;   //设置为第一次查询
            scope.list();
        };

        //重置 click
        scope.resetClick = function(){
            scope.search = {};
            scope.list();
        };

        //刷新 click
        scope.refreshClick = function(){
            scope.searchClick();
        }

    }])
    .controller('boardCarInventoryRegistrationCtrl.dialog',['$scope','$rootScope','gateService','ngDialog',function(scope,rootScope,gateService,ngDialog){
        //    导出
        scope.reason = '';
        scope.enterFlag = false;
        scope.confirmEnter = function(){
            //入库类型查询 并打开闸门
            gateService.getByType(scope.constant.gateType.inbound)
                .then(function(result){
                    if(result.success){
                        gateService.open(result.data.channel,scope.constant.gateController.open,scope.ngDialogData.queryParam + '-收货质检成功，闸门已打开',scope.ngDialogData.queryParam + '-收货质检成功，闸门已打开',scope.ngDialogData.queryParam + '-收货质检成功，闸门已打开')
                            .success(function(data,status,headers,config){
                                scope.Tools.alert('闸门打开成功');
                            })
                            .error(function(data,status,headers,config){
                                scope.Tools.alert('闸门打开失败');
                            })
                    }else{
                        scope.Tools.alert(result.message);
                    }
                });

            // 调取后台
            boardCarInventoryRegistrationService.inboundGateOpen({drId:scope.ngDialogData.drId}).then(function(res){
                if(res.success){
                    // 成功
                    ngDialog.closeAll();
                    rootScope.birGetList();
                    setTimeout(function(){
                        scope.Tools.alert(res.message);
                    },100);
                } else{
                    // 失败
                    ngDialog.closeAll();
                    scope.Tools.alert(res.message);
                }
            });
            console.log('drId',scope.ngDialogData.drId)
        };
        scope.formatGet = function(obj){
                var temp ='?';
                for(var k in obj){
                    temp+=(k+'='+obj[k])+'&'
                }
                return temp;
        };

        scope.dgChange = function(){
            if(scope.reason){
                scope.enterFlag = true;
                scope.exportSrc = contextPath+'/gate/inGateRecordImportExcel'+scope.formatGet(scope.ngDialogData.search);
            } else {
                scope.enterFlag = false;
            }
        }
    }])
