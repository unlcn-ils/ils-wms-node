/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';

wmsCtrl.controller('transferDepotCtrl',['$scope','$rootScope','$timeout','$state','Tools','transferDepotService',function($scope,$rootScope,$timeout,$state,$Tools,transferDepotService){

    $scope.pt1 = {
        title: {
            x: 'center',
            text: '中转库 进、销、存 推移图',
            textStyle: {
                color: '#fff',
                fontSize: '30'
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                crossStyle: {
                    color: '#999',
                    fontSize: '30'
                }
            }
        },
//      toolbox: {    //右上角图片下载
//        feature: {
//          dataView: {show: true, readOnly: false},
//          magicType: {show: true, type: ['line', 'bar']},
//          restore: {show: true},
//          saveAsImage: {show: true}
//        }
//      },
        color: ['#5086d6', '#f67c2b', '#a5a5a5'],
        legend: {
            left: 'right',
            data: ['下单', '提车', '未提'],
            textStyle: {
                color: '#fff'
            }
        },
        xAxis: [
            {
                type: 'category',
                data: ['-', '-', '-', '-', '-'],
                axisPointer: {
                    type: 'shadow'
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#000',
                        opacity:0.3
                    }
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '数量',
                min: 0,
                max: 900,
                interval: 150,
                axisLabel: {
                    formatter: '{value} 台',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#464646'
                    }
                }
            }
        ],
        series: [
            {
                name: '下单',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                        }
                    }
                }
            },
            {
                name: '提车',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                        }
                    }
                }
            },
            {
                name: '未提',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                        }
                    }
                }
            }
        ]
    };

    $rootScope.td1_load = function(a){

            var boxObj = document.getElementById('bs_td1_box');
            var canvasBox = document.getElementById('bs_td1_canvasBox');

            var boxW = boxObj.offsetWidth;
            var boxH = boxObj.offsetHeight;

            canvasBox.style.width  = boxW+'px';
            canvasBox.style.height = boxH+'px';

            var box = echarts.init(canvasBox);
            box.setOption(a);
    };

    //获取报表表数据
    transferDepotService.invoicingStatisticsChartCount().then(function(res){
        if(res.success){
            $Tools.bigScreenGetYMax($scope.pt1,res.data,'','NoRight');
            var r_d = res.data;
            var pt_D = $scope.pt1.series;
            var temp = [];
            for(var i=0; i< r_d.length;i++){
                if(r_d[i].data == null){
                    var tp =[];
                    for(var j = 0; j < r_d[i].rate.length; j++){
                        r_d[i].rate[j] = parseFloat(r_d[i].rate[j]*100).toFixed(2);
                    }
                    pt_D[i].data = r_d[i].rate;
                } else {
                    pt_D[i].data = r_d[i].data
                }
                pt_D[i].name = r_d[i].name;                                                   // 加载柱状图
                temp.push(r_d[i].name);
            }
            $scope.pt1.legend.data = temp;                                                     //  加载条件注释
            $scope.pt1.xAxis[0].data = r_d[0].date;                                           //  加载x轴数据

            var update = '更新时间:'+ $rootScope.getNowFormatDate();
            $scope.pt1.title.subtext = update;

            $rootScope.td1_load($scope.pt1);
        }
    });

    //获取主列表
    transferDepotService.invoicingStatisticsLineCount().then(function(result){
        if(result.success){
            $scope.tableList = result.data;
        }
    });


    window.clearTimeout($rootScope.bs_timer1);
    window.clearTimeout($rootScope.bs_timer2);
    window.clearTimeout($rootScope.bs_timer3);
    window.clearTimeout($rootScope.bs_timer4);
    window.clearInterval($rootScope.bs_timer5);
    window.clearTimeout($rootScope.onwayIntertime);

    $rootScope.bs_timer2 = setTimeout(function(){
        $state.go('onWay');
    },$rootScope.bs_intervalTime*60000)

    }]);
