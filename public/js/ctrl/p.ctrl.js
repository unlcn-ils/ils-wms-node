/**
 * Created by lihao on 2017/11/07.
 */
'use strict';
wmsCtrl.controller('pickCarCtrl',['$scope','$rootScope','$timeout','Tools','$state','pickCarService',function($scope,$rootScope,$timeout,$Tools,$state,pickCarService){


    $scope.pt1 = {
        title: {
            x: 'center',
            text: '提车推移图',
            subtext:'',
            textStyle: {
                color: '#fff',
                fontSize: '30'
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                crossStyle: {
                    color: '#999',
                    fontSize: '30'
                }
            }
        },
        toolbox: {
            feature: {
                dataView: {show: true, readOnly: false},
                magicType: {show: true, type: ['line', 'bar']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        color: ['#5086d6', '#f67c2b', '#a5a5a5','#ffc000'],
        legend: {
            bottom: 10,
            left: 'center',
            data: ['下单', '提车', '未提', '提车及时率'],
            textStyle: {
                color: '#fff'
            }
        },
        xAxis: [
            {
                type: 'category',
                data: ['-', '-', '-', '-', '-'],
                axisPointer: {
                    type: 'shadow'
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#000',
                        opacity:0.3
                    }
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '数量',
                min: 0,
                max: 900,
                interval: 150,
                axisLabel: {
                    formatter: '{value} 台',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#464646'
                    }
                }
            },
            {
                type: 'value',
                name: '比率',
                min: 0,
                max: 120,
                interval: 20,
                axisLabel: {
                    formatter: '{value} %',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#464646'
                    }
                }
            }
        ],
        series: [
            {
                name: '下单',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '提车',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '未提',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '提车及时率',
                type: 'line',
                yAxisIndex: 1,
                data: [0, 0, 0, 0, 0],
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            formatter: '{c}%',
                            color:'#fff'
                        },
                        lineStyle: {
                            color: '#ffc000'
                        }
                    }
                }
            }
        ]
    };

    $scope.pt2 = {
        title: {
            x: 'center',
            text: '前端发运推移图',
            textStyle: {
                color: '#fff',
                fontSize: '30'
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                crossStyle: {
                    color: '#999',
                    fontSize: '30'
                }
            }
        },
        toolbox: {
            feature: {
                dataView: {show: true, readOnly: false},
                magicType: {show: true, type: ['line', 'bar']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        color: ['#5086d6', '#f67c2b', '#a5a5a5','#ffc000'],
        legend: {
            bottom: 10,
            left: 'center',
            data: ['入库', '发运', '库存', '发运及时率'],
            textStyle: {
                color: '#fff'
            }
        },
        xAxis: [
            {
                type: 'category',
                data: ['-', '-', '-', '-', '-'],
                axisPointer: {
                    // type: 'shadow'
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#000',
                        opacity:0.3
                    }
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '数量',
                min: 0,
                max: 900,
                interval: 150,
                axisLabel: {
                    formatter: '{value} 台',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#464646'
                    }
                }
            },
            {
                type: 'value',
                name: '比率',
                min: 0,
                max: 120,
                interval: 20,
                axisLabel: {
                    formatter: '{value} %',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#464646'
                    }
                }
            }
        ],
        series: [
            {
                name: '入库',
                type: 'bar',
                stack: 'transport',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '发运',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '库存',
                type: 'bar',
                data: [0, 0, 0, 0, 0],
                barGap: 0,
                itemStyle: {
                    normal: {
                        label: {
                            show: true
                        }
                    }
                }
            },
            {
                name: '发运及时率',
                type: 'line',
                yAxisIndex: 1,
                data: [0, 0, 0, 0, 0],
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            formatter: '{c}%',
                            color:'#fff'
                        },
                        lineStyle: {
                            color: '#ffc000'
                        }
                    }
                }
            }
        ]
    };

    $scope.pt3 = {
        title: {
            text: '超期原因分析',
            left: 'center',
            textStyle: {
                color: '#fff',
                fontSize: '30'
            }
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        color: ['#5086d6', '#f67c2b', '#a5a5a5'],
        legend: {
            // orient: 'vertical',
            // top: 'middle',
            bottom: 10,
            left: 'center',
            data: ['质损', '管控', '暂运'],
            textStyle: {
                color: '#fff'
            }
        },
        series: [
            {
                type: 'pie',
                radius: '65%',
                center: ['50%', '50%'],
                selectedMode: 'single',
                label: {
                    normal: {
                        show: true,
                        formatter: '{b}: ({d}%)'
                        // formatter: '{b}: {c}({d}%)'
                    }
                },
                data: [
                    {
                        value: 0,
                        name: '质损'
                        // label: {
                        //     normal: {
                        //         formatter: [
                        //             '{title|{b}}{abg|}',
                        //             '  {weatherHead|原因}{valueHead|件数}{rateHead|占比}',
                        //             '{hr|}',
                        //             '  {Sunny|}{value|202}{rate|55.3%}',
                        //             '  {Cloudy|}{value|142}{rate|38.9%}',
                        //             '  {Showers|}{value|21}{rate|5.8%}'
                        //         ].join('\n'),
                        //         backgroundColor: '#eee',
                        //         borderColor: '#777',
                        //         borderWidth: 1,
                        //         borderRadius: 4,
                        //         rich: {
                        //             title: {
                        //                 color: '#eee',
                        //                 align: 'center'
                        //             },
                        //             abg: {
                        //                 backgroundColor: '#333',
                        //                 width: '100%',
                        //                 align: 'right',
                        //                 height: 25,
                        //                 borderRadius: [4, 4, 0, 0]
                        //             },
                        //             Sunny: {
                        //                 height: 30,
                        //                 align: 'left',
                        //                 backgroundColor: {
                        //                     image: weatherIcons.Sunny
                        //                 }
                        //             },
                        //             Cloudy: {
                        //                 height: 30,
                        //                 align: 'left',
                        //                 backgroundColor: {
                        //                     image: weatherIcons.Cloudy
                        //                 }
                        //             },
                        //             Showers: {
                        //                 height: 30,
                        //                 align: 'left',
                        //                 backgroundColor: {
                        //                     image: weatherIcons.Showers
                        //                 }
                        //             },
                        //             weatherHead: {
                        //                 color: '#333',
                        //                 height: 24,
                        //                 align: 'left'
                        //             },
                        //             hr: {
                        //                 borderColor: '#777',
                        //                 width: '100%',
                        //                 borderWidth: 0.5,
                        //                 height: 0
                        //             },
                        //             value: {
                        //                 width: 20,
                        //                 padding: [0, 20, 0, 30],
                        //                 align: 'left'
                        //             },
                        //             valueHead: {
                        //                 color: '#333',
                        //                 width: 20,
                        //                 padding: [0, 20, 0, 30],
                        //                 align: 'center'
                        //             },
                        //             rate: {
                        //                 width: 40,
                        //                 align: 'right',
                        //                 padding: [0, 10, 0, 0]
                        //             },
                        //             rateHead: {
                        //                 color: '#333',
                        //                 width: 40,
                        //                 align: 'center',
                        //                 padding: [0, 10, 0, 0]
                        //             }
                        //         }
                        //     }
                        // }
                    },
                    {value: 0, name: '管控'},
                    {value: 0, name: '暂运'}
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };

    $scope.pt4 = {
        title: {
            text: '发运模式推移图',
            left: 'center',
            textStyle: {
                color: '#fff',
                fontSize: '30'
            }
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        color: ['#5086d6', '#f67c2b', '#a5a5a5','#a1dac0','#e78e72','#304553','#6db0b7'],
        legend: {
            orient: 'vertical',
            left: 'right',
            data: ['', '', '',''],
            textStyle: {
                color: '#fff'
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                data: ['-', '-', '-', '-', '-', '-', '-'],
                axisPointer: {
                    type: 'shadow'
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#000',
                        opacity:0.3
                    }
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '数量',
                min: 0,
                max: 1400,
                interval: 200,
                axisLabel: {
                    formatter: '{value} 台',
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine: {
                    lineStyle: {
                        color: '#464646'
                    }
                }
            }
        ],
        series: []
    };

    $rootScope.bs_load_1 = function(a,b,c,d){
        var bWidth = document.getElementById('bs_pc_tb1').offsetWidth;
        var bHeight = document.getElementById('bs_pc_tb1').offsetHeight;

        var $_bWidth  = $('#bs_pc_tb1').width();
        var $_bHeight = $('#bs_pc_tb1').height();

        var canvasBox = document.getElementsByClassName('bs_pc_canvasBox');
        for (var i = 0; i < canvasBox.length; i++) {

            canvasBox[i].style.width = $_bWidth + 'px';
            canvasBox[i].style.height = $_bHeight + 'px';
        }


        var partArr = document.getElementsByClassName('bs_pc_part');

        for (var i = 0; i < partArr.length; i++) {
            partArr[i].style.width = $_bWidth + 'px';
        }


        var tb1 = echarts.init(document.getElementById('bs_pc_clTB1'));
        var tb2 = echarts.init(document.getElementById('bs_pc_clTB2'));
        var tb3 = echarts.init(document.getElementById('bs_pc_clTB3'));
        var tb4 = echarts.init(document.getElementById('bs_pc_clTB4'));

        var option1 = a;

        var option2 = b;

        var option3 = c;

        var option4 = d;

        a==null?'':tb1.setOption(option1);
        b==null?'':tb2.setOption(option2);
        c==null?'':tb3.setOption(option3);
        d==null?'':tb4.setOption(option4);

    };

    $scope.getData = function() {
        //提车推移图
        pickCarService.pickDataCount().then(function (res) {
            if (res.success && res.data != null) {
                $Tools.bigScreenGetYMax($scope.pt1,res.data);
                var r_d = res.data;
                var pt_D = $scope.pt1.series;
                var temp = [];
                for (var i = 0; i < r_d.length; i++) {
                    if (r_d[i].data == null) {
                        var tp = [];
                        for (var j = 0; j < r_d[i].rate.length; j++) {
                            if (r_d[i].rate[j]) {
                                r_d[i].rate[j] = parseFloat(r_d[i].rate[j]*100).toFixed(2);
                            } else {
                                r_d[i].rate[j] = 0;
                            }
                        }
                        pt_D[i].data = r_d[i].rate;
                    } else {
                        pt_D[i].data = r_d[i].data
                    }
                    pt_D[i].name = r_d[i].name;                                                   // 加载柱状图
                    temp.push(r_d[i].name);
                }
                $scope.pt1.legend.data = temp;                                                     //  加载条件注释
                $scope.pt1.xAxis[0].data = r_d[0].date;                                           //  加载x轴数据

                var update = '更新时间: ' + $rootScope.getNowFormatDate();
                $scope.pt1.title.subtext = update;
                $rootScope.bs_load_1($scope.pt1, null, null, null);
            }
        });

        //前端发运推移图
        pickCarService.shipmentDataCount().then(function (res) {
            if (res.success && res.data != null) {
                $Tools.bigScreenGetYMax($scope.pt2,res.data);
                var r_d = res.data;
                var pt_D = $scope.pt2.series;
                var temp = [];
                for (var i = 0; i < r_d.length; i++) {
                    if (r_d[i].data == null) {
                        var tp = [];
                        for (var j = 0; j < r_d[i].rate.length; j++) {
                            if (r_d[i].rate[j]) {
                                r_d[i].rate[j] = parseFloat(r_d[i].rate[j]* 100).toFixed(2);
                            } else {
                                r_d[i].rate[j] = 0;
                            }
                        }
                        pt_D[i].data = r_d[i].rate;
                    } else {
                        pt_D[i].data = r_d[i].data
                    }
                    pt_D[i].name = r_d[i].name;                                                   // 加载柱状图
                    temp.push(r_d[i].name);
                }
                $scope.pt2.legend.data = temp;                                                     //  加载条件注释
                $scope.pt2.xAxis[0].data = r_d[0].date;                                           //  加载x轴数据

                var update = '更新时间: ' + $rootScope.getNowFormatDate();
                $scope.pt2.title.subtext = update;
                $rootScope.bs_load_1(null, $scope.pt2, null, null);
            }
        });

        //超期质量分析
        pickCarService.reasonInfo().then(function (data) {
            if (data.success) {

                $scope.pt3.series[0].data = data.data;

                var update = '更新时间: ' + $rootScope.getNowFormatDate();
                $scope.pt3.title.subtext = update;
                $rootScope.bs_load_1(null, null, $scope.pt3, null);

            }
        });

        // 发运模式推移图
        pickCarService.transModelCount().then(function (res) {
            if (res.success && res.data != null) {
                $Tools.bigScreenGetYMax($scope.pt4,'',$Tools.arrTotalMax(res.data), 'NoRight');
                var r_d = res.data;
                var pt_D = $scope.pt4.series;
                $scope.pt4.series = [];
                var temp = [];
                for (var i = 0; i < r_d.length; i++) {
                    var obj = {
                            name: '',
                            type: 'bar',
                            stack: 'transport',
                            barWidth: '60',
                            data: [0, 0, 0, 0, 0, 0, 0],
                            itemStyle: {
                                normal: {
                                    label: {
                                        show: true
                                    }
                                }
                            }
                        };
                    obj.data = r_d[i].data;
                    obj.name = r_d[i].name;                                                   // 加载柱状图
                    // pt_D.push(obj);
                    $scope.pt4.series.push(obj)
                    temp.push(r_d[i].name);
                }
                $scope.pt4.legend.data = temp;                                                     //  加载条件注释
                $scope.pt4.xAxis[0].data = r_d[0].date;                                           //  加载x轴数据

                var update = '更新时间: ' + $rootScope.getNowFormatDate();
                $scope.pt4.title.subtext = update;
                $rootScope.bs_load_1(null, null, null, $scope.pt4);
            }
        });
    };
    window.clearTimeout($rootScope.bs_timer1);
    window.clearTimeout($rootScope.bs_timer2);
    window.clearTimeout($rootScope.bs_timer3);
    window.clearTimeout($rootScope.bs_timer4);
    clearInterval($rootScope.bs_timer5);
    window.clearTimeout($rootScope.onwayIntertime);

    $scope.getData();
    console.log(parseInt($rootScope.bs_intervalTime));
    $rootScope.bs_timer5 = setInterval(function(){
        console.log('变化了')
        $scope.getData();
    },600000);
    // },1000);
}]);