/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
wmsCtrl.controller('warehouseCtrl', ['$scope', 'warehouseService', 'dropDownService', function (scope, warehouseService, dropDownService) { //出库作业管理

    //更多条件默认值
    scope.moreText = scope.constant.moreText.spread;
    //更多条件
    scope.moreClick = function () {
        scope.otherForm = !scope.otherForm;
        if (scope.otherForm) {
            scope.moreText = scope.constant.moreText.packUp;
        } else {
            scope.moreText = scope.constant.moreText.spread;
        }
    }

    /*状态数据*/
    scope.dropDownList = {
        status: scope.Tools.dropDown.warehouseDropDown.concat(),    //出库状态
        quitTask: scope.Tools.dropDown.quitTaskDropDown.concat()   //是否退库
    }

    scope.dropDownList.status.unshift(scope.constant.defaultDropDownNullData); //插入出库状态数据
    scope.dropDownList.quitTask.unshift(scope.constant.defaultDropDownNullData); //插入出库状态数据

    //设置默认值
    scope.dropDownDefault = {
        status: scope.dropDownList.status[0], //出库状态
        quitTask: scope.dropDownList.quitTask[0]   //是否退库
    }

    //获取车型
    dropDownService.getVehicleSpec()
        .then(function (result) {
            if (result.success) {
                scope.dropDownList.vehicleSpec = result.data;
                scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
                scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
            } else {
                scope.Tools.alert(result.message);
            }
        })
    //获取装车道
    warehouseService.getEstimateLaneByWhCode(scope.userInfo.whCode)
        .then(function (otEstimateLaneResult) {
            if (otEstimateLaneResult.success) {
                scope.dropDownLists = {
                    otEstimateLane: otEstimateLaneResult.data
                }
                scope.dropDownLists.otEstimateLane.unshift(scope.constant.defaultDropDownNullData);  //向前插入空值
                //设置默认值
                scope.dropDownDefaults = {
                    otEstimateLane: scope.dropDownLists.otEstimateLane[0]   //计划装车道默认值
                };
            } else {
                scope.Tools.alert(otEstimateLaneResult.message);
            }
        })

    //获取司机
    dropDownService.getUserByWarehouse()
        .then(function (result) {
            if (result.success) {
                scope.dropDownList.driver = result.data;
                scope.dropDownList.driver.unshift(scope.constant.defaultDropDownNullData); //插入空值
                scope.dropDownDefault.driver = scope.dropDownList.driver[0];  //设置默认值
            } else {
                scope.Tools.alert(result.message);
            }
        })

    //列表查询接口参数构造
    scope.listParam = function () {

        if (!scope.search) {
            scope.search = {};
        }
        scope.search.pageSize = scope.pageSize;    //设置最大页
        if (!scope.fristSelect) { //不是第一次查询 则设置当前页
            scope.search.pageNo = scope.pageNo;
        } else {  //第一次查询设置为　第一页
            scope.search.pageNo = scope.constant.pageNo;
        }

        if (scope.otherForm) {    //展开了更多
            scope.search.otStatus = scope.dropDownDefault.status.value;    //出库状态
            scope.search.otVehicleSpecName = scope.dropDownDefault.vehicleSpec.wndVehicleName;   //车型
            scope.search.otEstimateLane = scope.dropDownDefaults.otEstimateLane.elName; //装车道
            scope.search.otDriver = scope.dropDownDefault.driver ? scope.dropDownDefault.driver.id : '';    //备料司机id
            scope.search.otQuitFlag = scope.dropDownDefault.quitTask.value; //是否退库
            return scope.search;
        } else {  //没有展开
            return {
                otVin: scope.search.otVin, //车架号
                pageSize: scope.search.pageSize,   //最大号
                pageNo: scope.search.pageNo    //当前页
            }
        }
    }

    //列表
    scope.fristSelect = true;   //判断是否是第一次查询
    scope.list = function (params) {
        //设置分页参数
        if (!params) {
            params = {};
        }
        params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
        params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

        warehouseService.list(params)
            .then(function (result) {
                if (result.success) {
                    scope.warehouseList = result.data.dataList;
                    if (scope.fristSelect) {
                        scope.Tools.page(document.getElementById('pages'), {
                            pages: result.data.page.totalPage,
                            curr: result.data.page.pageNo,
                            count: result.data.page.totalRecord
                        }, function (obj, first) {
                            scope.fristSelect = first;  //设置状态
                            //初始化多选框
                            scope.Tools.initCheckBox();
                            if (!first) { //判断不是第一次加载
                                var param = scope.listParam();
                                param.pageNo = obj.curr;    //当前页
                                param.pageSize = obj.limit; //最大页
                                scope.pageSize = obj.limit; //设置最大页
                                scope.pageNo = obj.curr; //设置当前页
                                scope.list(param);
                            }
                        })
                    } else {
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    }

                } else {
                    scope.Tools.alert(result.message);
                }
            })
    }

    //列表
    scope.list();

    //回车按下查询
    scope.searchListKeyDown = function (e) {
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {  //回车键
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
            e.preventDefault();
        }
    }

    //查询 click
    scope.searchClick = function () {
        scope.fristSelect = true;   //设置为第一次查询
        scope.list(scope.listParam());
    }

    //重置 click
    scope.resetClick = function () {
        scope.search = {};
        scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
        scope.dropDownDefault.status = scope.dropDownList.status[0];
        scope.dropDownDefaults.otEstimateLane = scope.dropDownLists.otEstimateLane[0];
    }

    //刷新 click
    scope.refreshClick = function () {
        scope.searchClick();
    }

    //备料出库 click
    scope.outboundWarehouseClick = function () {
        var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

        if (indexArr.length > 1) {
            scope.Tools.alert('只能选择一条数据');
            return;
        }

        scope.Tools.dialog({
            controller: 'warehouseCtrl.dialog',
            template: 'templates/warehouseManagement/warehouse/outbound-warehouse-form.html',
            closeByDocument: false,
            closeByEscape: false,
            data: {
                vin: indexArr.length != 0 ? scope.warehouseList[indexArr[0]].otVin : '',  //vin号
                outboundWarehouse: indexArr.length != 0 ? scope.warehouseList[indexArr[0]] : null
            },
            width: 550,
            title: '备料出库',
            scope: scope
        })
    }

    //备料确认
    scope.outboundConfirmClick = function () {
        var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

        if (indexArr.length > 1) {
            scope.Tools.alert('只能选择一条数据');
            return;
        }

        if (indexArr.length > 0) {
            var materialNo = scope.warehouseList[indexArr[0]].otPreparationMaterialNo;    //备料单号

            warehouseService.getPreparateInfoByMaterialNo(materialNo)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.dialog({
                            controller: 'warehouseCtrl.dialog',
                            template: 'templates/warehouseManagement/warehouse/outbound-confirm-form.html',
                            closeByDocument: false,
                            closeByEscape: false,
                            data: {
                                otPreparationMaterialNo: materialNo,  //备料单号
                                outboundConfirmList: result.data,
                                warehouse: scope.warehouseList[indexArr[0]]    //出库数据
                            },
                            width: 550,
                            title: '备料出库',
                            scope: scope
                        })
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        } else {
            scope.Tools.dialog({
                controller: 'warehouseCtrl.dialog',
                template: 'templates/warehouseManagement/warehouse/outbound-confirm-form.html',
                closeByDocument: false,
                closeByEscape: false,
                data: {
                    otPreparationMaterialNo: '',  //备料单号
                    outboundConfirmList: null
                },
                width: 550,
                title: '备料出库',
                scope: scope
            })
        }
    }

    //出库确认
    scope.outConfirmClick = function () {
        scope.Tools.dialog({
            controller: 'warehouseCtrl.dialog',
            template: 'templates/warehouseManagement/warehouse/out-confirm-form.html',
            closeByDocument: false,
            closeByEscape: false,
            data: {},
            width: 700,
            title: '出库确认',
            scope: scope
        })
    }

    //任务取消
    scope.cancelTaskClick = function () {
        var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

        if (indexArr.length == 0) {
            scope.Tools.alert('请选择一条数据');
            return;
        }

        if (indexArr.length > 1) {
            scope.Tools.alert('只能选择一条数据');
            return;
        }

        if (scope.warehouseList[indexArr[0]].otStatus == scope.constant.warehouse.finish || scope.warehouseList[indexArr[0]].otStatus == scope.constant.warehouse.cancel) {
            scope.Tools.alert('只能选择未完成前的数据');
            return;
        }

        scope.Tools.dialog({
            controller: 'warehouseCtrl.dialog',
            template: 'templates/warehouseManagement/warehouse/cancel-task-form.html',
            closeByDocument: false,
            closeByEscape: false,
            data: {
                warehouse: scope.warehouseList[indexArr[0]]    //选择的 出库数据
            },
            width: 500,
            title: '任务取消',
            scope: scope
        })
    }

    //备料退库  click
    scope.quitTaskClick = function () {
        var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

        if (indexArr.length == 0) {
            scope.Tools.alert('请选择一条数据');
            return;
        }

        if (indexArr.length > 1) {
            scope.Tools.alert('只能选择一条数据');
            return;
        }

        if (scope.warehouseList[indexArr[0]].otStatus != scope.constant.warehouse.finish) {
            scope.Tools.alert('只能选择已完成的数据');
            return;
        }

        scope.Tools.dialog({
            controller: 'warehouseCtrl.dialog',
            template: 'templates/warehouseManagement/warehouse/quit-task-form.html',
            closeByDocument: false,
            closeByEscape: false,
            data: {
                warehouse: scope.warehouseList[indexArr[0]]    //选择的 出库数据
            },
            width: 500,
            title: '备料退库',
            scope: scope
        })
    }
}])
    .controller('warehouseCtrl.dialog', ['$scope', 'warehouseService', 'gateService', 'junmaGateService', function (scope, warehouseService, gateService, junmaGateService) {   //出库作业管理 dialog

        //根据vin查询
        scope.searchByVinKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                if (scope.ngDialogData.vin) {
                    warehouseService.getPreparateInfoByVin(scope.ngDialogData.vin)
                        .then(function (result) {
                            if (result.success) {
                                scope.ngDialogData.outboundWarehouse = result.data;

                                //入库类型查询 并打开闸门  库存区---出库
                                var params = {
                                    gateType: 10,
                                    inOutType: 20
                                };
                                gateService.getByType(params)
                                    .then(function (result) {
                                        if (result.success) {
                                            gateService.open(result.data.channel, scope.constant.gateController.open, scope.ngDialogData.vin + '-备料出库成功，闸门已打开', scope.ngDialogData.vin + '-备料出库成功，闸门已打开', scope.ngDialogData.vin + '-备料出库成功，闸门已打开')
                                                .success(function (data, status, headers, config) {
                                                    scope.Tools.alert('闸门打开成功');
                                                })
                                                .error(function (data, status, headers, config) {
                                                    scope.Tools.alert('闸门打开失败');
                                                })
                                            if (scope.ngDialogData.outboundWarehouse) {
                                                warehouseService.updateTaskToOutbound({otVin: scope.ngDialogData.outboundWarehouse.otVin})
                                                    .then(function (result) {
                                                        if (result.success) {
                                                            scope.Tools.alert('出库确认成功');
                                                            scope.list(scope.listParam());
                                                            scope.closeThisDialog();
                                                        } else {
                                                            scope.Tools.alert(result.message);
                                                        }
                                                    })
                                            }
                                        } else {
                                            scope.Tools.alert(result.message);
                                        }
                                    })
                            } else {
                                scope.Tools.alert(result.message);
                            }
                        })
                } else {
                    scope.ngDialogData.outboundWarehouse = null;
                }
            }
        }

        //根据备料单号查询
        scope.searchByMaterialNoKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                if (scope.ngDialogData.otPreparationMaterialNo) {
                    warehouseService.getPreparateInfoByMaterialNo(scope.ngDialogData.otPreparationMaterialNo)
                        .then(function (result) {
                            if (result.success) {
                                scope.ngDialogData.outboundConfirmList = result.data;
                            } else {
                                scope.Tools.alert(result.message);
                            }
                        })
                } else {
                    scope.ngDialogData.outboundConfirmList = null;
                }
            }
        }

        //出库确认 click
        scope.warehouseConfirmClick = function () {
            if (scope.ngDialogData.outboundWarehouse) {
                warehouseService.updateTaskToOutbound({otVin: scope.ngDialogData.outboundWarehouse.otVin})
                    .then(function (result) {
                        if (result.success) {
                            scope.Tools.alert('出库确认成功');
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }

        //备料计划完成 click
        scope.outboundFinishClick = function () {
            if (scope.ngDialogData.outboundConfirmList) {
                warehouseService.updateTaskToConfirm({otPreparationMaterialNo: scope.ngDialogData.otPreparationMaterialNo})
                    .then(function (result) {
                        if (result.success) {
                            scope.Tools.alert('备料完成确认成功');
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }


        //出库确认 获取 vin 信息
        scope.searchVINListKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                if (scope.orderNo) {
                    warehouseService.getVinList(scope.orderNo)
                        .then(function (result) {
                            if (result.success) {
                                scope.vinList = result.data;
                            } else {
                                scope.Tools.alert(result.message);
                            }
                        })
                }
            }
        }

        //出库确认 根据vin确认数据
        scope.searchVINKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                if (scope.vinList) {
                    // var obj = {
                    //     barCode : scope.orderNo,
                    //     vin : scope.vin,
                    //     vinList : scope.vinList
                    // };
                    if (scope.vinList.indexOf(scope.vin) !== -1) {
                        // 开启闸门
                        var params = {
                            CardNo: scope.vin,
                            MacNo: 1
                        };

                        junmaGateService.open(params)
                            .success(function (data, status, headers, config) {
                                scope.Tools.alert('闸门打开成功');
                            })
                            .error(function (data, status, headers, config) {
                                scope.Tools.alert('闸门打开失败');
                            })
                    } else {
                        scope.Tools.alert('此车不属于该交接单，不允许出库！');
                    }

                    // warehouseService.outConfirm(obj)
                    //     .then(function(result){
                    //         if(result.success){
                    //             scope.Tools.alert('出库确认成功');
                    //             scope.vinList = result.data;
                    //             scope.confirmVinList = new Array(scope.vin);
                    //             scope.list(scope.listParam());
                    //
                    //             // if(result.data.indexOf(scope.orderNo)!==-1){
                    //             //     // 开启闸门
                    //             //     var params = {
                    //             //         CardNo:  scope.orderNo,
                    //             //         MacNo: 1
                    //             //     };
                    //             //
                    //             //     junmaGateService.open(params)
                    //             //         .success(function(data,status,headers,config){
                    //             //             scope.Tools.alert('闸门打开成功');
                    //             //         })
                    //             //         .error(function(data,status,headers,config){
                    //             //             scope.Tools.alert('闸门打开失败');
                    //             //         })
                    //             // } else {
                    //             //     scope.Tools.alert('此车不属于该交接单，不允许出库！');
                    //             // }
                    //
                    //
                    //         }else{
                    //             scope.Tools.alert(result.message);
                    //         }
                    //     })
                } else {
                    scope.Tools.alert('请根据单据号获取vin信息');
                }
            }
        }

        //任务取消 click
        scope.saveCancelTaskClick = function () {
            var obj = {
                otId: scope.ngDialogData.warehouse.otId,    //id
                cancleReason: scope.ngDialogData.cancleReason  //取消原因
            }
            warehouseService.cancleTaskForCq(obj)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.alert('任务取消成功');
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //备料退库 click
        scope.saveQuitTaskClick = function () {
            var obj = {
                otId: scope.ngDialogData.warehouse.otId,    //id
                cancleReason: scope.ngDialogData.cancleReason  //退库原因
            }
            warehouseService.quitTaskForCq(obj)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.alert('备料退库成功');
                        scope.list(scope.listParam());
                        scope.closeThisDialog();
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }])
    .controller('warehouseMaintenanceCtrl', ['$scope', 'warehouseMaintenanceService', function (scope, warehouseMaintenanceService) { //仓库维护

        //新增仓库维护
        scope.addWarehouseMaintenanceClick = function () {
            scope.Tools.dialog({
                controller: 'warehouseMaintenanceCtrl.dialog',
                template: 'templates/locationManagement/warehouseMaintenance/warehouse-maintenance-from.html',
                closeByDocument: false,
                closeByEscape: false,
                data: {
                    warehouseType: scope.Tools.dropDown.warehouseTypeDropDown, //仓库类型
                    warehouseAttr: scope.Tools.dropDown.warehouseAttrDropDown, //仓库属性
                    warehouseAuto: scope.Tools.dropDown.warehouseAutoDropDown,  //仓库库位自动分配
                    dropDownDefault: {
                        warehouseType: scope.Tools.dropDown.warehouseTypeDropDown[0],   //仓库类型默认值
                        warehouseAttr: scope.Tools.dropDown.warehouseAttrDropDown[0],   //仓库属性默认值
                        warehouseAuto: scope.Tools.dropDown.warehouseAutoDropDown[0]   //仓库库位自动分配默认值
                    }
                },
                width: 550,
                title: '新增仓库维护',
                scope: scope
            })
        }

        //更新仓库维护
        scope.updateWarehouseMaintenanceClick = function () {

            var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

            if (indexArr.length == 0) {
                scope.Tools.alert('请选择需要更新的数据');
                return;
            }

            if (indexArr.length > 1) {
                scope.Tools.alert('只能选择一条数据进行更新');
                return;
            }

            var warehouse = angular.copy(scope.warehouseMaintenanceList[indexArr[0]]);
            scope.Tools.dialog({
                controller: 'warehouseMaintenanceCtrl.dialog',
                template: 'templates/locationManagement/warehouseMaintenance/warehouse-maintenance-from.html',
                closeByDocument: false,
                closeByEscape: false,
                data: {
                    warehouse: warehouse,
                    warehouseType: scope.Tools.dropDown.warehouseTypeDropDown, //仓库类型
                    warehouseAttr: scope.Tools.dropDown.warehouseAttrDropDown, //仓库属性
                    warehouseAuto: scope.Tools.dropDown.warehouseAutoDropDown,  //仓库库位自动分配
                    dropDownDefault: {
                        warehouseType: scope.Tools.getWarehouseTypeDropDownByKey(warehouse.whType),   //仓库类型默认值
                        warehouseAttr: scope.Tools.getWarehouseAttrDropDownByKey(warehouse.whStyle),   //仓库属性默认值
                        warehouseAuto: scope.Tools.getWarehouseAutoDropDownByKey(warehouse.whIsAuto)   //仓库自动分配默认值
                    }
                },
                width: 550,
                title: '更新仓库维护',
                scope: scope
            })
        }

        //删除仓库维护
        scope.deleteWarehouseMaintenanceClick = function () {
            var indexArr = scope.Tools.getTableSelectItemIndex('.warehouseTable');  //获取table所选的下标

            var warehouseList = scope.Tools.getDataTOIndex(indexArr, scope.warehouseMaintenanceList);

            if (warehouseList.length == 0) {
                scope.Tools.alert('请选择需要删除的数据');
                return;
            }

            scope.Tools.dialogConfirm('确定要删除选中的数据吗？')
                .then(function (res) {
                    if (res) {
                        warehouseMaintenanceService.deleteWarehouse(warehouseList)
                            .then(function (result) {
                                if (result.success) {
                                    scope.list(scope.listParam());
                                    scope.Tools.alert('删除数据成功');
                                } else {
                                    scope.Tools.alert(result.message);
                                }
                            })
                    }
                })
        }

        //列表条件参数
        scope.listParam = function () {
            if (!scope.search) {
                scope.search = {};
            }
            scope.search.pageSize = scope.pageSize;    //设置最大页
            if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            } else {  //第一次查询设置为　第一页
                scope.search.pageNo = scope.constant.pageNo;
            }

            if (scope.otherForm) {    //展开了更多
                return scope.search;
            } else {
                return scope.search;
            }
        }

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

            warehouseMaintenanceService.list(params)
                .then(function (result) {
                    if (result.success) {
                        scope.warehouseMaintenanceList = result.data.warehouseList;
                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.data.wmsWarehouseQueryBO.totalPage,
                                curr: result.data.wmsWarehouseQueryBO.pageNo,
                                count: result.data.wmsWarehouseQueryBO.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if (!first) { //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //列表
        scope.list();

        //回车按下查询
        scope.searchListKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                scope.fristSelect = true;   //设置为第一次查询
                scope.list(scope.listParam());
                e.preventDefault();
            }
        }

        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        }

        //刷新 click
        scope.refreshClick = function () {
            scope.searchClick();
        }

        //装车道查询
        scope.estimateLane = function (whCode) {
            warehouseMaintenanceService.getEstimateLaneByWhCode(whCode)
                .then(function (result) {
                    if (result.success) {
                        scope.estimateLaneList = result.data;
                        //初始化多选框
                        scope.Tools.initCheckBox();
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }

        //选择item click
        scope.selectItemClick = function (item, e) {
            scope.Tools.initTableClick(e);  //选中行处理
            scope.selectWarehouse = item;    //选中的仓库数据
            scope.estimateLane(item.whCode);
        }

        //新增装车道
        scope.addEstimateLaneClick = function () {

            if (!scope.selectWarehouse) {
                scope.Tools.alert('请选择一行仓库进行操作');
                return;
            }

            scope.Tools.dialog({
                controller: 'warehouseMaintenanceCtrl.dialog',
                template: 'templates/locationManagement/warehouseMaintenance/estimate-lane-form.html',
                closeByDocument: false,
                closeByEscape: false,
                data: {
                    dropDownDefault: { //默认值
                    },
                    warehouse: scope.selectWarehouse   //选择的仓库
                },
                width: 400,
                title: '新增装车道',
                scope: scope
            })
        }

        //删除装车道
        scope.deleteEstimateLaneClick = function () {
            var indexArr = scope.Tools.getTableSelectItemIndex('.estimateLaneTable');  //获取table所选的下标

            if (indexArr.length == 0) {
                scope.Tools.alert('请选择装车道数据');
                return;
            }
            var estimateLaneList = scope.Tools.getDataTOIndex(indexArr, scope.estimateLaneList);

            scope.Tools.dialogConfirm('确定要删除选中的数据吗？')
                .then(function (res) {
                    if (res) {
                        warehouseMaintenanceService.deleteEstimateLane(estimateLaneList)
                            .then(function (result) {
                                if (result.success) {
                                    scope.estimateLane(scope.selectWarehouse.whName);
                                    scope.Tools.alert('删除装车道成功');
                                } else {
                                    scope.Tools.alert(result.message);
                                }
                            })
                    }
                })
        }
    }])
    .controller('warehouseMaintenanceCtrl.dialog', ['$scope', 'warehouseMaintenanceService', function (scope, warehouseMaintenanceService) {  //仓库维护dialog

        //新增或修改 仓库
        scope.saveOrUpdateWarehouseClick = function () {

            scope.ngDialogData.warehouse.whType = scope.ngDialogData.dropDownDefault.warehouseType.value;   //仓库类型
            scope.ngDialogData.warehouse.whStyle = scope.ngDialogData.dropDownDefault.warehouseAttr.value;   //仓库属性方式
            scope.ngDialogData.warehouse.whIsAuto = scope.ngDialogData.dropDownDefault.warehouseAuto.value;   //仓库自动分配

            if (scope.ngDialogData.warehouse.whId) {   //修改
                //更新仓库
                warehouseMaintenanceService.updateWarehouse(scope.ngDialogData.warehouse)
                    .then(function (result) {
                        if (result.success) {
                            scope.Tools.alert('更新入库单成功');
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            } else {  //新增
                //新增仓库
                warehouseMaintenanceService.addWarehouse(scope.ngDialogData.warehouse)
                    .then(function (result) {
                        if (result.success) {
                            scope.Tools.alert('新增仓库成功');
                            scope.$parent.fristSelect = true;   //设置为第一次查询
                            scope.list(scope.listParam());
                            scope.closeThisDialog();
                        } else {
                            scope.Tools.alert(result.message);
                        }
                    })
            }
        }

        //新增装车道 click
        scope.saveEstimateLaneClick = function () {

            scope.ngDialogData.estimateLane.elWhCode = scope.ngDialogData.warehouse.whCode; //仓库code
            scope.ngDialogData.estimateLane.elWhName = scope.ngDialogData.warehouse.whName; //仓库name

            warehouseMaintenanceService.addEstimateLane(scope.ngDialogData.estimateLane)
                .then(function (result) {
                    if (result.success) {
                        scope.Tools.alert('新增装车道成功');
                        scope.estimateLane(scope.ngDialogData.warehouse.whName);
                        scope.closeThisDialog();
                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        }
    }])
    .controller('warehouseRecordCtrl', ['$scope', 'warehouseService', 'dropDownService', 'ngDialog', function (scope, warehouseService, dropDownService, ngDialog) {    //出库记录查询 ctrl
        //导出
        scope.export = function () {
            ngDialog.open({
                template: '/templates/warehouseManagement/warehouseRecord/export.html',
                controller: 'warehouseRecordExportCtrl.dialog',
                title: '出库记录导出',
                width: 590
            })
        };
        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function () {
            scope.otherForm = !scope.otherForm;
            if (scope.otherForm) {
                scope.moreText = scope.constant.moreText.packUp;
            } else {
                scope.moreText = scope.constant.moreText.spread;
            }
        };
        scope.search = {};
        //列表查询接口参数构造
        scope.listParam = function () {

            if (!scope.search) {
                scope.search = {};
            }
            scope.search.pageSize = scope.pageSize;    //设置最大页
            if (!scope.fristSelect) { //不是第一次查询 则设置当前页
                scope.search.pageNo = scope.pageNo;
            } else {  //第一次查询设置为　第一页
                scope.search.pageNo = scope.constant.pageNo;
            }

            if (scope.otherForm) {    //展开了更多
                scope.search.otVehicleSpecName = scope.dropDownDefault.vehicleSpec.wndVehicleName;   //车型
                scope.search.otOutboundFlag = scope.dropDownDefault.shipmentStatus.value;   //发运状态
                return scope.search;
            } else {  //没有展开
                return {
                    otVin: scope.search.otVin, //车架号
                    pageSize: scope.search.pageSize,   //最大号
                    pageNo: scope.search.pageNo    //当前页
                }
            }
        }

        //下拉对象
        scope.dropDownList = {
            shipmentStatus: scope.Tools.dropDown.shipmentPlanStatusDropDown.concat()
        };

        scope.dropDownList.shipmentStatus.unshift(scope.constant.defaultDropDownNullData);  //插入空值
        //下拉默认值
        scope.dropDownDefault = {
            shipmentStatus: scope.dropDownList.shipmentStatus[0]
        };

        //获取车型
        dropDownService.getVehicleSpec()
            .then(function (result) {
                if (result.success) {
                    scope.dropDownList.vehicleSpec = result.data;
                    scope.dropDownList.vehicleSpec.unshift(scope.constant.defaultDropDownNullData); //插入空值
                    scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];  //设置默认值
                } else {
                    scope.Tools.alert(result.message);
                }
            })

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;
            warehouseService.getOutboundRecordList(params)
                .then(function (result) {
                    if (result.success) {
                        scope.inventoryRecordsList = result.data.dataList;
                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.data.page.totalPage,
                                curr: result.data.page.pageNo,
                                count: result.data.page.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                scope.Tools.initCheckBox();
                                if (!first) { //判断不是第一次加载
                                    var param = scope.listParam();
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            scope.Tools.initCheckBox();
                        }

                    } else {
                        scope.Tools.alert(result.message);
                    }
                })
        };

        //列表
        scope.list();

        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list(scope.listParam());
        };

        //重置 click
        scope.resetClick = function () {
            scope.search = {};
            scope.dropDownDefault.vehicleSpec = scope.dropDownList.vehicleSpec[0];
        };

        //刷新 click
        scope.refreshClick = function () {
            scope.searchClick();
        }
    }])
    .controller('warehouseRecordCtrl.dialog', ['$scope', function (scope) {    //出库记录查询 ctrl dialog
    }])

    .controller('warehouseRecordExportCtrl.dialog', ['$scope', 'warehouseRecordExportService', '$timeout', 'ngDialog', function (scope, warehouseRecordExportService, $timeout, ngDialog) { //入库记录查询 dialog controller
        scope.excelUrl = '';
        scope.formatData = function (obj) {
            var str = '?';
            for (var k in obj) {
                str += '&' + k + '=' + obj[k]
            }
            return str;
        };
        scope.getData = function (e) {
            scope.search.whCode = scope.userInfo.whCode;
            scope.search.userId = scope.userInfo.id;
            if ((scope.search.outConfirmStartTime && scope.search.outConfirmEndTime)) {
                scope.excelUrl = contextPath + 'outboundTask/outboundRecordImportExcel' + scope.formatData(scope.search);
                $timeout(function () {
                    document.getElementById('wrlExport').click();
                    scope.search.outConfirmStartTime = '';
                    scope.search.outConfirmEndTime = '';
                    ngDialog.closeAll()
                }, 20);
            } else {
                e.preventDefault();
                scope.Tools.alert('请填写出库起止时间！');
            }
        };
        scope.close = function () {
            ngDialog.closeAll()
        }
    }])
    .controller('wmsAppCtrl', ['$scope', '$rootScope', '$timeout', function ($scope, $rootScope, $timeout) {

        // fs ==  fullScreen
        $rootScope.bigScreenFlag = false;
        $rootScope.bs_reload = function () {
            $rootScope.bigScreenFlag = true;
            var docElm = document.documentElement;
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            }
            else if (docElm.msRequestFullscreen) {
                docElm = document.body; //overwrite the element (for IE)
                docElm.msRequestFullscreen();
            }
            else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            }
            else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            }
            console.log('这时候才全屏....');
        };
        $rootScope.bs_reload_cancel = function () {
            location.reload();
        }
    }])
    .controller('wmsPreparationPlanCtrl', ["$scope", '$rootScope', '$timeout', 'ngDialog', 'wmsPreparationPlanService', function (scope, rootScope, $timeout, ngDialog, wmsPreparationPlanService) {
        //更多条件默认值
        scope.moreText = scope.constant.moreText.spread;
        //更多条件
        scope.moreClick = function () {
            scope.otherForm = !scope.otherForm;
            if (scope.otherForm) {
                scope.moreText = scope.constant.moreText.packUp;
            } else {
                scope.moreText = scope.constant.moreText.spread;
            }
        };
        scope.search = {
            orderno: '',
            vin: '',
            customerName: '',
            status: '',
            startTime: '',
            endTime: ''
        };

        scope.statusList = [
            {name: '未处理', value: '20'},
            {name: '已处理', value: '30'}
        ];
        scope.customerList = [];

        //列表
        scope.fristSelect = true;   //判断是否是第一次查询
        scope.list = function (params) {
            //设置分页参数
            if (!params) {
                params = {};
            }
            params.pageSize = params.pageSize ? params.pageSize : scope.constant.page.pageSize;
            params.pageNo = params.pageNo ? params.pageNo : scope.constant.page.pageNo;

            // scope.search.departureType?scope.search.departureType=scope.search.departureType.value:'';
            // scope.search.departureInOutType?scope.search.departureInOutType=scope.search.departureInOutType.value:'';

            //避免出现对象数组引用
            var temp = {
                orderno: scope.search.orderno,
                vin: scope.search.vin,
                customerName: scope.search.customerName,
                status: scope.search.status,
                startTime: scope.search.startTime,
                endTime: scope.search.endTime
            };


            // 判断其是否为对象
            if (scope.search.status instanceof Object) {
                temp.status = scope.search.status.value;
            }

            params = angular.extend(params, temp);

            console.log(scope.search);

            wmsPreparationPlanService.getStockTransferList(params)
                .then(function (result) {
                    if (result.success) {
                        scope.tableList = result.data;

                        if (scope.fristSelect) {
                            scope.Tools.page(document.getElementById('pages'), {
                                pages: result.pageVo.totalPage,
                                curr: result.pageVo.pageNo,
                                count: result.pageVo.totalRecord
                            }, function (obj, first) {
                                scope.fristSelect = first;  //设置状态
                                //初始化多选框
                                // scope.Tools.initCheckBox();
                                if (!first) { //判断不是第一次加载
                                    var param = {};
                                    param.pageNo = obj.curr;    //当前页
                                    param.pageSize = obj.limit; //最大页
                                    scope.pageSize = obj.limit; //设置最大页
                                    scope.pageNo = obj.curr; //设置当前页
                                    scope.list(param);
                                }
                            })
                        } else {
                            //初始化多选框
                            // scope.Tools.initCheckBox();
                        }
                    } else {
                        // scope.Tools.alert(result.message);
                    }
                })
        }

        // 根据车架号， 回车查询
        scope.searchListKeyDown = function (e) {
            var keycode = window.event ? e.keyCode : e.which;
            if (keycode == 13) {  //回车键
                scope.fristSelect = true;   //设置为第一次查询
                scope.list();
            }
        }

        //  获取主列表
        rootScope.wppGetList = scope.list;
        scope.list();

        // 获取主客户
        scope.getCustomerList = function () {
            wmsPreparationPlanService.getCustomerNameList().then(function (res) {
                if (res.success) {
                    scope.customerList = [];
                    for (var i = 0; i < res.data.length; i++) {
                        scope.customerList.push(res.data[i].odCustomerName)
                    }
                    console.log(scope.customerList)
                }
            })
        }
        scope.getCustomerList();


        //查询 click
        scope.searchClick = function () {
            scope.fristSelect = true;   //设置为第一次查询
            scope.list();
        }

        //重置 click
        scope.resetClick = function () {
            scope.search = {
                orderno: '',
                vin: '',
                customerName: '',
                status: '',
                startTime: '',
                endTime: ''
            };
            scope.list();
        }

        //刷新 click
        scope.refreshClick = function () {
            scope.resetClick();
        }

        scope.createPlan = function () {
            scope.oddIdsArr = [];
            //获取选中的list列表
            $('.wms_wpp_main_table').find('td i').each(function (i, obj) {
                if ($(obj).parent().hasClass('layui-form-checked')) {
                    scope.oddIdsArr.push($(obj).parents('tr').attr('oddid'));
                }
            });
            if (scope.oddIdsArr == 0) {
                scope.Tools.alert('未选择！')
            } else {
                scope.Tools.dialog({
                    controller: 'wmsPreparationPlanCtrl.dialog',
                    template: 'templates/warehouseManagement/wmsPreparationPlan/layer/createPlan.html',
                    closeByDocument: false,
                    closeByEscape: false,
                    data: {
                        oddIds: scope.oddIdsArr.toString()
                    },
                    width: 615,
                    title: '创建备料计划',
                    scope: scope
                })
            }

        }
    }])
    .controller('wmsPreparationPlanCtrl.dialog', ['$scope', '$rootScope', '$timeout', 'ngDialog', 'wmsPreparationPlanService', function (scope, rootScope, $timeout, ngDialog, wmsPreparationPlanService) {

        scope.createObj = {
            oddIds: '',
            estimateFinishTime: '',
            estimateLoadingTime: '',
            carrier: '',
            vehiclePlate: ''
        };
        scope.createObj.oddIds = scope.ngDialogData.oddIds;

        //    确认创建
        scope.confirmCreate = function () {
            wmsPreparationPlanService.savePreparationPlanStockTransfer(scope.createObj).then(function (res) {
                if (res.success) {
                    rootScope.wppGetList();
                    ngDialog.closeAll();
                    $timeout(function () {
                        scope.Tools.alert(data.message)
                    }, 20)
                } else {

                }
            })
        }

        //    关闭  创建备料计划
        scope.close = function () {
            ngDialog.closeAll()
        }
    }]);
