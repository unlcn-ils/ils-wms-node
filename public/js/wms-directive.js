/**
 * Created by lsj on 17/9/20.
 */
'use strict';
var directive = angular.module('wms.directives', [])
.directive('refresh',['$timeout',function(timeout){    //刷新按钮特效
    return {
        restrict:'A',//使用方式
        replace:false,//是否用模板代替指令
        scope:true,//作用域
        controller:false,//控制器
        transclude:false,//设置为true表示当前指令中可以嵌入其他内容或指令
        link: function (scope,element,attrs,ctrl) {
            $(element).bind('click',function(){
                $(element).css('display','inline-block');
                timeout(function(){
                    $(element).css('display','');
                },1000)
            })
        }
    };
}])
.directive('setFocus',[function(){  //input 获取焦点
    return {
        restrict:'A',//使用方式
        replace:false,//是否用模板代替指令
        scope:false,//作用域
        controller:false,//控制器
        transclude:false,//设置为true表示当前指令中可以嵌入其他内容或指令
        link: function (scope,element,attrs,ctrl) {
            element[0].focus(); //获取焦点
        }
    };
}])
.directive('keyListener',['$timeout',function(timeout){ //禁止输入
    return{
        restrict:'A',//使用方式
        replace:false,//是否用模板代替指令
        scope:true,//作用域
        controller:false,//控制器
        transclude:false,//设置为true表示当前指令中可以嵌入其他内容或指令
        link: function (scope,element,attrs,ctrl) {
            angular.element(element).keydown(function (e) {
                var keyCode = window.event ? e.keyCode:e.which;

                if(keyCode == 8){ //删除键
                    scope.$apply(function(){
                        scope[attrs.ngModel] = '';
                    });
                }else if(keyCode >= 48 && keyCode <= 107){
                    return false;
                }else if(keyCode >= 109 && keyCode <= 111){
                    return false;
                }else if(keyCode >= 186 && keyCode <= 222){
                    return false;
                }
            })
        }
    }
}])
.directive('repeatFinish',['$timeout',function($timeout){
    return {
        link: function(scope,element,attr){

            if(scope.$last == true){
                //当table有数据时，清空无数据标识
                if($(element[0]).tagName = 'TR'){
                    $timeout(function(){
                        layui.use('form',function(){
                            var $ = layui.jquery, form = layui.form();
                            form.render();
                        });
                    },20);
                }
            }
        }
    }
}])
    .directive('trSelect', ['$document',function($document) {
        return {
            restrict: 'AEC',
            scope: {
                copyText: '='
            },
            link: function(scope, element, attrs){

                //点击事件
                element.bind('click', function(){
                    var tms_tr =  $(element[0]).parent(); // tr
                    //创建将被复制的内容
                    var i =           tms_tr.first().find('div i');
                    var i_div =       tms_tr.first().find('div');
                    var table_div =   tms_tr.parent().find('div');
                    var tr =          tms_tr;
                    var input =       tms_tr.find('input');
                    var table_input = tms_tr.parent().find('input');
                    if(i_div.hasClass('layui-form-checkbox')){       // 如果为checkBox
                        if(i_div.hasClass('layui-form-checked')){    // 被选中
                            input.prop('checked',false);
                            i_div.removeClass('layui-form-checked');
                            tr.removeClass('trSelect');
                        } else {                                        //没有被选中
                            table_input.prop('checked',false);
                            table_div.removeClass('layui-form-checked');
                            i_div.addClass('layui-form-checked');
                            input.prop('checked',true);
                            tr.addClass('trSelect').siblings().removeClass('trSelect');
                        }

                    }
                    if(i_div.hasClass('layui-form-radio')){
                        table_input.prop('checked',false);
                        input.prop('checked',true);
                        i.html('&#xe643;');
                        table_div.removeClass('layui-form-radioed');
                        i_div.addClass('layui-form-radioed');
                        tr.addClass('trSelect').siblings().removeClass('trSelect');
                    }
                });
            }

        };
    }])
    .directive('percentage',['$timeout',function($timeout){
            return {
                restrict:'AE',
                replace:true,
                transclude:true,
                scope: {
                    pt:'='
                },
                templateUrl:'/templates/bigScreen/components/percentage.html',
                link : function(scope, elem, attrs, ctrl,$timeout){
                    scope.thisPt = (scope.pt*100.00).toFixed(2);
                }
            }
    }])
    .directive('status',['$timeout',function($timeout){
            return {
                restrict:'AE',
                replace: true,
                transclude: true,
                scope:{
                    st:'='
                },
                templateUrl: './templates/bigScreen/components/status.html',
                link : function(scope,elem,attrs,$timeout){
                    scope.statusArr = [
                        {code:'NORMAL',text:'正常',color:'#00c261'},
                        {code:'WARNING',text:'预警',color:'#f29100'},
                        {code:'OVERDUE-WARNING',text:'超期',color:'#f30'},
                    ];
                    scope.currStatus ={code:1,text:'正常',color:'#00c261'};
                    for(var i=0; i<scope.statusArr.length; i++){
                        if(scope.st == scope.statusArr[i].text){
                            scope.currStatus = scope.statusArr[i];
                        }
                    }
                }
            }
    }])
    .directive('transport',['$timeout',function($timeout){
            return {
                restrict:'AE',
                replace: true,
                transclude: true,
                scope:{
                    ts:'='
                },
                templateUrl:'./templates/bigScreen/components/transport.html',
                link: function(scope, elem, attrs, $timeout){
                    scope.tsURL = '';
                    scope.imageArr = [
                        {name:'板车陆运',url:'tuobanche.png'},
                        {name:'水运',url:'ship.png'},
                        {name:'铁运',url:'train.png'},
                        {name:'地跑陆运',url:'driver.png'}
                        ];
                    for(var i=0; i<scope.imageArr.length;i++){
                        if(scope.ts==scope.imageArr[i].name){
                            scope.tsURL = scope.imageArr[i].url
                        }
                    }
                }
            }
    }])