/**
 * Created by Tuffy on 16/6/7.
 */
'use strict';

var wmsService = angular.module('wms.services', [])
    .constant('Settings', {
        Context: {
            path: contextPath
        }
    })
    .service('HttpService', ['Restangular', '$rootScope', function (restangular, rootScope) {

        return {
            customPOST: function (url, data, obj) {
                if (obj) {
                    return restangular.one(url).customPOST(data, undefined, undefined, obj);
                } else {
                    return restangular.one(url).customPOST(data);
                }
            },
            post: function (url, data) {
                return restangular.one(url).post(data);
            },
            get: function (url, params) {
                return restangular.one(url).get('/', params);
            },
            customGET: function (url, params) {
                return restangular.one(url).customGET('/', params);
            },
            put: function (url, data) {

            },
            del: function (url, data) {

            }
        };
    }])
    .service('inStorageBillService', ['HttpService', function (httpService) {   //入库service
        return {
            addReceipt: function (data) {  //新增收货单
                return httpService.customPOST('asnOrder/addAsnOrder', data);
            },
            inStorageList: function (data) {  //入库单列表
                return httpService.customPOST('asnOrder/getAsnOrderList', data);
            },
            deliverToConfirm: function (data) {   //君马收货质检
                return httpService.customPOST('asnOrder/deliverToConfirm', data);
            },
            reservoirList: function (whCode) { //库区查询
                return httpService.customGET('baseData/getEmptyZone/' + whCode);
            },
            storageLocationList: function (whCode, zoneId) {   //库位查询
                return httpService.customGET('baseData/getEmptyLocation/' + whCode + '/' + zoneId);
            },
            createAlloc: function (allocType, data) {   //手动分配
                return httpService.customPOST('wmsAlloc/createAlloc/' + allocType, data);
            },
            confirmAllocation: function (data) { //确认分配
                return httpService.customPOST('wmsAlloc/confirmAllocation', data);
            },
            confirmInbound: function (data, userId) {    //确认入库
                return httpService.customPOST('asnOrder/confirmInbound/' + userId, data);
            },
            printBarcode: function (data) {  //打印
                return httpService.customPOST('asnOrder/printBarcode', data);
            },
            getReceiptById: function (id) { //根据id获取入库单
                return httpService.customGET('asnOrder/getAsnOrderById/' + id);
            },
            updateAsnOrder: function (data) {    //更新入库单
                return httpService.customPOST('asnOrder/updateAsnOrder', data);
            },
            deleteAsnOrder: function (data) {    //删除入库单
                return httpService.customPOST('asnOrder/deleteAsnOrder', data);
            },
            autoAssign: function (whcode, data) {  //自动分配
                return httpService.customPOST('wmsAlloc/autoAllcation/' + whcode, data);
            },
            getInBoundInfoByVin: function (param) {  //根据运单号或者底盘号查询订单
                return httpService.customGET('asnOrder/getInBoundInfoByVin/' + param);
            },
            cancelInbound: function (data) { //取消入库
                return httpService.customPOST('asnOrder/updateCancleInbound', data);
            },
            queryByVin: function (vin) {    //根据vin号查询
                return httpService.customGET('asnOrder/queryByVin/' + vin);
            },
            locationAdjust: function (data) {    //库位分配
                return httpService.customPOST('asnOrder/locationAdjust', data);
            },
            inboundRecord: function (json) { //入库单记录查询
                return httpService.customPOST('asnOrder/inboundRecord', json, {'Content-Type': 'text/plain;charset=UTF-8'});
            },
            getAsnList: function (json) {    //asn入库单查询
                return httpService.customPOST('asn/getAsnList', json, {'Content-Type': 'text/plain;charset=UTF-8'});
            },
            barcodePrint: function (info, userId) {  //西南库收货质检
                return httpService.customPOST('extraInbound/barcodePrint/' + userId, info, {'Content-Type': 'text/plain;charset=UTF-8'});
            },
            printAsnQrCode: function (data) {
                return httpService.customPOST('jminbound/printAsnQrCode', data);
            },
            downloadTemplate: function () {
                return httpService.customGET('/jminbound/downloadTemplate');
            },
            queryListByTime: function (data) {
                return httpService.customPOST('asn/queryListByTime', data);
            }
        };
    }])
    .service('inventorySelectService', ['HttpService', function (httpService) {    //库存service
        return {
            list: function (data) {  //库存列表
                return httpService.customPOST('wmsInventory/list', data);
            },
            detail: function (id) {    //详情
                return httpService.customGET('wmsInventory/listInventoryLocation/' + id);
            },
            queryInventoryRecordCount: function (data) {    //库存导出验证接口
                return httpService.customGET('wmsInventory/queryInventoryRecordCount' + data)
            },
            stockFreeze: function (data) {    //库存导出验证接口
                return httpService.customPOST('wmsInventory/stockFreeze', data)
            },
            stockNormal: function (data) {    //库存导出验证接口
                return httpService.customPOST('wmsInventory/stockNormal', data)
            },
            changeLocCode: function (data) {    //库存导出验证接口
                return httpService.customPOST('wmsInventory/changeLocCode', data)
            }
        }
    }])
    .service('inventoryRecordsQueryservice', ['HttpService', function (HttpService) {
        return {
            queryRecordCount: function (data) {
                return HttpService.customGET('/asnOrder/queryRecordCount' + data)
            }
        }
    }])
    .service('warehouseMaintenanceService', ['HttpService', function (httpService) {    //仓库维护 service
        return {
            list: function (data) {  //列表查询
                return httpService.customPOST('wh/getWarehouseList', data);
            },
            addWarehouse: function (data) {  //新增仓库
                return httpService.customPOST('wh/addWarehouse', data);
            },
            updateWarehouse: function (data) {   //更新仓库
                return httpService.customPOST('wh/updateWarehouse', data);
            },
            deleteWarehouse: function (data) {   //删除仓库
                return httpService.customPOST('wh/deleteWarehouse', data);
            },
            getEstimateLaneByWhCode: function (whName) {   //装车道列表
                return httpService.customPOST('estimateLane/getEstimateLaneByWhCode', whName, {'Content-Type': 'text/plain;charset=UTF-8'});
            },
            addEstimateLane: function (data) {   //新增装车道
                return httpService.customPOST('estimateLane/addEstimateLane', data);
            },
            deleteEstimateLane: function (data) {    //删除装车道
                return httpService.customPOST('estimateLane/deleteEstimateLane', data);
            }
        }
    }])
    .service('reservoirService', ['HttpService', function (httpService) {   //库区service
        return {
            list: function (data) {  //库区列表
                return httpService.customPOST('wmsZone/list', data);
            },
            add: function (data) {   //新增库区
                return httpService.customPOST('wmsZone/addZone', data);
            },
            update: function (data) {    //更新库区
                return httpService.customPOST('wmsZone/updateZone', data);
            },
            getById: function (id) { //根据id查库区
                return httpService.customGET('wmsZone/getZoneById/' + id);
            },
            deleteZoneNew: function (data) {   //删除仓库
                return httpService.customPOST('/wmsZone/deleteZoneNew', data);
            }
        }
    }])
    .service('stockUnitService', ['HttpService', function (httpService) {  //库位 service
        return {
            list: function (data) {  //库位列表
                return httpService.customPOST('wmsLocation/list', data);
            },
            add: function (data) {   //新增库位
                return httpService.customPOST('wmsLocation/addLocation', data)
            },
            getById: function (id) { //根据id查询库位
                return httpService.customGET('wmsLocation/getLocationById/' + id);
            },
            update: function (data) {    //更新库位
                return httpService.customPOST('wmsLocation/updateLocation', data);
            },
            enableLocation: function (data, flag) {   //更新库位状态
                return httpService.customPOST('wmsLocation/enableLocation/' + flag, data);
            }
        }
    }])
    .service('repairBillService', ['HttpService', function (httpService) { //维修单 service
        return {
            list: function (data) {  //维修单列表
                return httpService.customPOST('wmsRepair/list', data);
            },
            submitRepair: function (data) {  //提交维修单
                return httpService.customPOST('wmsRepair/submitRepair', data);
            },
            addWmsRepair: function (data) {  //维修单新增
                return httpService.customPOST('wmsRepair/addWmsRepair', data);
            },
            updateWmsRepair: function (data) {   //维修单更新
                return httpService.customPOST('wmsRepair/updateWmsRepair', data);
            },
            getById: function (id) {   //维修单id查询
                return httpService.customGET('wmsRepair/getRepairById/' + id);
            },
            submitAfterCheckAdd: function (data) {   //维修单后检验
                return httpService.customPOST('wmsRepair/submitAfterCheckAdd', data);
            },
            submitAfterCheckCopyAndAdd: function (data) {    //维修单后检验复制并新增
                return httpService.customPOST('wmsRepair/submitAfterCheckCopyAndAdd', data);
            },
            printRepair: function (data) {    // 打印维修单
                return httpService.customPOST('wmsRepair/printRepair', data)

            }
        }
    }])
    .service('warehouseRecordExportService', ['HttpService', function (HttpService) {
        return {
            outboundRecordImportExcel: function (data) {
                return HttpService.customGET('outboundTask/outboundRecordImportExcel' + data)
            }
        }
    }])
    .service('warehouseService', ['HttpService', function (httpService) {  //出库单
        return {
            list: function (data) {  //出库单列表
                return httpService.customPOST('outboundTask/getOutboundTaskList', data);
            },
            addBLOrder: function (data) {    //新增备料单
                return httpService.customPOST('outOrder/addBLOrder', data);
            },
            getEstimateLaneByWhCode: function (whName) {   //装车道列表
                return httpService.customPOST('estimateLane/getEstimateLaneByWhCode', whName, {'Content-Type': 'text/plain;charset=UTF-8'});
            },
            removeLocationConfirm: function (data) { //移库确认
                return httpService.customPOST('outOrder/removeLocationConfirm', data);
            },
            outboundCheck: function (data) { //出库质检
                return httpService.customPOST('outOrder/outboundCheck', data);
            },
            outboundConfirm: function (data) {   //出库确认
                return httpService.customPOST('outOrder/outboundConfirm', data);
            },
            exceptionRecord: function (data) {   //异常登记
                return httpService.customPOST('outOrder/exceptionRecord', data);
            },
            outboundPlanList: function (json) {  //备料计划list
                return httpService.customPOST('wmsPreparationPlan/list', json, {'Content-Type': 'text/plain;charset=UTF-8'});
            },
            getVehicleDetail: function (ppId) {  //备料计划详情
                return httpService.customGET('wmsPreparationPlan/getVehicleDetail/' + ppId);
            },
            createPlan: function (estimateLaneId, data) {    //创建备料计划
                return httpService.customPOST('wmsPreparationPlan/createPlan/' + estimateLaneId, data);
            },
            getPreparateInfoByVin: function (vin) { //根据vin查询出库作业信息
                return httpService.customGET('outboundTask/getPreparateInfoByVin/' + vin);
            },
            getPreparateInfoByMaterialNo: function (materialNo) {  //根据备料单号 查询
                return httpService.customGET('outboundTask/getPreparateInfoByMaterialNo/' + materialNo);
            },
            updateTaskToOutbound: function (data) {  //出库确认
                return httpService.customPOST('outboundTask/updateTaskToOutbound', data);
            },
            updateTaskToConfirm: function (data) {   //备料完成
                return httpService.customPOST('outboundTask/updateTaskToConfirm', data);
            },
            contrastInfo: function (vin) {   //根据vin获取扫码信息
                return httpService.customGET('extraOutbound/contrastInfo/' + vin);
            },
            getOutboundRecordList: function (data) { //出库记录查询
                return httpService.customPOST('outboundTask/getOutboundRecordList', data);
            },
            updateStatus: function (vin) { //更新出库单状态
                return httpService.customGET('extraOutbound/updateStatus/' + vin);
            },
            getPrintInfo: function (otId) {  //获取交接单打印信息
                return httpService.customGET('outboundTask/getPrintInfo/' + otId);
            },
            getVinList: function (orderNo) {    //根据单据号获取vinlist
                return httpService.customGET('outboundTask/getVinList/' + orderNo);
            },
            outConfirm: function (data) {    //出库确认
                return httpService.customPOST('outboundTask/outConfirm', data);
            },
            updatePrintInfo: function (otId, userId) {   //出库交接单打印信息更新
                return httpService.customPOST('outboundTask/updatePrintInfo/' + otId + '/' + userId);
            },
            cancleTaskForCq: function (data) {   //任务取消
                return httpService.customPOST('outboundTask/cancleTaskForCq', data);
            },
            quitTaskForCq: function (data) { //备料退库
                return httpService.customPOST('outboundTask/quitTaskForCq', data);
            },
            getPrintOutboundTaskList: function (data) { //出库交接单列表
                return httpService.customPOST('outboundTask/getPrintOutboundTaskList', data);
            },
            updateShipToAddPlan: function (ppId) {   //发运计划变更
                return httpService.customPOST('wmsPreparationPlan/updateShipmentToTask/' + ppId);
            },
            addCar: function (ppId) {    //增车
                return httpService.customPOST('wmsPreparationPlan/updateShipToAddPlan/' + ppId);
            },
            deleteCar: function (vdId) { //删除车
                return httpService.customPOST('wmsPreparationPlan/deletePreparetionDetail/' + vdId);
            }
        }
    }])
    .service('stockUnitStrategyService', ['HttpService', function (httpService) {   //策略service
        return {
            list: function (whCode) {  //策略列表    仓库code
                return httpService.customGET('inboundPloy/getPloyList/' + whCode);
            },
            getPloyConditionList: function (pyCode) {  //策略条件列表
                return httpService.customGET('inboundPloy/getPloyConditionList/' + pyCode);
            },
            getPloyFlowtoList: function (pyCode) {   //策略流向列表
                return httpService.customGET('inboundPloy/getPloyFlowtoList/' + pyCode);
            },
            getPloyLocationList: function (pyCode) { //策略库位分配
                return httpService.customGET('inboundPloy/getPloyLocationList/' + pyCode);
            },
            addPloy: function (data) {   //新增策略
                return httpService.customPOST('inboundPloy/addPloy', data);
            },
            updatePloy: function (data) {    //更新策略
                return httpService.customPOST('inboundPloy/updatePloy', data);
            },
            addPloyCondition: function (data) {  //新增策略条件
                return httpService.customPOST('inboundPloy/addPloyCondition', data);
            },
            addPloyFlowto: function (data) { //新增策略流向
                return httpService.customPOST('inboundPloy/addPloyFlowto', data);
            },
            addPloyLocation: function (data) {    //新增策略库位配置
                return httpService.customPOST('inboundPloy/addPloyLocation', data);
            },
            deletePloy: function (data) {    //删除策略
                return httpService.customPOST('inboundPloy/deletePloy', data);
            },
            deletePloyCondition: function (data) {   //删除策略条件
                return httpService.customPOST('inboundPloy/deletePloyCondition', data);
            },
            deletePloyFlowto: function (data) {   //删除策略流向
                return httpService.customPOST('inboundPloy/deletePloyFlowto', data);
            },
            deletePloyLocation: function (data) {   //删除策略库位
                return httpService.customPOST('inboundPloy/deletePloyLocation', data);
            },
            genPloyContent: function (pyCode) {  //生成规则
                return httpService.customGET('inboundPloy/genPloyContent/' + pyCode);
            }
        }
    }])
    .service('shipmentPlanService', ['HttpService', function (httpService) {    //发运计划service
        return {
            getShipmentPLanList: function (data) {   //获取发运计划列表
                return httpService.customPOST('wmsShipmentPlan/getShipmentPLanList', data);
            },
            getShipmentPLanVehicleList: function (parentId) {   //获取区域信息
                return httpService.customGET('/wmsShipmentPlan/getShipmentPLanVehicleList/' + parentId);
            },
            createPreparationPlan: function (data) {    //获取车型
                return httpService.customPOST('wmsShipmentPlan/createPreparationPlan', data);
            }
        }
    }])
    .service('userService', ['HttpService', function (httpService) {    //用户service
        return {
            listUser: function (data) {   //用户列表
                return httpService.customPOST('sysUser/listUser', data);
            },
            addSysUser: function (data) {    //新增用户
                return httpService.customPOST('sysUser/addSysUser', data);
            },
            deleteSysUser: function (data) { //删除用户
                return httpService.customPOST('sysUser/deleteSysUser', data);
            },
            findSysUserById: function (userId) { //用户id
                return httpService.customGET('sysUser/findSysUserById/' + userId);
            },
            updateSysUser: function (data) { //更新用户
                return httpService.customPOST('sysUser/updateSysUser', data);
            }
        }
    }])
    .service('roleService', ['HttpService', function (httpService) {    //角色 service
        return {
            listRole: function (data) {  //角色列表
                return httpService.customPOST('sysRole/listRole', data);
            },
            addRole: function (data) {   //新增角色
                return httpService.customPOST('sysRole/addRole', data);
            },
            deleteRole: function (data) {    //删除角色
                return httpService.customPOST('sysRole/deleteRole', data);
            },
            listPermiss: function (data) {   //根据角色id获取权限
                return httpService.customPOST('sysRole/listPermiss', data);
            },
            updateRole: function (data) {    //更新角色
                return httpService.customPOST('sysRole/updateRole', data);
            }
        }
    }])
    .service('permissionsService', ['HttpService', function (httpService) { //角色 service
        return {
            list: function (data) {  //角色列表
                return httpService.customPOST('permissions/list', data);
            },
            getPermissListByUserId: function (userId) {    //根据用户id获取角色
                return httpService.customGET('permissions/getPermissListByUserId/' + userId);
            }
        }
    }])
    .service('dropDownService', ['HttpService', function (httpService) {    //下拉service
        return {
            getZoneList: function (data) {   //库区下拉 仓库code
                return httpService.customGET('baseData/getZone/' + data);
            },
            getCustomerInfo: function () {   //获取货主
                return httpService.customGET('baseData/getCustomerInfo');
            },
            getVehicleSpec: function () {    //获取车型  -- /baseData/getVehicleSpec
                return httpService.customGET('baseData/getVehicleSpec');
            },
            getColorListInventorySelect: function (data) {    //获取颜色列表
                return httpService.customPOST('/wmsInventory/colourList', data);
            },
            getMaterialListInventorySelect: function (data) {    //获取材料列表
                return httpService.customPOST('/wmsInventory/materialList', data);
            },
            getArea: function (parentId) {   //获取区域信息
                return httpService.customGET('/baseData/getAreaTree/' + parentId);
            },
            getWarehouse: function () {  //获取仓库
                return httpService.customGET('/wh/getWarehouse');
            },
            getColorList: function () {  //车型颜色
                return httpService.customGET('/baseData/getColorList');
            },
            getUserByWarehouse: function () {    //获取司机
                return httpService.customGET('/sysUser/getUserByWarehouse');
            }
        }
    }])
    .service('borrowCarRegistrationService', ['HttpService', function (HttpService) {
        return {
            borrowLine: function (data) {
                return HttpService.customPOST('/borrow/borrowLine', data);
            },
            findBorrowDetail: function (data) {
                return HttpService.customGET('/borrow/findBorrowDetail/' + data);
            },
            //新增 /borrow/add
            add: function (data) {
                return HttpService.customPOST('/borrow/add', data);
            },
            modify: function (data) {
                return HttpService.customPOST('/borrow/modify', data);
            },
            del: function (data) {
                return HttpService.customPOST('/borrow/del', data);
            },
            //     获取编辑详情
            queryModifyDetail: function (data) {
                return HttpService.customGET('/borrow/queryModifyDetail/' + data);
            },
            //    获取打印列表信息 /borrow/printInfo
            printInfo: function (data) {
                return HttpService.customGET('/borrow/printInfo/' + data);
            },
            //    获取千牛token
            getUploadToken: function () {
                return HttpService.customGET('/borrow/getUploadToken');
            },
            //    通过key  获取 url
            getDownUrl: function (data) {
                return HttpService.customGET('/borrow/getDownUrl/' + data)
            }
        }
    }])
    .service('returnCarRegistrationService', ['HttpService', function (HttpService) {
        return {
            returnLine: function (data) {
                return HttpService.customPOST('/return/returnLine', data);
            },
            findReturnDetail: function (data) {
                return HttpService.customGET('/return/findReturnDetail/' + data);
            },
            add: function (data) {
                return HttpService.customPOST('/return/add', data);
            },
            //    编辑前获取数据 /return/queryModifyDetail
            queryModifyDetail: function (data) {
                return HttpService.customGET('/return/queryModifyDetail/' + data);
            },
            del: function (data) {
                return HttpService.customPOST('/return/del', data);
            },

            //    编辑保存 /return/modify
            modify: function (data) {
                return HttpService.customPOST('/return/modify', data);
            },
            //    还车时 Enter 查询   /return/getDetail/
            getDetail: function (data) {
                return HttpService.customGET('/return/getDetail/' + data);
            }

        }
    }])
    .service('borrowAndReturnRecordSearchService', ['HttpService', function (HttpService) {
        return {
            borrowHistoryLine: function (data) {  // 1
                return HttpService.customPOST('/borrow/borrowHistoryLine', data);
            }
        }
    }])
    .service('shipmentPlanRejectService', ['HttpService', function (HttpService) {
        return {
            queryShipmentList: function (data) {
                return HttpService.customPOST('/shipmentReject/queryShipmentList', data)
            },
            shipmentReject: function (data) {
                return HttpService.customGET('/shipmentReject/shipmentReject/' + data)
            },
            rejectUnDealList: function (data) {
                return HttpService.customGET('/shipmentReject/rejectUnDealList/' + data)
            },
            rejectUnDeal: function (data) {
                return HttpService.customPOST('/shipmentReject/rejectUnDeal/', data)
            },
            queryDetailList: function (data) {
                return HttpService.customGET('/shipmentReject/queryDetailList/' + data)
            },
            cancleTask: function (data) {
                return HttpService.customPOST('/shipmentReject/cancleTask', data)
            },
            changePrepare: function (data) {
                return HttpService.customPOST('/shipmentReject/changePrepare', data)
            },
            changeByVin: function (data) {
                return HttpService.customPOST('/shipmentReject/changeByVin', data)
            }
        }
    }])
    .service('entranceCardService', ['HttpService', function (HttpService) {    //门禁卡 service
        return {
            list: function (data) {  //列表
                return HttpService.customPOST('/card/cardLine', data)
            },
            queryDriverUser: function () {   //获取司机用户
                return HttpService.customGET('card/queryDriverUser');
            },
            addCard: function (data) {   //新增门禁卡
                return HttpService.customPOST('card/add', data);
            },
            updateCard: function (data) {   //新增门禁卡
                return HttpService.customPOST('card/modify', data);
            }
        }
    }])
    .service('scooterOffRegistrationService', ['HttpService', function (httpService) {  //板车离场 service
        return {
            queryRegisterList: function (data) { //列表
                // return httpService.customPOST('departrueRegister/queryRegisterList',data);
                return httpService.customPOST('gate/gateOpenFailList', data);
            },
            normarDeparture: function (data) {   //正常离场
                return httpService.customPOST('/departrueRegister/normarDeparture', data);
            },
            errorDeparture: function (data) {   //异常离场
                return httpService.customPOST('/departrueRegister/errorDeparture', data);
            },
            outboundGateOpen: function (data) {
                // return httpService.customPOST('/departrueRegister/outboundGateOpen',data);
                return httpService.customPOST('gate/gateOpenByHm', data);
            }
        }
    }])
    .service('onWayService', ['HttpService', function (HttpService) {
        return {
            onWayLine: function (data) {
                return HttpService.customPOST('/bigscreen/onWayLine', data)
            }
        }
    }])
    .service('pickCarService', ['HttpService', function (HttpService) {
        return {
            reasonInfo: function () {
                return HttpService.customGET('/bigscreen/reasonChartCount');
            },
            pickDataCount: function () {
                return HttpService.customGET('/bigscreen/pickChartCount')
            },
            shipmentDataCount: function () {
                return HttpService.customGET('/bigscreen/shipmentChartCount')
            },
            transModelCount: function () {
                return HttpService.customGET('/bigscreen/transModelChartCount')
            }
        }
    }])
    .service('distributionService', ['HttpService', function (HttpService) {
        return {
            // 获取上面主报表  /bigscreen/distributionChartCount
            distributionChartCount: function () {
                return HttpService.customGET('/bigscreen/distributionChartCount');
            },
            //  获取表格    /bigscreen/distributionLineCount
            distributionLineCount: function () {
                return HttpService.customGET('/bigscreen/distributionLineCount')
            }


        }
    }])
    .service('transferDepotService', ['HttpService', function (HttpService) {
        return {
            //    前置库主报表  /bigscreen/invoicingStatisticsChartCount
            invoicingStatisticsChartCount: function () {
                return HttpService.customGET('/bigscreen/invoicingStatisticsChartCount');
            },
            //    前置库进、销、存  /bigscreen/invoicingStatisticsInfo
            invoicingStatisticsLineCount: function () {
                return HttpService.customGET('/bigscreen/invoicingStatisticsLineCount');
            }
        }
    }])
    .service('drayOutInService', ['HttpService', function (HttpService) {
        return {
            //    获取报表 /bigscreen/inOutDrayChartCount
            inOutDrayChartCount: function () {
                return HttpService.customGET('/bigscreen/inOutDrayChartCount')
            },
            //    获取下面表格  /bigscreen/inOutDrayLineCount
            inOutDrayLineCount: function () {
                return HttpService.customGET('/bigscreen/inOutDrayLineCount')
            }
        }
    }])
    .service('gateService', ['$http', 'HttpService', function (http, httpService) {  //闸门控制 service
        return {
            open: function (channel, open, led1, led2, sound) { //打开闸门
                return http({
                    method: 'POST',
                    url: 'http://192.168.5.210:8080/Open',
                    headers: {'Content-Type': 'text/plain;charset=UTF-8'},
                    data: {
                        cmd: 'lpr',
                        data: {
                            channel: channel,
                            open: open,
                            led1: led1,
                            led2: led2,
                            sound: sound
                        }
                    }
                })
            },
            getByType: function (data) { //根据类型获取闸门配置
                return httpService.customPOST('gateConfig/getByType/', data);
            }
        }
    }])
    .service('junmaGateService', ['$http', 'HttpService', function (http, HttpService) {   // 君马控制闸门
        return {
            open: function (params) {
                return http({
                    method: 'POST',
                    url: 'http://172.18.34.168/Parking/ParkingSevice.aspx',
                    headers: {'Content-Type': 'text/plain;charset=UTF-8'},
                    data: params
                })
            }
        }
    }])
    .service('searchWaybillStatusService', ['HttpService', function (HttpService) {
        return {
            getTrackList: function (data) {
                return HttpService.customPOST('/vehicleTracking/getTrackList', data)
            },
            importOutExcel: function (data) {
                // $.get(contextPath+'/vehicleTracking/importOutExcel'+data,function(){});
                return HttpService.customGET('/vehicleTracking/importOutExcel' + data);
            }
        }
    }])
    .service('abnormalManagementService', ['HttpService', function (HttpService) {
        return {
            queryExcepList: function (data) {
                return HttpService.customPOST('/excpManage/queryExcepList', data)
            },
            getImageList: function (data) {
                return HttpService.customPOST(' /excpManage/getImageList', data)
            },
            updateToDeal: function (data) {    // 异常处理
                return HttpService.customPOST('/excpManage/updateToDeal', data)
            },
            updateToClosed: function (data) {    // 异常关闭
                return HttpService.customPOST('/excpManage/updateToClosed', data)
            }
        }
    }])
    .service('boardCarInventoryRegistrationService', ['HttpService', function (HttpService) {
        return {
            inboundGateList: function (data) {
                return HttpService.customPOST('gate/inboundGateList', data)
            },
            inboundGateOpen: function (data) {
                return HttpService.customPOST('gate/inboundGateOpen', data)
            }
        }
    }])
    .service('wmsPreparationPlanService', ['HttpService', function (HttpService) {
        return {
            getStockTransferList: function (data) {    // 获取主列表
                return HttpService.customPOST('wmsPreparationPlan/getStockTransferList', data)
            },
            savePreparationPlanStockTransfer: function (data) {   // 手动创建备料计划
                return HttpService.customPOST('wmsPreparationPlan/savePreparationPlanStockTransfer', data)
            },
            getCustomerNameList: function (data) {     // 获取客户列表
                return HttpService.customGET('wmsPreparationPlan/getCustomerNameList')
            }
        }
    }])
    .service('wmsJmOurOfStorageOperationService', ['HttpService', function (HttpService) {
        return {
            getSendList: function (data) {
                return HttpService.customPOST('/outOfStorageOperation/queryList', data)
            },
            updateSendDcsAgain: function (data) {
                return HttpService.customPOST('/outOfStorageOperation/sendDcsAgain', data)
            },
            updateSendSapAgain: function (data) {
                return HttpService.customPOST('/outOfStorageOperation/sendSapAgain', data)
            },
            updateDcsToSuccess: function (data) {
                return HttpService.customPOST('/outOfStorageOperation/updateDcsToSuccess', data)
            },
            updateSapToSuccess: function (data) {
                return HttpService.customPOST('/outOfStorageOperation/updateSapToSuccess', data)
            }
        }
    }])
    .service('wmsJmHandoverOperationService', ['HttpService', function (HttpService) {
        return {
            getSendList: function (data) {
                return HttpService.customPOST('/handoverOrderOperation/queryList', data)
            },
            updateSendDcsAgain: function (data) {
                return HttpService.customPOST('/handoverOrderOperation/updateSendToDCSAgain', data)
            },
            updateSendSapAgain: function (data) {
                return HttpService.customPOST('/handoverOrderOperation/updateSendToSAPAgain', data)
            },
            updateSendCrmAgain: function (data) {
                return HttpService.customPOST('/handoverOrderOperation/updateSendToCRMAgain', data)
            },
            updateDcsToSuccess: function (data) {
                return HttpService.customPOST('/handoverOrderOperation/updateSendDCSToSuccess', data)
            },
            updateSapToSuccess: function (data) {
                return HttpService.customPOST('/handoverOrderOperation/updateSendSAPToSuccess', data)
            },
            updateCrmToSuccess: function (data) {
                return HttpService.customPOST('/handoverOrderOperation/updateSendCRMToSuccess', data)
            }
        }
    }])
    .service('inventoryVerificationService', ['HttpService', function (HttpService) {
        return {
            getInboundManageList: function (data) {
                return HttpService.customPOST('/inventoryManager/getInboundManageList', data)
            },
            getInboundManageListExport: function (data) {
                return HttpService.customGET('/inventoryManager/getInboundManageExport', data)
            },
            getOutboundManageList: function (data) {
                return HttpService.customPOST('/inventoryManager/getOutboundManageList', data)
            },
            getOutboundManageListExport: function (data) {
                return HttpService.customGET('/inventoryManager/getOutboundManageExport', data)
            },
            getInventoryManageList: function (data) {
                return HttpService.customPOST('/inventoryManager/getInventoryManageList', data)
            }
            // updateSendCrmAgain: function (data) {
            //     return HttpService.customPOST('/handoverOrderOperation/updateSendToCRMAgain', data)
            // },
            // updateDcsToSuccess: function (data) {
            //     return HttpService.customPOST('/handoverOrderOperation/updateSendDCSToSuccess', data)
            // },
            // updateSapToSuccess: function (data) {
            //     return HttpService.customPOST('/handoverOrderOperation/updateSendSAPToSuccess', data)
            // },
            // updateCrmToSuccess: function (data) {
            //     return HttpService.customPOST('/handoverOrderOperation/updateSendCRMToSuccess', data)
            // }
        }
    }])
    .service('urlService',['HttpService',function(HttpService){
        return {
            getUrlList : function(data){
                return HttpService.customGET('/gate/getJmGateUrl',data)
            },
            editeUrl : function(data){
                return HttpService.customPOST('/gate/updateJmGateUrl',data)
            }
        }
    }]);
