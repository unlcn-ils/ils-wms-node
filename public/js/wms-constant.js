/**
 * Created by lsj on 2017/8/17.
 */
'use strict';
angular.module('wms.constant', [])
    .constant('constant', {
        billsType: {   //单据类型
            systemSchedul: '1', //TMS推送
            manualEntry: '2',    //WMS业务产生库内返工、委外返工
            add: '3',   //WMS新增
            exit: '7'  //备料退库
        },
        inspectionResults: { //检验结果
            asnOrderPass: '10',    //合格
            asnOrderNotpassInRepair: '20' //异常
            // asnOrderNotpassOutRepair : '30',    //不合格-委外返工
            // asnOrderNotpass : '40'  //不合格-退厂
        },
        orderStatus: { //订单状态
            wmsInboundCreate: '10',    //未收货
            wmsInboundWaitAllocation: '20',    //已收货
            wmsInboundFinish: '30', //已入库
            wmsInboundCancel: '40' //取消入库
        },
        billsTypeName: {   //单据类型名称
            systemSchedul: 'TMS推送',
            manualEntry: 'WMS业务产生库内返工、委外返工',
            add: 'WMS新增',
            exit: '备料退库'
        },
        inspectionResultsName: {   //检验结果名称
            asnOrderPass: '合格',
            asnOrderNotpassInRepair: '异常'
            // asnOrderNotpassOutRepair : '不合格-委外返工',
            // asnOrderNotpass : '不合格-退厂'
        },
        orderStatusName: { //订单状态名称
            wmsInboundCreate: '未收货',
            wmsInboundWaitAllocation: '已收货',
            wmsInboundFinish: '已入库',
            wmsInboundCancel: '取消入库'
        },
        status: {  //状态
            enable: 10,   //可用
            disable: 20    //禁用
        },
        statusName: {  //状态名称
            enable: '可用',
            disable: '禁用'
        },
        page: {    //分页
            pageSize: 10,
            pageNo: 1
        },
        moreText: {    //更多展开收起文本
            packUp: '收起',
            spread: '更多条件'
        },
        distributionType: {    //分配类型
            manual: 10,
            auto: 20
        },
        stockUnitStatus: { //库位的状态
            start: 'Y',
            stop: 'N'
        }, stockUnitStatusName: { //库位的状态 name
            start: '启用',
            stop: '禁用'
        },
        repairType: {  //维修类型
            inner: 10, //库内维修
            outer: 20  //库外维修
        },
        repairTypeName: {  //维修类型 Name
            inner: '库内维修',
            outer: '库外维修'
        },
        repairStatus: {    //维修状态
            unprocessed: 10,   //未处理
            maintenance: 20,   //维修中
            finish: 30 //已完成
        },
        repairStatusName: {    //维修状态 Name
            unprocessed: '未处理',
            maintenance: '维修中',
            finish: '已完成'
        },
        repairResult: {    //维修结果
            qualified: 10,   //合格
            disqualification: 20,   //不合格
            cancellinStocks: 30 //退库
        },
        repairResultName: {    //维修结果 name
            qualified: '返修验车合格',   //合格
            disqualification: '返修验车不合格',   //不合格
            cancellinStocks: '退库' //退库
        },
        warehouse: {    //出库状态
            unprocessed: 10,   //未领取
            execution: 20,   //待开始
            haveOutbound: 30,   //进行中
            finish: 40, //已完成
            cancel: 50 //已取消
        },
        warehouseName: {    //出库状态 name
            unprocessed: '未领取',
            execution: '待开始',
            haveOutbound: '进行中',
            finish: '已完成',
            cancel: '已取消'
        },
        defaultDropDownNullData: {    //空数据
            name: '',
            value: ''
        },
        strategyConditionType: {    //策略类型
            shipper: 10,   //货主
            vehicle: 20    //车型
        },
        strategyConditionTypeName: {    //策略类型
            shipper: '货主',
            vehicle: '车型'
        },
        strategyOperator: {    //运算符
            xd: 10,  //等于
            bdy: 20, //不等于
            xy: 90,   //小于
            dy: 100,   //大于
            xydy: 80,    //小于等于
            dydy: 70,    //大于等于
            bh: 30,  //包含
            bbh: 40,    //不包含
            s: 50,    //是
            bs: 60   //不是
        },
        strategyOperatorName: {    //运算符
            xd: '等于',
            bdy: '不等于',
            xy: '小于',
            dy: '大于',
            xydy: '小于等于',
            dydy: '大于等于',
            bh: '包含',
            bbh: '不包含',
            s: '是',
            bs: '不是'
        },
        exceptionType: { //异常状态
            injured: 10,   //带伤
            shortItem: 20  //缺件
        },
        exceptionTypeName: { //异常类型 name
            injured: '带伤',
            shortItem: '缺件'
        },
        warehouseType: {   //仓库类型
            commodityGarage: 10,   //商品车库
            entrepotStorage: 20, //中转库
            frontLibrary: 30 //前置库
        },
        warehouseTypeName: {   //仓库类型 name
            commodityGarage: '商品车库',
            entrepotStorage: '中转库',
            frontLibrary: '前置库'
        },
        warehouseAttr: {   //仓库属性
            standardLib: 10,   //标准库
            simpleLib: 20  //简单库
        },
        warehouseAttrName: {   //仓库属性 name
            standardLib: '标准库',
            simpleLib: '简单库'
        },
        warehouseAuto: {   //仓库是否自动分配库位
            yes: 'Y',
            no: 'N'
        },
        warehouseAutoName: {   //仓库是否自动分配库位 name
            yes: '是',
            no: '否'
        },
        checkCarStatus: {  //验车状态
            noCheck: 10,   //未验车
            check: 20,  //已验车
            error: 30,  //异常
            errorApp: 70   //异常 app
        },
        checkCarStatusName: {  //验车状态
            noCheck: '待验车',
            check: '合格',
            error: '异常未登记',
            errorApp: '异常已登记'
        },
        checkCarResult: { //验车结果
            qualified: 10, //合格
            unqualified: 20    //不合格
        },
        checkCarResultName: { //验车结果name
            qualified: '合格',
            unqualified: '不合格'
        },
        outboundStatus: {  //备料单 status
            unStart: 10,   //未开始
            planned: 20,   //已计划
            confirmed: 30, //已备料
            finish: 40, //已完成
            cancel: 50 //已取消
        },
        outboundStatusName: {  //备料单 status name
            unStart: '未开始',
            planned: '已计划',
            confirmed: '已备料',
            finish: '已完成',
            cancel: '已取消'
        },
        outboundType: {   //出库类型
            dispatcherOutbound: 10,    //调拨出库
            sendOutbound: 20,  //发运出库
            repairOutbound: 30,    //维修出库
            borrowOutbound: 40 //借用出库
        },
        outboundTypeName: {    //出库类型 name
            dispatcherOutbound: '调拨出库',
            sendOutbound: '发运出库',
            repairOutbound: '维修出库',
            borrowOutbound: '借用出库'
        },
        sex: { //性别
            men: 1,
            women: 2
        },
        sexName: { //性别 name
            men: '男',
            women: '女'
        },
        outboundOutStatus: {   //备料出库状态
            notOut: 10,    //未出库
            out: 20,    //已出库
            exit: 30   //已退库
        },
        outboundOutStatusName: {   //备料出库状态 name
            notOut: '未出库',    //未出库
            out: '已出库',    //已出库
            exit: '已退库'
        },
        shipmentPlanStatus: {  //发运状态
            untreated: '0',  //未发运
            treated: '1', //已发运
            leaved: '2'  // 已离场
        },
        shipmentPlanStatusName: {  //发运状态 name
            untreated: '未发运',
            treated: '已发运',
            leaved: '已离场'
        },
        userType: {    //用户类型
            pc: 1, //pc
            app: 0, //app
            driver: 2  //司机
        },
        userTypeName: {    //用户类型 name
            pc: 'PC',
            app: 'APP',
            driver: '司机'
        },
        userWarehouseType: {   //用户仓库类型
            JMC_NC_QS: 'JMC_NC_QS',    //全顺库
            UNLCN_XN_: 'UNLCN_XN_',    //重庆库
            JM_: 'JM_'    //君马
        },
        shipmentPlanType: {    //发运类型
            normal: 'A1',
            dispatcher: 'A2'
        },
        shipmentPlanTypeName: {    //发运类型 name
            normal: '正常发运',
            dispatcher: '调度发运'
        },
        businessType: {    //入库类型
            Z1: 'Z1',  //PDI合格入库
            Z2: 'Z2',  //调拨入库
            Z3: 'Z3',  //维修出库
            Z4: 'Z4',  //维修后入库
            Z5: 'Z5',  //其他入库
            Z6: 'Z6',  //其他出库
            Z7: 'Z7',  //借用出库
            Z8: 'Z8',  //借用入库
            Z9: 'Z9'   //退货入库
        },
        businessTypeName: {    //入库类型 name
            Z1: 'PDI合格入库',  //PDI合格入库
            Z2: '调拨入库',
            Z3: '维修出库',
            Z4: '维修后入库',
            Z5: '其他入库',
            Z6: '其他出库',
            Z7: '借用出库',
            Z8: '借用入库',
            Z9: '退货入库'
        },
        shipmentRejectStatus: {    //发运驳回状态
            yes: '10',
            no: '20'
        },
        shipmentRejectStatusName: {    //发运驳回状态 name
            yes: '否',
            no: '是'
        },
        entranceCardStatus: {  //门禁卡状态
            start: 1,
            stop: 0
        },
        entranceCardStatusName: {  //门禁卡状态 name
            start: '启用',
            stop: '禁用'
        },
        locSize: { //库位存车类型
            minSize: '小车型',   //小车型
            maxSize: '大车型'    //大车型
        },
        locSizeName: { //库位存车类型 name
            minSize: '小车型',
            maxSize: '大车型'
        },
        locType: { //库位类型
            normal: 10,   //正常
            standby: 20    //备用
        },
        locTypeName: { //库位类型 name
            normal: '正常',
            standby: '备用'
        },
        outboundPrintStatus: { //出库交接单打印状态
            notPrint: 0,   //未打印
            print: 1   //已打印
        },
        outboundPrintStatusName: { //出库交接单打印状态 name
            notPrint: '未打印',
            print: '已打印'
        },
        changeCharStatus: {    //换车状态
            noChang: 10,   //未换车
            change: 20,    //已换车
            notChange: 30  //无车可换
        },
        changeCharStatusName: {    //换车状态 name
            noChang: '未换车',
            change: '已换车',
            notChange: '无车可换'
        },
        inventoryStatus: { //库存状态
            normal: 10,    //正常在库
            shipLock: 20,  //发运锁定
            borrowLock: 30,    //借用锁定
            maintenanceLock: 40,   //维修锁定
            outbound: 50 //已出库
        },
        inventoryStatusName: { //库存状态 name
            normal: '正常在库',
            shipLock: '发运锁定',
            borrowLock: '借用锁定',
            maintenanceLock: '维修锁定',
            outbound: '已出库'
        },
        openGateStatus: {  //开闸状态
            notOpen: 10,   //未开闸
            open: 20   //已开闸
        },
        openGateStatusName: {  //开闸状态 name
            notOpen: '未开闸',
            open: '已开闸'
        },
        openGateStatusTwo: {  //开闸状态
            planError: '计划内装车出场异常',
            notPlanError: '非计划装车出场异常',
            dischargeCargo: '卸车离场'
        },
        openGateStatusNameTwo: {  //开闸状态 name
            planError: '计划内装车出场异常',
            notPlanError: '非计划装车出场异常',
            dischargeCargo: '卸车离场'
        },
        departureType: { //离场类型
            normalDeparture: 10,  //正常离场
            errorDeparture: 20  //异常离场
        },
        departureTypeName: { //离场类型 name
            normalDeparture: '正常离场',
            errorDeparture: '异常离场'
        },
        gateType: {    //闸门类型
            inbound: 10,   //入库
            outbound: 20   //出库
        },
        gateTypeName: {    //闸门类型 name
            inbound: '入库',
            outbound: '出库'
        },
        gateController: {  //闸门控制
            notOpen: 0,    //不开闸门
            open: 1    //开闸
        },
        quitTask: {    //是否退库
            yes: 1,
            no: 0
        },
        quitTaskName: {    //是否退库  name
            yes: '是',
            no: '否'
        },
        operationOutOfStorageActionName: {
            C1Action: '出库类型',
            C2Action: '入库类型'
        },
        operationOutOfStorageAction: { //出入库运维的出入库类型  C1出库、C2入库
            C1Action: 'C1',
            C2Action: 'C2'
        },
        operationOutOfStorageSendStatusName: { //出入库运维的发送状态
            not_send: '未发送',
            sending: '发送中',
            send_success:'发送成功',
            send_fail:'发送失败'
        },
        operationOutOfStorageSendStatus: {
            not_send: '40',
            sending: '30',
            send_success:'10',
            send_fail:'20'
        },
        inventoryVerificationCustomerCode: {
            JM: 'JM',
            JL: 'JL',
            WMS_ALL:'WMS_ALL'
        }
    });