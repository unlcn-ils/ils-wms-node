/**
 * Created by houjianhui on 2017/7/19.
 */
'use strict';
wmsService.service('Tools', ['ngDialog', 'constant', '$timeout', function (ngDialog, constant, timeout) {

    var self = this;

    return {
        /*
         *   bigScreen   大屏y轴的显示高度为最大值高度加100
         *
         *   @obj     Echart 报表参数
         *   @arr     后端返回的画图数据
         *   @total   发运模式如专用
         *   @NoRight 标记是否有左侧百分比
         */
        bigScreenGetYMax: function (obj, arr, total, NoRight) {
            var temp = 0;
            if (total) {
                temp = total;
            } else {
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].data instanceof Array) {
                        for (var j = 0; j < arr[i].data.length; j++) {
                            if (temp < arr[i].data[j]) {
                                temp = arr[i].data[j];
                            }
                        }
                    }
                }
            }

            if (temp <= 10) {
                obj.yAxis[0].max = 12;
                obj.yAxis[0].interval = 1;
                if (!(NoRight == 'NoRight')) {
                    obj.yAxis[1].max = 120;
                    obj.yAxis[1].interval = 10;
                }

            } else if (temp <= 100) {
                var num = (Math.ceil(temp / 10)) * 10;     //    65/10 -> 7 ->  70
                var inter = num / 10;                        // 7
                obj.yAxis[0].max = num;                     // yMax = 700
                obj.yAxis[0].interval = 10;                 // inter = 100
                if (!(NoRight == 'NoRight')) {
                    obj.yAxis[1].max = Math.ceil(120 / inter) * inter;
                    obj.yAxis[1].interval = Math.ceil(120 / inter);
                }
            } else if (temp <= 400) {
                var num = (Math.floor(temp / 50) + 1) * 50;
                var inter = num / 50;
                obj.yAxis[0].max = num;
                obj.yAxis[0].interval = 50;
                if (!(NoRight == 'NoRight')) {
                    obj.yAxis[1].max = Math.ceil(120 / inter) * inter;
                    obj.yAxis[1].interval = Math.ceil(120 / inter);
                }
            } else if (temp <= 2000) {
                var num = (Math.ceil(temp / 100) + 1) * 100;
                var inter = num / 100;
                obj.yAxis[0].max = num;
                obj.yAxis[0].interval = 200;
                if (!total) {
                    if (!(NoRight == 'NoRight')) {
                        obj.yAxis[1].max = Math.ceil(120 / inter) * inter;
                        obj.yAxis[1].interval = Math.ceil(120 / inter);
                    }
                }

            } else if (temp <= 10000) {
                var num = (Math.ceil(temp / 1000)) * 1000;
                var inter = num / 1000;
                obj.yAxis[0].max = num;
                obj.yAxis[0].interval = 500;
                if (!total) {
                    obj.yAxis[1].max = Math.ceil(120 / inter) * (inter + 1);
                    obj.yAxis[1].interval = Math.ceil(120 / inter);
                }

            }
        },
        arrTotalMax: function (arr) {
            var temp = 0;
            var tempArr = [];
            for (var i = 0; i < arr.length; i++) {
                tempArr.push(0)
            }
            for (var i = 0; i < arr.length; i++) {
                var total = 0;

                if (arr[i].data instanceof Array) {
                    for (var j = 0; j < arr[i].data.length; j++) {
                        tempArr[j] += parseInt(arr[i].data[j]);
                    }
                }
            }
            for (var k = 0; k < tempArr.length; k++) {
                if (temp < tempArr[k]) {
                    temp = tempArr[k];
                }
            }
            return temp;
        },
        dialog: function (settings) {    //弹出层
            return ngDialog.open(settings);
        },
        dialogConfirm: function (msg) { //确认弹出层
            return ngDialog.openConfirm({
                closeByDocument: false,
                closeByEscape: false,
                template: 'templates/dialog/dialog-confirm.html',
                width: 500,
                title: '确认提示',
                data: {
                    msg: msg
                }
            });
        },
        dropDown: {    //下拉数据
            billsTypeDropDown: [   //单据类型下拉
                {
                    name: constant.billsTypeName.systemSchedul,  //TMS推送
                    value: constant.billsType.systemSchedul
                },
                {
                    name: constant.billsTypeName.manualEntry,  //WMS业务产生库内返工、委外返工
                    value: constant.billsType.manualEntry
                },
                {
                    name: constant.billsTypeName.add,  //wms新增
                    value: constant.billsType.add
                },
                {
                    name: constant.billsTypeName.exit,  //备料退库
                    value: constant.billsType.exit
                }
            ],
            orderStatusDropDown: [ //订单状态下拉
                {
                    name: constant.orderStatusName.wmsInboundCreate,   //未收货
                    value: constant.orderStatus.wmsInboundCreate
                },
                {
                    name: constant.orderStatusName.wmsInboundWaitAllocation,   //已收货
                    value: constant.orderStatus.wmsInboundWaitAllocation
                },
                /*{
                    name : constant.orderStatusName.wmsInboundFinishAllocation, //已分配
                    value : constant.orderStatus.wmsInboundFinishAllocation
                },
                {
                    name : constant.orderStatusName.wmsInboundConfirmAllocation,    //待入库
                    value : constant.orderStatus.wmsInboundConfirmAllocation
                },*/
                {
                    name: constant.orderStatusName.wmsInboundFinish,   //已入库
                    value: constant.orderStatus.wmsInboundFinish
                },
                {
                    name: constant.orderStatusName.wmsInboundCancel,   //取消入库
                    value: constant.orderStatus.wmsInboundCancel
                }
            ],
            inspectionResultsDropDown: [   //检验结果下拉
                {
                    name: constant.inspectionResultsName.asnOrderPass, //合格
                    value: constant.inspectionResults.asnOrderPass
                },
                {
                    name: constant.inspectionResultsName.asnOrderNotpassInRepair,  //不合格-库内返修
                    value: constant.inspectionResults.asnOrderNotpassInRepair
                }
                // {
                //     name : constant.inspectionResultsName.asnOrderNotpassOutRepair, //不合格-委外返工
                //     value : constant.inspectionResults.asnOrderNotpassOutRepair
                // },
                // {
                //     name :  constant.inspectionResultsName.asnOrderNotpass, //不合格-退厂
                //     value : constant.inspectionResults.asnOrderNotpass
                // }
            ],
            statusDropDown: [  //状态下拉
                {
                    name: constant.statusName.enable,  //可用
                    value: constant.status.enable
                },
                {
                    name: constant.statusName.disable, //不可用
                    value: constant.status.disable
                }
            ],
            repairTypeDropDown: [  //维修类型状态
                {
                    name: constant.repairTypeName.inner,   //库内维修
                    value: constant.repairType.inner
                },
                {
                    name: constant.repairTypeName.outer,   //库外维修
                    value: constant.repairType.outer
                }
            ],
            repairStatusDropDown: [    //维修状态
                {
                    name: constant.repairStatusName.unprocessed,   //未处理
                    value: constant.repairStatus.unprocessed
                },
                {
                    name: constant.repairStatusName.maintenance,   //维修中
                    value: constant.repairStatus.maintenance
                },
                {
                    name: constant.repairStatusName.finish,   //已完成
                    value: constant.repairStatus.finish
                }
            ],
            repairResultDropDown: [    //维修结果
                {
                    name: constant.repairResultName.qualified,   //合格
                    value: constant.repairResult.qualified
                },
                {
                    name: constant.repairResultName.disqualification,   //不合格
                    value: constant.repairResult.disqualification
                }/*,
                {
                    name : constant.repairResultName.cancellinStocks,   //退库
                    value : constant.repairResult.cancellinStocks
                }*/
            ],
            warehouseDropDown: [    //出库状态
                {
                    name: constant.warehouseName.unprocessed,   //未领取
                    value: constant.warehouse.unprocessed
                },
                {
                    name: constant.warehouseName.execution,   //待开始
                    value: constant.warehouse.execution
                },
                {
                    name: constant.warehouseName.haveOutbound,   //进行中
                    value: constant.warehouse.haveOutbound
                },
                {
                    name: constant.warehouseName.finish,   //已完成
                    value: constant.warehouse.finish
                },
                {
                    name: constant.warehouseName.cancel,   //已取消
                    value: constant.warehouse.cancel
                }
            ],
            stockUnitStatusDropDown: [   //库位状态下拉
                {
                    name: constant.stockUnitStatusName.start,   //启用
                    value: constant.stockUnitStatus.start
                },
                {
                    name: constant.stockUnitStatusName.stop,    //停用
                    value: constant.stockUnitStatus.stop
                }
            ],
            strategyConditionTypeDropDown: [    //策略条件类型
                {
                    name: constant.strategyConditionTypeName.shipper,   //货主
                    value: constant.strategyConditionType.shipper
                },
                {
                    name: constant.strategyConditionTypeName.vehicle,    //车型
                    value: constant.strategyConditionType.vehicle
                }
            ],
            strategyOperatorDropDown: [    //策略运算符
                {
                    name: constant.strategyOperatorName.xd,    //相等
                    value: constant.strategyOperator.xd
                },
                {
                    name: constant.strategyOperatorName.bdy,    //不相等
                    value: constant.strategyOperator.bdy
                },
                {
                    name: constant.strategyOperatorName.xy,    //小于
                    value: constant.strategyOperator.xy
                },
                {
                    name: constant.strategyOperatorName.dy,    //大于
                    value: constant.strategyOperator.dy
                },
                {
                    name: constant.strategyOperatorName.xydy,    //小于等于
                    value: constant.strategyOperator.xydy
                },
                {
                    name: constant.strategyOperatorName.dydy,    //大于等于
                    value: constant.strategyOperator.dydy
                },
                {
                    name: constant.strategyOperatorName.bh,    //包含
                    value: constant.strategyOperator.bh
                },
                {
                    name: constant.strategyOperatorName.bbh,    //不包含
                    value: constant.strategyOperator.bbh
                },
                {
                    name: constant.strategyOperatorName.s,    //是
                    value: constant.strategyOperator.s
                },
                {
                    name: constant.strategyOperatorName.bs,    //不是
                    value: constant.strategyOperator.bs
                }
            ],
            exceptionTypeDropDown: [   //异常类型
                {
                    name: constant.exceptionTypeName.injured,  //带伤
                    value: constant.exceptionType.injured
                },
                {
                    name: constant.exceptionTypeName.shortItem,  //缺件
                    value: constant.exceptionType.shortItem
                }
            ],
            warehouseTypeDropDown: [   //仓库类型
                {
                    name: constant.warehouseTypeName.commodityGarage,  //商品车库
                    value: constant.warehouseType.commodityGarage
                },
                {
                    name: constant.warehouseTypeName.entrepotStorage,  //中转库
                    value: constant.warehouseType.entrepotStorage
                },
                {
                    name: constant.warehouseTypeName.frontLibrary,  //前置库
                    value: constant.warehouseType.frontLibrary
                }
            ],
            warehouseAttrDropDown: [   //仓库属性
                {
                    name: constant.warehouseAttrName.standardLib,  //标准库
                    value: constant.warehouseAttr.standardLib
                },
                {
                    name: constant.warehouseAttrName.simpleLib,  //简单库
                    value: constant.warehouseAttr.simpleLib
                }
            ],
            warehouseAutoDropDown: [   //仓库库位自动分配
                {
                    name: constant.warehouseAutoName.yes,  //是
                    value: constant.warehouseAuto.yes
                },
                {
                    name: constant.warehouseAutoName.no,  //否
                    value: constant.warehouseAuto.no
                }
            ],
            checkCarStatusDropDown: [  //验车状态
                {
                    name: constant.checkCarStatusName.noCheck,  //待验车
                    value: constant.checkCarStatus.noCheck
                },
                {
                    name: constant.checkCarStatusName.check,  //合格
                    value: constant.checkCarStatus.check
                },
                {
                    name: constant.checkCarStatusName.error,  //异常
                    value: constant.checkCarStatus.error
                },
                {
                    name: constant.checkCarStatusName.errorApp,  //异常 app
                    value: constant.checkCarStatus.errorApp
                }
            ],
            checkCarResultDropDown: [ //验车结果
                {
                    name: constant.checkCarResultName.qualified,   //合格
                    value: constant.checkCarResult.qualified
                },
                {
                    name: constant.checkCarResultName.unqualified,   //不合格
                    value: constant.checkCarResult.unqualified
                }
            ],
            outboundStatusDropDown: [  //备料单状态
                {
                    name: constant.outboundStatusName.unStart,   //未开始
                    value: constant.outboundStatus.unStart
                },
                {
                    name: constant.outboundStatusName.planned,   //已计划
                    value: constant.outboundStatus.planned
                },
                {
                    name: constant.outboundStatusName.confirmed,   //已备料
                    value: constant.outboundStatus.confirmed
                },
                {
                    name: constant.outboundStatusName.finish,   //已完成
                    value: constant.outboundStatus.finish
                },
                {
                    name: constant.outboundStatusName.cancel,   //已取消
                    value: constant.outboundStatus.cancel
                }
            ],
            sexDropDown: [ //性别
                {
                    name: constant.sexName.men,   //男
                    value: constant.sex.men
                },
                {
                    name: constant.sexName.women,   //女
                    value: constant.sex.women
                }
            ],
            outboundOutStatusDropDown: [   //备料出库状态
                {
                    name: constant.outboundOutStatusName.notOut,   //未出库
                    value: constant.outboundOutStatus.notOut
                },
                {
                    name: constant.outboundOutStatusName.out,   //已出库
                    value: constant.outboundOutStatus.out
                },
                {
                    name: constant.outboundOutStatusName.exit,   //已退库
                    value: constant.outboundOutStatus.exit
                }
            ],
            shipmentPlanStatusDropDown: [  //发运计划状态
                {
                    name: constant.shipmentPlanStatusName.untreated,   //未处理
                    value: constant.shipmentPlanStatus.untreated
                },
                {
                    name: constant.shipmentPlanStatusName.treated,   //已处理
                    value: constant.shipmentPlanStatus.treated
                }
            ],
            userTypeDropDown: [    //用户类型
                {
                    name: constant.userTypeName.pc,   //pc
                    value: constant.userType.pc
                },
                {
                    name: constant.userTypeName.app,   //app
                    value: constant.userType.app
                },
                {
                    name: constant.userTypeName.driver,   //司机
                    value: constant.userType.driver
                }
            ],
            shipmentPlanTypeDropDown: [    //发运类型
                {
                    name: constant.shipmentPlanTypeName.normal,   //正常发运
                    value: constant.shipmentPlanType.normal
                },
                {
                    name: constant.shipmentPlanTypeName.dispatcher,   //调度发运
                    value: constant.shipmentPlanType.dispatcher
                }
            ],
            businessTypeDropDown: [    //入库类型
                {
                    name: constant.businessTypeName.Z1,   //PDI合格入库
                    value: constant.businessType.Z1
                },
                {
                    name: constant.businessTypeName.Z2,   //调拨入库
                    value: constant.businessType.Z2
                },
                {
                    name: constant.businessTypeName.Z3,   //维修出库
                    value: constant.businessType.Z3
                },
                {
                    name: constant.businessTypeName.Z4,   //维修后入库
                    value: constant.businessType.Z4
                },
                {
                    name: constant.businessTypeName.Z5,   //其他入库
                    value: constant.businessType.Z5
                },
                {
                    name: constant.businessTypeName.Z6,   //其他出库
                    value: constant.businessType.Z6
                },
                {
                    name: constant.businessTypeName.Z7,   //借用出库
                    value: constant.businessType.Z7
                },
                {
                    name: constant.businessTypeName.Z8,   //借用入库
                    value: constant.businessType.Z8
                },
                {
                    name: constant.businessTypeName.Z9,   //退货入库
                    value: constant.businessType.Z9
                }
            ],
            shipmentRejectStatusDropDown: [    //发运驳回状态
                {
                    name: constant.shipmentRejectStatusName.yes,   //是
                    value: constant.shipmentRejectStatus.yes
                },
                {
                    name: constant.shipmentRejectStatusName.no,   //否
                    value: constant.shipmentRejectStatus.no
                }
            ],
            entranceCardStatusDropDown: [    //门禁卡状态
                {
                    name: constant.entranceCardStatusName.start,   //启用
                    value: constant.entranceCardStatus.start
                },
                {
                    name: constant.entranceCardStatusName.stop,   //禁用
                    value: constant.entranceCardStatus.stop
                }
            ],
            locTypeStatusDropDown: [    //库位类型
                {
                    name: constant.locTypeName.normal,   //正常
                    value: constant.locType.normal
                },
                {
                    name: constant.locTypeName.standby,   //备用
                    value: constant.locType.standby
                }
            ],
            locSizeStatusDropDown: [    //库位存车类型
                {
                    name: constant.locSizeName.minSize,   //小车型
                    value: constant.locSize.minSize
                },
                {
                    name: constant.locSizeName.maxSize,   //大车型
                    value: constant.locSize.maxSize
                }
            ],
            outboundPrintStatusDropDown: [ //出库打印状态
                {
                    name: constant.outboundPrintStatusName.notPrint,   //未打印
                    value: constant.outboundPrintStatus.notPrint
                },
                {
                    name: constant.outboundPrintStatusName.print,   //已打印
                    value: constant.outboundPrintStatus.print
                }
            ],
            openGateStatusDropDown: [    //开闸状态
                {
                    name: constant.openGateStatusName.notOpen,   //未开闸
                    value: constant.openGateStatus.notOpen
                },
                {
                    name: constant.openGateStatusName.open,   //已开闸
                    value: constant.openGateStatus.open
                }
            ],
            departureTypeDropDown: [    //离场类型
                {
                    name: constant.departureTypeName.normalDeparture,   //正常离场
                    value: constant.departureType.normalDeparture
                },
                {
                    name: constant.departureTypeName.errorDeparture,   //异常离场
                    value: constant.departureType.errorDeparture
                }
            ],
            quitTaskDropDown: [    //是否退库
                {
                    name: constant.quitTaskName.yes,   //是
                    value: constant.quitTask.yes
                },
                {
                    name: constant.quitTaskName.no,   //否
                    value: constant.quitTask.no
                }
            ],
            operationOutOfStorageDown: [
                {
                    //出库
                    name: constant.operationOutOfStorageActionName.C1Action,
                    value: constant.operationOutOfStorageAction.C1Action
                }, {
                    //入库
                    name: constant.operationOutOfStorageActionName.C2Action,
                    value: constant.operationOutOfStorageAction.C2Action
                }
            ],
            send_status: [
                {
                    //未发送
                    name: constant.operationOutOfStorageSendStatusName.not_send,
                    value: constant.operationOutOfStorageSendStatus.not_send
                }, {
                    //发送中
                    name: constant.operationOutOfStorageSendStatusName.sending,
                    value: constant.operationOutOfStorageSendStatus.sending
                }, {
                    //发送失败
                    name: constant.operationOutOfStorageSendStatusName.send_fail,
                    value: constant.operationOutOfStorageSendStatus.send_fail
                }, {
                    //发送成功
                    name: constant.operationOutOfStorageSendStatusName.send_success,
                    value: constant.operationOutOfStorageSendStatus.send_success
                }
            ]
        },
        modelAssignment: function (valueObj, scope) {  //给model赋值
            var ngModel = null;
            var value = valueObj;
            if (!(valueObj instanceof Object)) { //不是对象
                value = valueObj;
                ngModel = this.elem.attributes['ng-model'].value;
                scope = this.scope;
            } else {
                ngModel = valueObj.currentTarget.attributes['ng-model'].value
                value = valueObj.currentTarget.value;
            }

            if (ngModel) {
                var arrObj = ngModel.split('.');
                if (arrObj.length == 1) {
                    scope[arrObj[0]] = value;
                } else if (arrObj.length == 2) {
                    if (!scope[arrObj[0]]) {
                        scope[arrObj[0]] = {};
                    }
                    scope[arrObj[0]][arrObj[1]] = value;
                } else if (arrObj.length == 3) {
                    scope[arrObj[0]][arrObj[1]][arrObj[2]] = value;
                } else if (arrObj.length == 4) {
                    scope[arrObj[0]][arrObj[1]][arrObj[2]][arrObj[3]] = value;
                } else if (arrObj.length == 5) {
                    scope[arrObj[0]][arrObj[1]][arrObj[2]][arrObj[3]][arrObj[4]] = value;
                }
            }
        },
        alert: function (msg) {  //普通提示框
            if (msg) {
                layer.msg(msg);
            }
        },
        open: function (msg) {   // 当返回信息较多时，弹出永久弹出框，供用户关闭
            if (msg) {
                layer.open({
                    title: '提示',
                    content: msg
                })
            }

        },
        page: function (element, settings, callback) { //分页
            if (!settings) {
                settings = {};
            }
            settings.elem = element ? $(element) : $(settings.elem);
            settings.jump = settings.jump ? settings.jump : callback;
            settings.theme = '#de531a';
            settings.last = settings.last ? settings.last : 10;
            settings.curr = settings.curr ? settings.curr : 1;
            settings.prev = '<';
            settings.next = '>';
            settings.skip = true;
            settings.layout = ['prev', 'page', 'next', 'count', 'limit', 'skip'];
            layui.use(['laypage'], function () {
                var laypage = layui.laypage;
                laypage.render(settings);
            })
        },
        initCheckBox: function () {  //初始化多选
            if (layui.form) {
                var form = layui.form();
                timeout(function () {
                    form.render();
                    form.on('checkbox(allChoose)', function (data) {
                        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]');
                        child.each(function (index, item) {
                            item.checked = data.elem.checked;
                        });
                        form.render('checkbox');
                    });
                }, 100);
            } else {
                layui.use(['form'], function () {
                    var form = layui.form();
                    timeout(function () {
                        form.render();
                        form.on('checkbox(allChoose)', function (data) {
                            var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]');
                            child.each(function (index, item) {
                                item.checked = data.elem.checked;
                            });
                            form.render('checkbox');
                        });
                    }, 100);
                });
            }
        },
        initTableClick: function (e) {    //初始化表格click
            if (e) {
                $(e.currentTarget).parent().children().removeClass('checkStatus');  //移除所有tr的背景状态选中样式
                $(e.currentTarget).addClass('checkStatus'); //添加当前背景选中的样式

                //移除之前选择的数据
                angular.forEach($(e.currentTarget).parent().children(), function (v, k) {
                    if (k != 0) {
                        if ($(v).children().first().find('div.layui-form-checked').length > 0) {
                            $(v).children().first().find('div.layui-form-checked').trigger('click');
                        }
                    }
                })

                //判断表格第一个是否是checkbox 如果有则选中
                if ($(e.currentTarget).children().first().find('div.layui-form-checkbox').length > 0) {
                    timeout(function () {
                        $(e.currentTarget).children().first().children('div.layui-form-checkbox').trigger('click');
                    }, 100)
                }
            }
        },
        getTableSelectItemIndex: function (selector) {    //获取表格选中的checkbox index
            var child = $(selector).find('tbody input[type="checkbox"][name=ckValue]');
            var indexArray = new Array();
            child.each(function (index, item) {
                if (item.checked) {   //判断是否选中
                    indexArray.push(item.value | index);    //获取index
                }
            });
            return indexArray;
        },
        getDataTOIndex: function (array, data) {  //根据下标取出相对应的数据
            var dataArr = new Array();
            for (var i = 0; i < array.length; i++) {
                dataArr.push(data[array[i]]);
            }
            return dataArr;
        },
        print: function (html) { //打印
            var newWin = window.open('', '"_blank"');
            newWin.document.write('<html><head><link href="js/lib/layui/css/layui.css" type="text/css" rel="stylesheet"><link href="css/wms.css" type="text/css" rel="stylesheet"></head><body>');
            newWin.document.write(html);
            newWin.document.write('<script type="text/javascript">setTimeout(function(){window.print();window.close()},500)</script>');
            newWin.document.write('</body></html>');
        },
        printQr: function (base64, vin, localNo, orderBillNo, pages, whCode) {   //打印二维码   localNo 库位 orderBillNo运单号

            if (!vin) {
                vin = '';
            }

            if (!localNo) {
                localNo = '';
            }

            if (!orderBillNo) {
                orderBillNo = '';
            }

            var html = '';
            LODOP.PRINT_INIT("二维码打印");

            if (whCode == constant.userWarehouseType.UNLCN_XN_) {
                var tableStart = '<table border="0" style="font-size: 12px; margin: 0px; padding: 0px; font-weight:bold;border:none;" width="112px">';
                var tr1 = '<tr><td rowspan="2" style="overflow: hidden"><div style="width:43px;height:43px;overflow:hidden"><img style="margin-top: -8px;margin-left: -8px" src="data:image/png;base64,' + base64 + '"/></div></td><td>' + vin.substring(10, 17) + '</td></tr>';
                var tr2 = '<tr><td>' + localNo + '</td></tr>';
                var tr3 = '<tr><td colspan="2" style="font-size: 10px;margin: 0px; padding: 0px;">' + orderBillNo + '</td></tr>';
                var tableEnd = '</table>';
                html = tableStart + tr1 + tr2 + tr3 + tableEnd;
                LODOP.SET_PRINT_PAGESIZE(0, 300, 200);
                LODOP.ADD_PRINT_HTM(7, 7, '13mm', '18mm', html);
            } else if (whCode == constant.userWarehouseType.JM_) {
                var tableStart = '<table border="0" style="font-size: 12px; margin: 0px; padding: 0px; font-weight:bold; height: 100px;">';
                var tr1 = '<tr><td><img style="margin-left: -8px" src="data:image/png;base64,' + base64 + '"/></td></tr>';
                var tr2 = '<tr><td><div style="margin-left: 72px;">' + vin + '</div></td></tr>';
                var tableEnd = '</table>';
                html = tableStart + tr1 + tr2 + tableEnd;
                LODOP.SET_PRINT_PAGESIZE(0, 700, 1000);
                LODOP.ADD_PRINT_HTM(10, 0, '70mm', '100mm', html);
            }

            if (pages) {
                LODOP.SET_PRINT_COPIES(pages);  //设置打印数量
            }
            /*LODOP.PREVIEW(); //调试模式*/
            LODOP.PRINT();  //打印模式
        },
        printHTML: function (html, width, pages) { // width : 宽度 pages : 页数
            if (!width) {
                width = 790;
            }
            var html1 = '<html><head><link href="js/lib/layui/css/layui.css" type="text/css" rel="stylesheet"><link href="css/wms.css" type="text/css" rel="stylesheet"></head><body style="width:' + width + 'px;">';
            html1 += html;
            html1 += '</body></html>';

            LODOP.PRINT_INIT("打印");
            LODOP.ADD_PRINT_HTM(0, 0, '210mm', '297mm', html1);
            if (pages) {
                LODOP.SET_PRINT_COPIES(pages);  //设置打印数量
            }
            LODOP.SET_PRINT_MODE('PRINT_PAGE_PERCENT', 'Width:95%');
            //调试模式
            LODOP.PREVIEW();

            //打印模式
            // LODOP.PRINT();
        },
        printHTMLWidth: function (html, width, pages) { // width : 宽度 pages : 页数
            if (!width) {
                width = 790;
            }
            var html1 = '<html><head><link href="js/lib/layui/css/layui.css" type="text/css" rel="stylesheet"><link href="css/wms.css" type="text/css" rel="stylesheet"></head><body style="width:' + width + 'px;">';
            html1 += html;
            html1 += '</body></html>';

            LODOP.PRINT_INIT("打印");
            LODOP.SET_PRINT_PAGESIZE(1, 2410, 2790);
            // LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED",1);//横向时的正向显示
            LODOP.ADD_PRINT_HTM(0, 35, '241mm', '279mm', html1);
            if (pages) {
                LODOP.SET_PRINT_COPIES(pages);  //设置打印数量
            }
            LODOP.SET_PRINT_MODE('PRINT_PAGE_PERCENT', 'Width:95%');
            //调试模式
            LODOP.PREVIEW();

            //打印模式
            // LODOP.PRINT();
        },
        isRange: function (s, e) {   //  验证时间是否合法
            var rs = false;
            s == '' || !s ? rs = true : '';
            e == '' || !e ? rs = true : '';
            if (rs) {
                return rs
            }
            var s_t = parseInt(new Date(s).getTime());
            var e_t = parseInt(new Date(e).getTime());
            if (e_t - s_t >= 0) {
                return true
            } else {
                return false
            }
        },
        checkPrintInstall: function () {   //检测打印机插件是否安装
            try {
                if (LODOP && LODOP.VERSION) {
                    LODOP.SET_LICENSES("北京知车科技有限责任公司", "BC324309DFEA0DECE56958C928E1CA08", "", "");    //设置注册码
                    return true;
                } else {
                    return false;
                }
            } catch (e) {
                return false;
            }
        },
        downLoadPrintPlugIn: function () {
            ngDialog.openConfirm({
                closeByDocument: false,
                closeByEscape: false,
                template: 'templates/dialog/dialog-confirm.html',
                width: 500,
                title: '确认提示',
                data: {
                    msg: '本机还未安装打印插件，是否继续下载插件安装？'
                }
            }).then(function (res) {
                if (res) {
                    window.open('plugIn/Lodop6.220_CLodop3.020.zip');
                }
            })
        },
        getBillsTypeDropDownByKey: function (key) { //根据key获取单据类型值
            for (var i = 0; i < this.dropDown.billsTypeDropDown.length; i++) {
                if (this.dropDown.billsTypeDropDown[i].value == key) {
                    return this.dropDown.billsTypeDropDown[i];
                }
            }
        },
        getRepairTypeDropDownByKey: function (key) {    //根据key获取维修类型值
            for (var i = 0; i < this.dropDown.repairTypeDropDown.length; i++) {
                if (this.dropDown.repairTypeDropDown[i].value == key) {
                    return this.dropDown.repairTypeDropDown[i];
                }
            }
        },
        getInspectionResultsDropDownByKey: function (key) {  //根据key获取检验结果
            for (var i = 0; i < this.dropDown.inspectionResultsDropDown.length; i++) {
                if (this.dropDown.inspectionResultsDropDown[i].value == key) {
                    return this.dropDown.inspectionResultsDropDown[i];
                }
            }
        },
        getRepairResultDropDownByKey: function (key) {   //根据key获取维修结果
            for (var i = 0; i < this.dropDown.repairResultDropDown.length; i++) {
                if (this.dropDown.repairResultDropDown[i].value == key) {
                    return this.dropDown.repairResultDropDown[i];
                }
            }
        },
        getWarehouseTypeDropDownByKey: function (key) {    //根据key 获取仓库类型
            for (var i = 0; i < this.dropDown.warehouseTypeDropDown.length; i++) {
                if (this.dropDown.warehouseTypeDropDown[i].value == key) {
                    return this.dropDown.warehouseTypeDropDown[i];
                }
            }
        },
        getWarehouseAttrDropDownByKey: function (key) {    //根据key 获取仓库属性
            for (var i = 0; i < this.dropDown.warehouseAttrDropDown.length; i++) {
                if (this.dropDown.warehouseAttrDropDown[i].value == key) {
                    return this.dropDown.warehouseAttrDropDown[i];
                }
            }
        },
        getWarehouseAutoDropDownByKey: function (key) {    //根据key 获取仓库自动分配
            for (var i = 0; i < this.dropDown.warehouseAutoDropDown.length; i++) {
                if (this.dropDown.warehouseAutoDropDown[i].value == key) {
                    return this.dropDown.warehouseAutoDropDown[i];
                }
            }
        }
    };
}])