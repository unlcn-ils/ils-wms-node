var express = require('express');
var router = express.Router();

var baseConfig = require('./resource/base-config');
var request = require('request');

/* GET home page. */
router.get('/', function(req, res, next){
    var nodeRes = res;

    //验证用户是否合法
    if(baseConfig.validate) {
        // request.get({
        //     qs: 'pageNo=1&pageSize=10',
        //     json: true,
        //     async : false,
        //     url: "http://10.2.4.80:8080/ils-tms-web/loginController/login?",
        //     headers: {
        //         'User-Agent': 'request'
        //     }
        // },function(err, resp, body){});
        // console.log("11111");
        // request.get({
        //     qs: 'pageNo=1&pageSize=10',
        //     json: true,
        //     async : false,
        //     url: "http://10.2.4.80:8080/ils-tms-web/loginController/login?",
        //     headers: {
        //         'User-Agent': 'mozilla/5.0 (windows nt 10.0; wow64) applewebkit/537.36 (khtml, like gecko) chrome/60.0.3112.78 safari/537.36'
        //     }
        // },function(err, resp, body){});
        // // console.log("22222");

        request.get({
            qs: 'pageNo=1&pageSize=10',
            json: true,
            url: "http://10.2.4.80:8080/ils-tms-web/oauth2-login?",
            headers: {
                'User-Agent': 'mozilla/5.0 (windows nt 10.0; wow64) applewebkit/537.36 (khtml, like gecko) chrome/60.0.3112.78 safari/537.36'
            }
        }, function (err,resp, body) {
            console.log('2222');
            // console.log(err +'---'+resp.statusCode +'--'+ resp.toString());
            // console.log(resp);
            // console.log(req);
            console.log("------->"+body);
            if (!err && resp.statusCode === 200) {
                if (body.success) {
                    nodeRes.header("Access-Control-Allow-Origin", "*"); //设置请求来源不受限制
                    nodeRes.render('index', {
                        isCompressed: false,
                        devView: baseConfig.devView,
                        version: baseConfig.version,
                        tmsMap : baseConfig.tmsMap,
                        host : baseConfig.host
                    });
                } else {
                    console.log(body);
                    nodeRes.render('illegalUser');
                }
            } else {
                nodeRes.render('error');
                console.log('---获取数据失败---');
                console.log(body);

            }
        });
    } else {
        nodeRes.header("Access-Control-Allow-Origin", "*"); //设置请求来源不受限制
        nodeRes.render('login', {
            compressed: baseConfig.compressed,
            devView: baseConfig.devView,
            version: baseConfig.version,
            tmsMap : baseConfig.tmsMap,
            host : baseConfig.host
        });
    }
});

router.get('/index', function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*"); //设置请求来源不受限制
    res.render('index', {
        compressed: baseConfig.compressed,
        devView: baseConfig.devView,
        version: baseConfig.version,
        tmsMap : baseConfig.tmsMap,
        host : baseConfig.host
    });
});

module.exports = router;
